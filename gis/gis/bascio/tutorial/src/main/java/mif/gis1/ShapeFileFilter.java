package mif.gis1;


import java.io.File;
import java.io.FileFilter;

public class ShapeFileFilter implements FileFilter {

    public boolean accept(File pathname) {
        
        String accExt = "shp";
        String name = pathname.getName().toLowerCase();
        if(pathname.isDirectory()){
            return false;
        }
        
        if (name.endsWith(accExt)){
            return true;
        } else {
            return false;
        }
    }

}
