package mif.gis1;

import java.awt.BorderLayout;
import java.awt.Checkbox;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JToolBar;
import javax.swing.table.DefaultTableModel;

import org.geotools.data.FeatureSource;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Query;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DefaultMapContext;
import org.geotools.map.MapContext;
import org.geotools.map.MapLayer;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Fill;
import org.geotools.styling.Graphic;
import org.geotools.styling.Mark;
import org.geotools.styling.Rule;
import org.geotools.styling.Stroke;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.Symbolizer;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;
import org.opengis.filter.spatial.Intersects;
import org.opengis.geometry.BoundingBox;
import org.opengis.geometry.primitive.Point;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;

/**
 * Prompts the user for a shapefile and displays the contents on the screen in a
 * map frame. <p> This is the GeoTools Quickstart application used in
 * documentationa and tutorials. *
 */
public class GIS extends JMapFrame implements ActionListener {

    //MyMapFrame myFrame = new MyMapFrame();
	 /*
     * Factories that we will use to create style and filter objects
     */
    private StyleFactory sf = CommonFactoryFinder.getStyleFactory(null);
    private FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);
    private String geometryAttributeName;
    private GeomType geometryType;

    /*
     * Convenient constants for the type of feature geometry in the shapefile
     */
    private enum GeomType {

        POINT, LINE, POLYGON
    };

    /*
     * Some default style variables
     */
    private static final Color LINE_COLOUR = Color.BLACK;
    private static final Color FILL_COLOUR = Color.WHITE;
    private static final Color SELECTED_COLOUR = Color.RED;
    private static final float OPACITY = 1.0f;
    private static final float LINE_WIDTH = 1.0f;
    private static final float POINT_SIZE = 10.0f;
    private JComboBox featureTypeCBox;
    private JTable table;
    private JTextField text;
    //used for zooming selected area
    private double minX = -1;
    private double minY = -1;
    private double maxX = -1;
    private double maxY = -1;
    private boolean selectSeparate = false;
    //Tikrina ar pakeistas layeris layer table
    private int lastSelected = -1;
    Checkbox showOnMap = new Checkbox("ShowOnMap");
    private SimpleFeatureCollection selectedFeatures = FeatureCollections.newCollection();

    /**
     * GeoTools Quickstart demo application. Prompts the user for a shapefile
     * and displays its contents on the screen in a map frame
     */
    public static void main(String[] args) throws Exception {
        // display a data store file chooser dialog for shapefiles
        File file = new File("/home/antanas/Unikas/gis/duomenai/LT250shp/Keliai.shp");//JFileDataStoreChooser.showOpenFile("shp", null);
        File file2 = new File("/home/antanas/Unikas/gis/duomenai/LT250shp/Ezerai.shp");
        if (file == null) {
            return;
        }

        FileDataStore store = FileDataStoreFinder.getDataStore(file);

        FeatureSource featureSource = store.getFeatureSource();
        //map.addLayer(featureSource, null);

        store = FileDataStoreFinder.getDataStore(file2);
        featureSource = store.getFeatureSource();


        javax.swing.SwingUtilities.invokeLater(new Runnable() {

            public void run() {
                GIS gis = new GIS();
                gis.init();
                gis.display();
            }
        });
        //map.addLayer(featureSource, null);

        //myFrame.setSize(1200, 600);
        //myFrame.setVisible(true);

    }

    public void init() {
        MapContext map = new DefaultMapContext();
        map.setTitle("Mano brangi Lietuvėlė");
        this.getContentPane().setLayout(new BorderLayout());

        enableToolBar(true);
        enableStatusBar(true);
        enableLayerTable(true);
        //System.out.println(this.getSelectedLayer());
        //gaidiska implementacija, darom savaip.
        //mapLayerTable = new MapLayerTable(mapPane);

        /*
         * We put the map layer panel and the map pane into a JSplitPane so that
         * the user can adjust their relative sizes as needed during a session.
         * The call to setPreferredSize for the layer panel has the effect of
         * setting the initial position of the JSplitPane divider
         */
        //mapLayerTable.setPreferredSize(new Dimension(200, -1));
        //JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, false, mapLayerTable, mapPane);
        //panel.add(splitPane, "grow");
        JToolBar toolBar = getToolBar();
        JButton btnSelect = new JButton("Select");
        JButton btnUnselect = new JButton("Unselect all");
        JButton btnZoomSelect = new JButton("Zoom selected");
        JButton btnQuery = new JButton("Query data");
        JButton btnSelectSeparate = new JButton("Select separate");
        toolBar.addSeparator();
        toolBar.add(btnSelect);
        toolBar.addSeparator();
        toolBar.add(btnUnselect);
        toolBar.addSeparator();
        toolBar.add(btnZoomSelect);
        toolBar.addSeparator();
        toolBar.add(btnSelectSeparate);
        //GIS.this.getMapPane().setCursorTool(new SelectManyTool(GIS.this));
        btnSelect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                GIS.this.selectSeparate = false;
                GIS.this.getMapPane().setCursorTool(new SelectManyTool(GIS.this));
            }
        });
        btnUnselect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                GIS.this.selectSeparate = false;
                unselectAll();
            }
        });
        btnZoomSelect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                GIS.this.selectSeparate = false;
                Envelope2D env = new Envelope2D();
                System.out.println(" " + minX + " " + minY + " " + maxX + " " + maxY);
                env.setFrameFromDiagonal(minX, minY, maxX, maxY);
                getMapPane().setDisplayArea(env);
            }
        });

        btnSelectSeparate.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                GIS.this.selectSeparate = true;
                GIS.this.getMapPane().setCursorTool(new SelectManyTool(GIS.this));
            }
        });
        initMenu();
        setMapContext(map);
        initQuery();
    }

    private void initQuery() {
        JPanel queryPanel = new JPanel(new BorderLayout());


        JButton data = new JButton("Data");
        //showOnMap = new Checkbox("Show on map");
        text = new JTextField(80);
        text.setText("include"); // include selects everything!
        JPanel panel = new JPanel();
        panel.add(text);
        panel.add(data);
        panel.add(showOnMap);
        queryPanel.add(panel, BorderLayout.NORTH);
        //queryPanel.add(data, BorderLayout.EAST);
        table = new JTable();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setModel(new DefaultTableModel(0, 0));
        table.setPreferredScrollableViewportSize(new Dimension(500, 200));
        JScrollPane scrollPane = new JScrollPane(table);
        queryPanel.add(scrollPane, BorderLayout.CENTER);

        data.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                executeQuery(GIS.this.text.getText());
            }
        });

        table.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
                unselectAll();
                GIS.this.setGeometry(GIS.this.getMapContext().getLayer(GIS.this.getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor());
                int[] selectedRows;
                if (showOnMap.getState()) {
                    GIS.this.table.selectAll();
                }
                selectedRows = GIS.this.table.getSelectedRows();
                Set<FeatureId> IDs = new HashSet<FeatureId>();

                FeatureCollection selectedFeatures = new ListFeatureCollection((SimpleFeatureType) GIS.this.getMapContext().getLayer(GIS.this.getSelectedLayerIndex()).getFeatureSource().getSchema());
                for (int i = 0; i < selectedRows.length; i++) {
                    String featureId = (String) GIS.this.table.getValueAt(selectedRows[i], 0);
                    //FeatureId id = new FeatureIdImpl(featureId);
                    try {
                        FeatureCollection features = GIS.this.getMapContext().getLayer(GIS.this.getSelectedLayerIndex()).getFeatureSource().getFeatures();
                        FeatureIterator iterator = features.features();
                        minX = maxX = minY = minX = -1;
                        while (iterator.hasNext()) {
                            Feature feature = iterator.next();
                            if (feature.getIdentifier().getID().equals(featureId)) {
                                BoundingBox bounds = feature.getBounds();
                                //see if this feature expands zooming area
                                maxX = (maxX == -1 || bounds.getMaxX() > maxX) ? bounds.getMaxX() : maxX;
                                maxY = (maxY == -1 || bounds.getMaxY() > maxY) ? bounds.getMaxY() : maxY;
                                minX = (minX == -1 || bounds.getMinX() < minX) ? bounds.getMinX() : minX;
                                minY = (minY == -1 || bounds.getMinY() < minY) ? bounds.getMinY() : minY;

                                selectedFeatures.add(feature);
                                break;
                            }
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                Class<?> clazz = GIS.this.getMapContext().getLayer(GIS.this.getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor().getType().getBinding();
                if (Point.class.isAssignableFrom(clazz)) {
                    minX = (minX < 5) ? 0 : minX - 5;
                    minY = (minY < 5) ? 0 : minY - 5;
                    maxX += 5;
                    maxY += 5;
                }
                displaySelectedFeatures(IDs);
            }
        });
        //queryPanel.add(new JLabel("testas"),BorderLayout.SOUTH);
        this.getContentPane().add(queryPanel, BorderLayout.SOUTH);
        this.pack();
        //Component[] comps = this.getComponents();
        //for(int i = 0; i < comps.length; i++){
        //	 if(comps[i] instanceof StatusBar){
        //		 System.out.println("Radau");
        //	 }
        //}
        //this.pack();
        //this.ena
    }

    private void executeQuery(String query) {
        int selectedIndex = this.getSelectedLayerIndex();
        if (selectedIndex == -1) {
            JOptionPane.showMessageDialog(this, "Please select a layer");
            return;
        }

        SimpleFeatureSource source = (SimpleFeatureSource) this.getMapContext().getLayer(selectedIndex).getFeatureSource();

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();
        try {
            org.opengis.filter.Filter filter = CQL.toFilter(query);

            Query query_ = new Query(schema.getName().getLocalPart(), filter, new String[]{name});

            SimpleFeatureCollection features = source.getFeatures(filter);

            FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
            table.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Redraws map
     */
    public void unselectAll() {
        MapLayer[] layers = this.getMapContext().getLayers();
        for (int i = 0; i < layers.length; i++) {
            setGeometry(layers[i].getFeatureSource().getSchema().getGeometryDescriptor());
            Style style = createSelectedStyle(new HashSet<FeatureId>());
            layers[i].setStyle(style);
        }
        this.repaint();
        try {
            Thread.sleep(300);
        } catch (Exception e) {
        }
    }

    /**
     * This method is called by our feature selection tool when the user has
     * clicked on the map.
     *
     * @param pos map (world) coordinates of the mouse cursor
     */
    public void selectFeatures(Rectangle rec) {
        System.out.println("Rectangle: " + rec.getX() + ":" + rec.getY());
        int selectedIndex = this.getSelectedLayerIndex();
        //pravalo pries tai seletinto layerio spalvas
        if (selectedIndex != lastSelected) {
            unselectAll();
        }
        lastSelected = selectedIndex;
        if (selectedIndex == -1) {
            JOptionPane.showMessageDialog(this, "Please select a layer");
            return;
        }
        System.out.println("Selected: " + this.getSelectedLayerIndex());
        setGeometry(this.getMapContext().getLayer(this.getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor());
        /*
         * Construct a 5x5 pixel rectangle centered on the mouse click position
         */
        //java.awt.Point screenPos = ev.getPoint();
        Rectangle screenRect = rec;//new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);

        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive
         * method here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = this.getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        System.out.println(worldRect);
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                this.getMapContext().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with the bounding
         * box
         */
        Intersects filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(bbox));

        /*
         * Use the filter to identify the selected features
         */
        try {
            SimpleFeatureCollection selectedFeatures = //this.getLayeredPane().get 
                    (SimpleFeatureCollection) this.getMapContext().getLayer(selectedIndex).getFeatureSource().getFeatures(filter);

            if (selectSeparate) {
                if (this.selectedFeatures != null) {
                    this.selectedFeatures.addAll(selectedFeatures);
                }
                selectedFeatures = this.selectedFeatures;
            } else {
                this.selectedFeatures = FeatureCollections.newCollection();
            }
            FeatureCollectionTableModel model = new FeatureCollectionTableModel(selectedFeatures);
            table.setModel(model);
            FeatureIterator iter = selectedFeatures.features();
            Set<FeatureId> IDs = getSelectedFeatureIDs(selectedFeatures);

            if (IDs.isEmpty()) {
                System.out.println("   no feature selected");
            }

            displaySelectedFeatures(IDs);

        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private Set<FeatureId> getSelectedFeatureIDs(FeatureCollection selectedFeatures) {
        FeatureIterator iter = selectedFeatures.features();
        Set<FeatureId> IDs = new HashSet<FeatureId>();
        maxY = maxX = minY = minX = -1;
        try {
            while (iter.hasNext()) {
                Feature feature = iter.next();
                BoundingBox bounds = feature.getBounds();
                //see if this feature expands zooming area
                maxX = (maxX == -1 || bounds.getMaxX() > maxX) ? bounds.getMaxX() : maxX;
                maxY = (maxY == -1 || bounds.getMaxY() > maxY) ? bounds.getMaxY() : maxY;
                minX = (minX == -1 || bounds.getMinX() < minX) ? bounds.getMinX() : minX;
                minY = (minY == -1 || bounds.getMinY() < minY) ? bounds.getMinY() : minY;

                IDs.add(feature.getIdentifier());

                System.out.println("   " + feature.getIdentifier());
            }
            Class<?> clazz = GIS.this.getMapContext().getLayer(GIS.this.getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor().getType().getBinding();
            if (Point.class.isAssignableFrom(clazz)) {
                minX = (minX < 5) ? 0 : minX - 5;
                minY = (minY < 5) ? 0 : minY - 5;
                maxX += 5;
                maxY += 5;
            }

        } finally {
            iter.close();
        }
        return IDs;
    }

    public void display() {
        setSize(1200, 600);
        setVisible(true);
    }

    public void initMenu() {
        JMenuBar menubar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");

        JMenuItem menuItem = new JMenuItem("Add layer");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        menuItem = new JMenuItem("Add layers from dir");
        menuItem.addActionListener(this);
        fileMenu.add(menuItem);

        menubar.add(fileMenu);


        setJMenuBar(menubar);
        //myFrame.getContentPane().add(menu);

    }

    /**
     * Sets the display to paint selected features yellow and unselected
     * features in the default style.
     *
     * @param IDs identifiers of currently selected features
     */
    public void displaySelectedFeatures(Set<FeatureId> IDs) {
        Style style;

        if (IDs.isEmpty()) {
            style = this.getMapContext().getLayer(this.getSelectedLayerIndex()).getStyle();
            //createDefaultStyle();

        } else {
            style = createSelectedStyle(IDs);
        }

        this.getMapContext().getLayer(this.getSelectedLayerIndex()).setStyle(style);
        //this.getMapPane().repaint();
        this.repaint();
        /*
         * MapLayer[] layers = getMapContext().getLayers(); for(int i = 0; i <
         * layers.length; i++){ layers[i].setStyle(style); }
        getMapPane().repaint();
         */
    }

    /**
     * Create a default Style for feature display
     */
    private Style createDefaultStyle() {
        //FeatureTypeStyle fts = sf.createFeatureTypeStyle();

        //Rule rule = createRule(LINE_COLOUR, FILL_COLOUR);
        //fts.rules().add(rule);


        Style style = sf.createStyle();
        //style.featureTypeStyles().add(fts);
        return style;
    }

    /**
     * Create a Style where features with given IDs are painted yellow, while
     * others are painted with the default colors.
     */
    private Style createSelectedStyle(Set<FeatureId> IDs) {
        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        Rule selectedRule = createRule(SELECTED_COLOUR, SELECTED_COLOUR);
        selectedRule.setFilter(ff.id(IDs));
        fts.rules().add(selectedRule);

        Rule otherRule = createRule(LINE_COLOUR, FILL_COLOUR);
        otherRule.setElseFilter(true);
        fts.rules().add(otherRule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    /**
     * Helper for createXXXStyle methods. Creates a new Rule containing a
     * Symbolizer tailored to the geometry type of the features that we are
     * displaying.
     */
    private Rule createRule(Color outlineColor, Color fillColor) {
        Symbolizer symbolizer = null;
        Fill fill = null;
        Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(LINE_WIDTH));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
        }

        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }

    private void setGeometry(GeometryDescriptor gd) {
        GeometryDescriptor geomDesc = gd;//this.getMapContext().getLayer(this.getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor();
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz)
                || MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeomType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz)
                || MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeomType.LINE;

        } else {
            geometryType = GeomType.POINT;
        }

    }

    private void addLayer(File shapeFile) {
        try {
            //AbstractGridFormat format = GridFormatFinder.findFormat( shapeFile );        
            //AbstractGridCoverage2DReader reader = format.getReader(shapeFile);
            //if(reader != null){
            //	this.getMapContext().addLayer(reader, null);
            //	return;
            //}
            FileDataStore store = FileDataStoreFinder.getDataStore(shapeFile);
            FeatureSource featureSource = store.getFeatureSource();
            this.getMapContext().addLayer(featureSource, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        //Kazkas pasirinkta is meniu
        if (e.getSource() instanceof JMenuItem) {
            JMenuItem source = (JMenuItem) (e.getSource());
            if ("Add layer".equals(source.getText())) {
                File file = JFileDataStoreChooser.showOpenFile("shp", null);
                //TODO test
                //file = new File("/home/antanas/Unikas/gis/duomenai/LT250shp/Keliai.shp");//JFileDataStoreChooser.showOpenFile("shp", null);
                //File file2 = new File("/home/antanas/Unikas/gis/duomenai/LT250shp/Ezerai.shp");
                //addLayer(file2);
                if (file != null) {
                    addLayer(file);
                }
            } else if ("Add layers from dir".equals(source.getText())) {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.showOpenDialog(this);
                File files = chooser.getSelectedFile();
                if (files != null && files.isDirectory()) {
                    File[] file = files.listFiles(new ShapeFileFilter());
                    for (int i = 0; i < file.length; i++) {
                        addLayer(file[i]);
                    }
                }
            }
        }

    }
}