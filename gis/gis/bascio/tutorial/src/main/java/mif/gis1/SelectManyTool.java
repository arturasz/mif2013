package mif.gis1;

import java.awt.Cursor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.Point2D;

import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.JMapPane;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;

import static java.lang.Math.abs;

public class SelectManyTool extends CursorTool {
    
    private final Point startPosDevice;
    private final Point2D startPosWorld;
    private final GIS frame;
    private boolean dragged;
    
    /**
     * Constructor
     */
    public SelectManyTool(GIS frame) {
    	this.frame = frame;
    	startPosDevice = new Point();
        startPosWorld = new DirectPosition2D();
        dragged = false;
    }
    
    /**
     * Zoom in by the currently set increment, with the map
     * centred at the location (in world coords) of the mouse
     * click
     * 
     * @param e map mapPane mouse event
     */
    @Override
    public void onMouseClicked(MapMouseEvent e) {
        //Rectangle paneArea = getMapPane().getVisibleRect();
        
        java.awt.Point screenPos = e.getPoint();
        Rectangle rec = new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);
        this.frame.selectFeatures(rec);
        //DirectPosition2D mapPos = e.getMapPosition();
        /*
        double scale = getMapPane().getWorldToScreenTransform().getScaleX();
        double newScale = scale * zoom;

        DirectPosition2D corner = new DirectPosition2D(
                mapPos.getX() - 0.5d * paneArea.getWidth() / newScale,
                mapPos.getY() + 0.5d * paneArea.getHeight() / newScale);
        
        Envelope2D newMapArea = new Envelope2D();
        newMapArea.setFrameFromCenter(mapPos, corner);
        getMapPane().setDisplayArea(newMapArea);*/
    }
    
    /**
     * Records the map position of the mouse event in case this
     * button press is the beginning of a mouse drag
     *
     * @param ev the mouse event
     */
    @Override
    public void onMousePressed(MapMouseEvent ev) {
        startPosDevice.setLocation(ev.getPoint());
        startPosWorld.setLocation(ev.getMapPosition());
    }

    /**
     * Records that the mouse is being dragged
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseDragged(MapMouseEvent ev) {
        dragged = true;
    }

    /**
     * If the mouse was dragged, determines the bounds of the
     * box that the user defined and passes this to the mapPane's
     * {@link org.geotools.swing.JMapPane#setDisplayArea(org.opengis.geometry.Envelope) }
     * method
     *
     * @param ev the mouse event
     */
    @Override
    public void onMouseReleased(MapMouseEvent ev) {
        if (dragged && !ev.getPoint().equals(startPosDevice)) {
            //Envelope2D env = new Envelope2D();
            //env.setFrameFromDiagonal(startPosWorld, ev.getMapPosition());
            Rectangle rec = new Rectangle((int)startPosDevice.getX(),
            		(int)startPosDevice.getY(),
            		abs((int)(ev.getX() - startPosDevice.getX())), abs((int)(ev.getY() - startPosDevice.getY())));
            System.out.println(rec.toString());
            dragged = false;
            frame.selectFeatures(rec);
            //getMapPane().setDisplayArea(env);
        }
    }

    /**
     * Returns true to indicate that this tool draws a box
     * on the map display when the mouse is being dragged to
     * show the zoom-in area
     */
    @Override
    public boolean drawDragBox() {
        return true;
    }

}
