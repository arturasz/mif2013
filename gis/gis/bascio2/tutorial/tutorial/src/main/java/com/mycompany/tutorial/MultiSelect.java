/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tutorial;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;

/**
 * @author lonelystar
 * Date: 2012-03-17, 14.53.23
 */
public class MultiSelect extends CursorTool{
    
    private final Point startPosDevice;
    private final Point2D startPosWorld;
    private boolean dragged;
    private final SelectionLab map;
    
    public MultiSelect(SelectionLab map) {

    	this.startPosDevice = new Point();
        this.startPosWorld = new DirectPosition2D();
        this.dragged = false;
        this.map = map;
    }
    
    @Override
    public void onMouseClicked(MapMouseEvent e) {
        //Rectangle paneArea = getMapPane().getVisibleRect();
        
        java.awt.Point screenPos = e.getPoint();
        Rectangle rec = new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);
        this.map.selectMultiFeatures(rec,true);
        //DirectPosition2D mapPos = e.getMapPosition();
        /*
        double scale = getMapPane().getWorldToScreenTransform().getScaleX();
        double newScale = scale * zoom;

        DirectPosition2D corner = new DirectPosition2D(
                mapPos.getX() - 0.5d * paneArea.getWidth() / newScale,
                mapPos.getY() + 0.5d * paneArea.getHeight() / newScale);
        
        Envelope2D newMapArea = new Envelope2D();
        newMapArea.setFrameFromCenter(mapPos, corner);
        getMapPane().setDisplayArea(newMapArea);*/
    }
    
    @Override
    public void onMousePressed(MapMouseEvent ev) {
        startPosDevice.setLocation(ev.getPoint());
        startPosWorld.setLocation(ev.getMapPosition());
    }
    
    @Override
    public void onMouseDragged(MapMouseEvent ev) {
        dragged = true;
    }
    
    @Override
    public void onMouseReleased(MapMouseEvent ev) {
        if (dragged && !ev.getPoint().equals(startPosDevice)) {
            //Envelope2D env = new Envelope2D();
            //env.setFrameFromDiagonal(startPosWorld, ev.getMapPosition());
            Rectangle rec = new Rectangle((int)startPosDevice.getX(),
            		(int)startPosDevice.getY(),
            		((int)(ev.getX() - startPosDevice.getX())), ((int)(ev.getY() - startPosDevice.getY())));
            System.out.println(rec.toString());
            dragged = false;
            this.map.selectMultiFeatures(rec,true);
            //getMapPane().setDisplayArea(env);
        }
    }
    
    @Override
    public boolean drawDragBox() {
        return true;
    }

}
