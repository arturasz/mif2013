<UserStyle>
	<FeatureTypeStyle>
		<Rule>
			<LineSymbolizer>
				<Stroke>
					<CssParameter name="stroke">#00FF00</CssParameter>
					<CssParameter name="width">2.0</CssParameter>
				</Stroke>
			</LineSymbolizer>
			<PolygonSymbolizer>
				<Fill>
					<CssParameter name="fill">#82FF82</CssParameter>
					<CssParameter name="fill-opacity">0.8</CssParameter>
				</Fill>
			</PolygonSymbolizer>
		</Rule>
	</FeatureTypeStyle>
</UserStyle>