<UserStyle>
	<FeatureTypeStyle>
		<Rule>
			<LineSymbolizer>
				<Stroke>
					<CssParameter name="stroke">#0000FF</CssParameter>
					<CssParameter name="width">2.0</CssParameter>
				</Stroke>
			</LineSymbolizer>
			<PolygonSymbolizer>
				<Fill>
					<CssParameter name="fill">#8282FF</CssParameter>
					<CssParameter name="fill-opacity">0.8</CssParameter>
				</Fill>
			</PolygonSymbolizer>
		</Rule>
	</FeatureTypeStyle>
</UserStyle>