<UserStyle>
	<FeatureTypeStyle>
		<Rule>
			<!--PointSymbolizer>
				<Stroke>
					<CssParameter name="stroke">#000000</CssParameter>
					<CssParameter name="width">0.7</CssParameter>
				</Stroke>
				<Fill>
					<CssParameter name="fill">#000000</CssParameter>
					<CssParameter name="fill-opacity">1</CssParameter>
				</Fill>
				<Size>1.0</Size>
			</PointSymbolizer-->
			
			<PointSymbolizer> 
				<Graphic>
					<Mark>
						<WellKnownName>circle</WellKnownName>
						<Fill>
							<CssParameter name="fill">#CD2626</CssParameter>
						</Fill>
						<Stroke>
							<CssParameter name="stroke">#802A2A</CssParameter>
						</Stroke>
					</Mark>
					<Opacity>1.0</Opacity>
					<Size>3.0</Size> 
				</Graphic> 
			</PointSymbolizer>     
		    
		</Rule>
	</FeatureTypeStyle>
</UserStyle>