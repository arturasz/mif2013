<UserStyle>
	<FeatureTypeStyle>
		<Rule>
			<LineSymbolizer>
				<Stroke>
					<CssParameter name="stroke">#00FF00</CssParameter>
					<CssParameter name="width">1.5</CssParameter>
				</Stroke>
			</LineSymbolizer>
			<PolygonSymbolizer>
				<Fill>
					<CssParameter name="fill">#82ff82</CssParameter>
					<CssParameter name="fill-opacity">0.5</CssParameter>
				</Fill>
			</PolygonSymbolizer>
		</Rule>
	</FeatureTypeStyle>
</UserStyle>