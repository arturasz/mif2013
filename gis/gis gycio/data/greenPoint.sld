<UserStyle>
	<FeatureTypeStyle>
		<Rule>
			<PointSymbolizer> 
				<Graphic>
					<Mark>
						<WellKnownName>circle</WellKnownName>
						<Fill>
							<CssParameter name="fill">#82ff82</CssParameter>
						</Fill>
						<Stroke>
							<CssParameter name="stroke">#000000</CssParameter>
						</Stroke>
					</Mark>
					<Opacity>1.0</Opacity>
					<Size>6.0</Size> 
				</Graphic> 
			</PointSymbolizer>     
		</Rule>
	</FeatureTypeStyle>
</UserStyle>