/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02;

import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.Polygon;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.factory.GeoTools;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.crs.DefaultGeographicCRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.PropertyName;
import org.opengis.filter.identity.FeatureId;

/**
 *
 * @author Benamis
 */
public class FeatureCollectionOperation {

    static public FeatureCollection<SimpleFeatureType, SimpleFeature> filterCql(FeatureCollection<SimpleFeatureType, SimpleFeature> dataLayerFeatureCollection, String cqlQuery) {
        FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());

        if (cqlQuery.isEmpty()) {
            return null;
        }

        List<Filter> filterList = null;
        try {
            filterList = CQL.toFilterList(cqlQuery);
            Filter filter = filterFactory.and(filterList);
            return dataLayerFeatureCollection.subCollection(filter);
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    static public FeatureCollection<SimpleFeatureType, SimpleFeature> filterId(FeatureCollection<SimpleFeatureType, SimpleFeature> dataLayerFeatureCollection, String localName, Integer[] objectIds) {
        FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(null);
        Set<FeatureId> featureIds = new HashSet<FeatureId>();

        if (localName.isEmpty()) {
            return null;
        }
        if (objectIds.length == 0) {
            return null;
        }
        for(int i = 0; i < objectIds.length; i++) {
            FeatureId fid = filterFactory.featureId(localName+"."+objectIds[i]);
            featureIds.add(fid);
        }
        Filter filter = filterFactory.id(featureIds);
        try {
            return dataLayerFeatureCollection.subCollection(filter);
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    static public FeatureCollection filterArea(FeatureCollection<SimpleFeatureType, SimpleFeature> dataLayerFeatureCollection, String geometryDescriptor, double minX, double maxX, double minY, double maxY) {
        ReferencedEnvelope selectionArea = new ReferencedEnvelope(minX, maxX, minY, maxY, DefaultGeographicCRS.WGS84);
        Polygon polygon = JTS.toGeometry((Envelope)selectionArea);
        FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(GeoTools.getDefaultHints());

        PropertyName property = filterFactory.property(geometryDescriptor);
        Filter filterBBox = (Filter)filterFactory.bbox(property, selectionArea);
        Filter filterPolygon = (Filter)filterFactory.not(filterFactory.disjoint(property, filterFactory.literal(polygon)));
        Filter filter = (Filter)filterFactory.and(filterBBox, filterPolygon);
        try {
            return dataLayerFeatureCollection.subCollection(filter);
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    static private SimpleFeatureType getLineFeatureType() {
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        typeBuilder.setName("Line");
        typeBuilder.add("ID", Integer.class);
        typeBuilder.add("the_geom", LineString.class);
        typeBuilder.add("length", Double.class);
        return typeBuilder.buildFeatureType();
    }

    static private SimpleFeatureType getColidedLineFeatureType() {
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        typeBuilder.setName("SurfaceLine");
        typeBuilder.add("ID", Integer.class);
        typeBuilder.add("the_geom", LineString.class);
        typeBuilder.add("length", Double.class);
        typeBuilder.add("height", Double.class);
        return typeBuilder.buildFeatureType();
    }

    static public FeatureCollection<SimpleFeatureType, SimpleFeature> lineFeatureCollection(Geometry[] lines) {

        FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = FeatureCollections.newCollection();
        SimpleFeatureType featureType = getLineFeatureType();
        for (int i = 0; i < lines.length; i++) {
            SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);
            featureBuilder.add(featureCollection.size()+1);
            featureBuilder.add(lines[i]);
            featureBuilder.add(lines[i].getLength());
            SimpleFeature lineFeature = featureBuilder.buildFeature("Line."+(featureCollection.size()+1));
            featureCollection.add(lineFeature);
        }
        return featureCollection;

    }

    static public FeatureCollection<SimpleFeatureType, SimpleFeature> colideLinesOnPolygons(FeatureCollection<SimpleFeatureType, SimpleFeature> cableCollection, FeatureCollection<SimpleFeatureType, SimpleFeature> surfaceCollection) {
        FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = FeatureCollections.newCollection();
        SimpleFeatureType featureType = getColidedLineFeatureType();
        
        FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(null);
        PropertyName property = filterFactory.property(surfaceCollection.getSchema().getGeometryDescriptor().getLocalName());

        FeatureIterator<SimpleFeature> cableIterator = cableCollection.features();
        while (cableIterator.hasNext()) {
            SimpleFeature feature = cableIterator.next();
            Geometry featureGeometry = (Geometry)feature.getDefaultGeometry();
            Filter filterBBox = (Filter)filterFactory.bbox(property, JTS.toEnvelope(featureGeometry));
            Filter filterPolygon = (Filter)filterFactory.not(filterFactory.disjoint(property, filterFactory.literal(featureGeometry)));
            Filter filter = (Filter)filterFactory.and(filterBBox, filterPolygon);
            FeatureCollection<SimpleFeatureType, SimpleFeature> fc = surfaceCollection.subCollection(filter);
            FeatureIterator<SimpleFeature> surfaceIterator = fc.features();
            while (surfaceIterator.hasNext()) {
                SimpleFeature surfaceElement = surfaceIterator.next();
                Geometry subLineString = ((Geometry)surfaceElement.getDefaultGeometry()).intersection(featureGeometry);
                if (subLineString.getLength() > 0) {
                    SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);
                    featureBuilder.add(featureCollection.size()+1);
                    featureBuilder.add(subLineString);
                    featureBuilder.add(subLineString.getLength());
                    featureBuilder.add(surfaceElement.getAttribute("Aukstis"));
                    SimpleFeature lineFeature = featureBuilder.buildFeature("SurfaceLine."+(featureCollection.size()+1));
                    featureCollection.add(lineFeature);
                }
            }
        }
        return featureCollection;
    }
}
