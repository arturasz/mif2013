/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui;

import gis02.gui.listener.LayerListListener;
import gis02.gui.model.CheckBoxListModel;
import gis02.tools.VectorUtils;
import java.io.File;
import javax.swing.JList;
import javax.swing.event.ListSelectionListener;
import java.awt.Color;
import javax.swing.UIDefaults;
import javax.swing.event.ListSelectionEvent;
import javax.swing.JComponent;
import javax.swing.ListCellRenderer;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JCheckBox;
import java.awt.BorderLayout;
import java.awt.Component;
import java.util.Vector;
import javax.swing.AbstractListModel;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

public class CheckBoxList extends JList
    implements ListSelectionListener {

    static Color listForeground, listBackground,
        listSelectionForeground,
        listSelectionBackground;
    static {
        UIDefaults uid = UIManager.getLookAndFeel().getDefaults();
        listForeground =  uid.getColor ("List.foreground");
        listBackground =  uid.getColor ("List.background");
        listSelectionForeground =  uid.getColor ("List.selectionForeground");
        listSelectionBackground =  uid.getColor ("List.selectionBackground");
    }

    private Vector<Integer> newSelections = new Vector<Integer>();
    private Vector<Integer> currentSelections = new Vector<Integer>();
    private Vector<LayerListListener> layerListenerList = new Vector<LayerListListener>();

    public CheckBoxList() {
        super();
        setCellRenderer (new CheckBoxListCellRenderer());
        addListSelectionListener (this);
    }

    public void setModel(CheckBoxListModel model) {
        setModel((AbstractListModel)model);
        
        model.addListDataListener(new ListDataListener() {
            public void contentsChanged(ListDataEvent e) {
            }
            public void intervalRemoved(ListDataEvent e) {
            }
            public void intervalAdded(ListDataEvent e) {
            }
        });
        
    }

    public Vector<Integer> getSelection() {
        return currentSelections;
    }

    public void resetSelection(Vector<Integer> selection) {
        removeListSelectionListener (this);
        currentSelections = selection;
        removeSelectionInterval(0, getModel().getSize());
        addListSelectionListener (this);
        //System.out.println("resetto:"+selection);
    }

    // ListSelectionListener implementation
    public void valueChanged (ListSelectionEvent e) {

        if (! e.getValueIsAdjusting()) {
            //System.out.println("current:"+currentSelections);
            removeListSelectionListener (this);
            newSelections.clear();
            int size = getModel().getSize();
            for (int i=0; i<size; i++) {
                if (getSelectionModel().isSelectedIndex(i)) {
                    newSelections.add (new Integer(i));
                }
            }
            //System.out.println("event:"+newSelections);

            Vector<Integer> select = VectorUtils.difference(newSelections, currentSelections);
            Vector<Integer> unselect = VectorUtils.intersection(newSelections, currentSelections);
            currentSelections = VectorUtils.union(select, VectorUtils.minus(currentSelections, unselect));
            //System.out.println("select:"+select);
            //System.out.println("unselect:"+unselect);
            //System.out.println("current:"+currentSelections);

            removeSelectionInterval(0, getModel().getSize());
            addListSelectionListener(this);
            for (int i = 0; i < select.size(); i++) {
                fireSelectedEvent(((CheckBoxListModel)getModel()).getFilenameAt(select.get(i)));
            }
            for (int i = 0; i < unselect.size(); i++) {
                fireUnselectedEvent(((CheckBoxListModel)getModel()).getFilenameAt(unselect.get(i)));
            }
        }

    }

    public void addLayerListListener(LayerListListener lll) {
        if (lll != null) {
            layerListenerList.add(lll);
        }
    }

    public void fireSelectedEvent(Object layerKey) {
    	if (layerListenerList != null) {
            Object[] list = layerListenerList.toArray();
            for (int i = 0; i < list.length; i++) {
                LayerListListener target = (LayerListListener)list[i];
                target.layerSelected((File) layerKey);
            }
        }
	}

    public void fireUnselectedEvent(Object layerKey) {
    	if (layerListenerList != null) {
            Object[] list = layerListenerList.toArray();
            for (int i = 0; i < list.length; i++) {
                LayerListListener target = (LayerListListener)list[i];
                target.layerUnselected((File) layerKey);
            }
        }
    }

    class CheckBoxListCellRenderer extends JComponent
        implements ListCellRenderer {
        DefaultListCellRenderer defaultComp;
        JCheckBox checkbox;
        public CheckBoxListCellRenderer() {
            setLayout (new BorderLayout());
            defaultComp = new DefaultListCellRenderer();
            checkbox = new JCheckBox();
            add (checkbox, BorderLayout.WEST);
            add (defaultComp, BorderLayout.CENTER);
        }

        public Component getListCellRendererComponent(JList list,
                                                      Object  value,
                                                      int index,
                                                      boolean isSelected,
                                                      boolean cellHasFocus){
            defaultComp.getListCellRendererComponent (list, value, index,
                                                      false, cellHasFocus);
            isSelected = currentSelections.contains(new Integer(index));
            checkbox.setSelected (isSelected);
            Component[] comps = getComponents();
            for (int i=0; i<comps.length; i++) {
                comps[i].setForeground (listForeground);
                comps[i].setBackground (listBackground);
            }
            return this;
        }
    }
}
