/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui;

import gis02.LayerCollection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.KeyStroke;

/**
 *
 * @author Benamis
 */
public class DropLayerFrame extends JFrame {
    private JComboBox dropLayerSelect = new JComboBox();
    private JButton cancelDropButton = new JButton("Cancel");
    private JButton okDropButton = new JButton("OK");
    private LayerCollection layerCollection;
    private Vector<File> filenames;
    private Vector<String> names;

    public DropLayerFrame(final LayerCollection layerCollection) {
        setTitle("Select layer to remove");
        this.layerCollection = layerCollection;
        initComponents();
        attachListeners();
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (visible) {
            filenames = layerCollection.getLayerKeys();
            names = new Vector<String>();
            for (int i = 0; i < filenames.size(); i++) {
                names.add(filenames.get(i).getName());
            }
            dropLayerSelect.setModel(new DefaultComboBoxModel(names));
        }
    }

    private void dropLayer() {
        int i = dropLayerSelect.getSelectedIndex();
        layerCollection.removeDataLayer(filenames.get(i));
    }

    private void attachListeners() {        
        ActionListener okActionListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dropLayer();
                setVisible(false);
            }
        };
        okDropButton.addActionListener(okActionListener);
        KeyStroke enter = KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true);
        okDropButton.registerKeyboardAction(okActionListener, enter, JComponent.WHEN_IN_FOCUSED_WINDOW);

        ActionListener cancelActionListener = new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               setVisible(false);
           }
        };
        cancelDropButton.addActionListener(cancelActionListener);
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);
        cancelDropButton.registerKeyboardAction(cancelActionListener, escape, JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    private void initComponents() {
        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        setResizable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dropLayerSelect, 0, 380, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(okDropButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cancelDropButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(dropLayerSelect, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cancelDropButton)
                    .addComponent(okDropButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        // </editor-fold>
    }
}
