/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui.model;

import gis02.LayerCollection;
import gis02.gui.CheckBoxList;
import gis02.gui.listener.LayerCollectionListener;
import java.io.File;
import java.util.Vector;
import javax.swing.AbstractListModel;
import javax.swing.DefaultListModel;
import org.geotools.feature.FeatureCollection;
/**
 *
 * @author Benamis
 */
public class CheckBoxListModel extends AbstractListModel {
    DefaultListModel a = new DefaultListModel();
    private CheckBoxList layerList = null;
    private Vector<File> filenames = new Vector();

    public CheckBoxListModel(LayerCollection layerCollection, CheckBoxList layerList) {
        this.layerList = layerList;
        layerCollection.addListener(new LayerCollectionListener(){
            public void actionDataLayerAdded(File layerKey, boolean visible, double mapMinX, double mapMaxX, double mapMinY, double mapMaxY) {
                addElement(layerKey, visible);
            }

            public void actionDataLayerRemoved(File layerKey) {
                removeElement(layerKey);
            }

            public void actionVisibilityChanged(File layerKey, boolean visibility) { }
            public void actionSelectionLayerChanged(File layerKey) {}
            public void actionVisibleMapChanged() { }
        });
    }

    public int getSize() {
        return filenames.size();
    }

    public Object getElementAt(int i) {
        return filenames.get(i).getName();
    }

    public File getFilenameAt(int i) {
        return filenames.get(i);
    }

    public Vector<File> getAllFilenames() {
        return filenames;
    }

    public void addElement(File layerKey, boolean addSelected) {
        filenames.insertElementAt(layerKey, 0);
        Vector<Integer> selected = layerList.getSelection();
        for (int i = 0; i < selected.size(); i++) {
            selected.set(i, selected.get(i)+1);
        }
        if (addSelected) {
            selected.add(new Integer(0));
        }
        fireIntervalAdded(this, 0, 0);
        layerList.resetSelection(selected);
    }

    public boolean removeElement(File layerKey) {
        int index = filenames.indexOf(layerKey);
        if (index >= 0) {
            filenames.remove(index);
            Vector<Integer> selected = layerList.getSelection();
            for (int i = index; i < selected.size(); i++) {
                selected.set(i, selected.get(i)-1);
            }
            fireIntervalRemoved(this, index, index);
            layerList.resetSelection(selected);
            return true;
        } else {
            return false;
        }
    }
}
