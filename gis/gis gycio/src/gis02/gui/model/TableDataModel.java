/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui.model;

import gis02.LayerCollection;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import javax.swing.table.AbstractTableModel;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.opengis.feature.Feature;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

/**
 *
 * @author Benamis
 */
public class TableDataModel extends AbstractTableModel {

    private Vector<Vector<Object>> data = new Vector<Vector<Object>>();
    private Vector<Vector<Object>> resultData = new Vector<Vector<Object>>();
    private HashMap<String,Integer> objectIdToIndex = new HashMap<String,Integer>();
    private Vector<String> columns = new Vector<String>();
    private File layerKey;
    private LayerCollection layerCollection;
    private boolean showSelectedOnly = false;
    private String idProperty;
    
    public TableDataModel(LayerCollection layerCollection, File layerKey) {
        this.layerKey = layerKey;
        this.layerCollection = layerCollection;
        readColumns();
        readData();
    }

    private void readColumns() {
        columns.add("SELECTED");
        FeatureCollection<SimpleFeatureType,SimpleFeature> featureCollection = layerCollection.getDataLayerFeatureCollection(layerKey);
        FeatureIterator featureIterator = featureCollection.features();
        if (!featureIterator.hasNext()) {
            return;
        }
        Feature feature = featureIterator.next();
        Collection<Property> properties = feature.getProperties();
        Iterator propertyIterator = properties.iterator();

        idProperty = "ID";
        while (propertyIterator.hasNext()) {
            Property property = (Property) propertyIterator.next();
            if (!property.getName().toString().equals("the_geom")) {
                columns.add(property.getName().toString());
            }
            if (property.getName().toString().equals("OBJECTID")) {
                idProperty = "OBJECTID";
            }
        }
        featureCollection.close(featureIterator);
    }

    private void readData() {
        FeatureCollection<SimpleFeatureType,SimpleFeature> featureCollection = layerCollection.getDataLayerFeatureCollection(layerKey);
        FeatureIterator featureIterator = featureCollection.features();
        if (!featureIterator.hasNext()) {
            return;
        }

        while (featureIterator.hasNext()) {
            Feature feature = featureIterator.next();
            Collection<Property> properties = feature.getProperties();
            Iterator<Property> propertyIterator = properties.iterator();
            Vector<Object> dataProperties = new Vector<Object>();
            dataProperties.add(new Boolean(false));
            while (propertyIterator.hasNext()) {
                Property property = (Property) propertyIterator.next();
                if (!property.getName().toString().equals("the_geom")) {
                    dataProperties.add(property.getValue().toString());
                }
            }
            String objectId = feature.getProperty(idProperty).getValue().toString();
            objectIdToIndex.put(objectId, data.size());
            data.add(dataProperties);
        }
        featureCollection.close(featureIterator);
        showSelectedOnly(showSelectedOnly);
    }

    @Override
    public String getColumnName(int col) {
        return columns.get(col);
    }

    public int getRowCount() { 
        return resultData.size();
    }

    public int getColumnCount() {
        return columns.size();
    }

    public Object getValueAt(int row, int col) {
        return resultData.get(row).get(col);
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return (col == 0);
    }

    @Override
    public Class getColumnClass(int c) {
        if (c == 0) {
            return Boolean.class;
        } else {
            return String.class;
        }
    }
    
    @Override
    public void setValueAt(Object value, int row, int col) {
        resultData.get(row).set(col, value);
        showSelectedOnly(showSelectedOnly);

        fireTableCellUpdated(row, col);
        if (col == 0) {
            Vector<Integer> objectIds = new Vector<Integer>();
            for (int i = 0; i < data.size(); i++) {
                if ((Boolean)data.get(i).get(0)) {
                    objectIds.add(new Integer(data.get(i).get(1).toString()));
                }
            }
            Integer[] idsArray = new Integer[objectIds.size()];
            for (int i = 0; i < objectIds.size(); i++) {
                idsArray[i] = objectIds.get(i);
            }
            layerCollection.reSelectDataLayer(layerKey, idsArray);
        }
    }

    public void showSelectedOnly(boolean selectedOnly) {
        showSelectedOnly = selectedOnly;
        resultData = new Vector<Vector<Object>>();
        if (showSelectedOnly) {
            for (int i = 0; i < data.size(); i++) {
                if ((Boolean)data.get(i).get(0) == true) {
                    resultData.add(data.get(i));
                }
            }
        } else {
            resultData = data;
        }
        fireTableDataChanged();
    }
    
    public void updateSelectionLayer(File layerKey) {
        for (int i = 0; i < data.size(); i++) {
            data.get(i).set(0, new Boolean(false));
        }
        FeatureCollection featureCollection = layerCollection.getSelectionLayerFeatureCollection(layerKey);
        if (featureCollection != null) {
            FeatureIterator<Feature> featureIterator = featureCollection.features();
            while (featureIterator.hasNext()) {
                String objectId = featureIterator.next().getProperty(idProperty).getValue().toString();
                int row = objectIdToIndex.get(objectId);
                data.get(row).set(0, new Boolean(true));
            }
            featureCollection.close(featureIterator);
        }
        showSelectedOnly(showSelectedOnly);
    }
}
