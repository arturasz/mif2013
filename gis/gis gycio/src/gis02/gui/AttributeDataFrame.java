/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * AttributeDataFrame.java
 *
 * Created on 2009.3.25, 09.38.16
 */

package gis02.gui;

import gis02.gui.model.TableDataModel;
import gis02.*;
import gis02.gui.listener.LayerCollectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Benamis
 */
public class AttributeDataFrame extends javax.swing.JFrame {

    private JTabbedPane layerPane;
    private HashMap<File,TableDataModel> dataModels = new HashMap<File,TableDataModel>();
    private HashMap<File,JPanel> panels = new HashMap<File,JPanel>();
    private HashMap<File,JTable> tables = new HashMap<File,JTable>();
    private LayerCollection layerCollection;

    public AttributeDataFrame(LayerCollection layerCollection) {
        this.setTitle("Attribute data");
        this.layerCollection = layerCollection;
        initComponents();
        layerCollection.addListener(new LayerCollectionListener() {
            public void actionDataLayerAdded(File layerKey, boolean visible, double mapMinX, double mapMaxX, double mapMinY, double mapMaxY) {
                addLayer(layerKey);
            }

            public void actionDataLayerRemoved(File layerKey) {
                removeLayer(layerKey);
            }

            public void actionSelectionLayerChanged(File layerKey) {
                dataModels.get(layerKey).updateSelectionLayer(layerKey);
            }
            public void actionVisibilityChanged(File layerKey, boolean visibility) {}
            public void actionVisibleMapChanged() {}
        });
    }

    public void addLayer(File layerKey) {
        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        JPanel jPanel1 = new JPanel();
        JTable jTable1 = new JTable();
        JScrollPane jScrollPane1 = new JScrollPane();
        JCheckBox jCheckBox1 = new JCheckBox();
        jCheckBox1.setText("Show selected only");
        jScrollPane1.setViewportView(jTable1);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 722, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jCheckBox1)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 444, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox1))
        );

        layerPane.insertTab(layerKey.getName(), null, jPanel1, null, 0);
        final TableDataModel dataModel = new TableDataModel(layerCollection, layerKey);
        dataModel.showSelectedOnly(false);
        dataModels.put(layerKey, dataModel);
        jCheckBox1.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                dataModel.showSelectedOnly(((JCheckBox) e.getSource()).isSelected());
            }
        });
        jTable1.setModel(dataModel);
        // </editor-fold>
        panels.put(layerKey, jPanel1);
        tables.put(layerKey, jTable1);
    }

    public void removeLayer(File layerKey) {
        JPanel panel = panels.get(layerKey);
        panels.remove(layerKey);
        layerPane.remove(panel);
        if (layerPane.getComponents().length == 0) {
            setVisible(false);
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() {
        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        layerPane = new javax.swing.JTabbedPane();
        ActionListener cancelActionListener = new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               setVisible(false);
           }
        };
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);
        layerPane.registerKeyboardAction(cancelActionListener, escape, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layerPane, javax.swing.GroupLayout.DEFAULT_SIZE, 727, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layerPane, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
        );
        pack();
        // </editor-fold>
    }

}
