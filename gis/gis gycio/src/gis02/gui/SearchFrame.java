/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * AttributeDataFrame.java
 *
 * Created on 2009.3.25, 09.38.16
 */

package gis02.gui;

import gis02.LayerCollection;
import gis02.gui.listener.LayerCollectionListener;
import gis02.tools.StringUtil;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.ParallelGroup;
import javax.swing.GroupLayout.SequentialGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.LayoutStyle;

/**
 *
 * @author Benamis
 */
public class SearchFrame extends javax.swing.JFrame {

    private JTabbedPane layerPane;
    private HashMap<File,JPanel> panels = new HashMap<File,JPanel>();
    private String[] comparisions = {"select operator", "<", "<=", "==", "!=", "=>", ">", "LIKE"};
    private LayerCollection layerCollection;

    public SearchFrame(LayerCollection layerCollection) {
        this.setTitle("Search");
        this.layerCollection = layerCollection;
        initComponents();

        layerCollection.addListener(new LayerCollectionListener() {
            public void actionDataLayerAdded(File layerKey, boolean visible, double mapMinX, double mapMaxX, double mapMinY, double mapMaxY) {
                addLayer(layerKey);
            }
            public void actionDataLayerRemoved(File layerKey) {
                removeLayer(layerKey);
            }
            public void actionSelectionLayerChanged(File layerKey) {}
            public void actionVisibilityChanged(File layerKey, boolean visibility) {}
            public void actionVisibleMapChanged() {}
        });
    }

    public void addLayer(final File layerKey) {
        Vector<String> columns = layerCollection.getLayerColumns(layerKey);

        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        JPanel jPanel = new JPanel();
        JButton clearSearchButton = new JButton();
        JButton okSearchButton = new JButton();
        final JComboBox[] nameComboBoxes = new JComboBox[10];
        final JComboBox[] comparissionComboBoxes = new JComboBox[10];
        JPanel[] localPanels = new JPanel[10];
        GroupLayout[] layouts = new GroupLayout[10];
        final JTextField[] textFields = new JTextField[10];

        GroupLayout layout = new GroupLayout(jPanel);
        jPanel.setLayout(layout);
        SequentialGroup sequentialForVertical = layout.createSequentialGroup().addGap(10);
        ParallelGroup paralelForHorizontal = layout.createParallelGroup(GroupLayout.Alignment.TRAILING);

        for (int i = 0; i < 10; i++) {
            nameComboBoxes[i] = new JComboBox();
            nameComboBoxes[i].setModel(new DefaultComboBoxModel(columns));
            comparissionComboBoxes[i] = new JComboBox();
            comparissionComboBoxes[i].setModel(new DefaultComboBoxModel(comparisions));
            textFields[i] = new JTextField();
            localPanels[i] = new JPanel();
            layouts[i] = new GroupLayout(localPanels[i]);
            localPanels[i].setLayout(layouts[i]);
            layouts[i].setHorizontalGroup(
                    layouts[i].createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layouts[i].createSequentialGroup()
                        .addComponent(nameComboBoxes[i], GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                        .addComponent(comparissionComboBoxes[i], GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                        .addComponent(textFields[i], GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
            );
            layouts[i].setVerticalGroup(
                    layouts[i].createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layouts[i].createParallelGroup(GroupLayout.Alignment.BASELINE)
                            .addComponent(nameComboBoxes[i])
                            .addComponent(comparissionComboBoxes[i])
                            .addComponent(textFields[i]))
            );
            sequentialForVertical = sequentialForVertical.addComponent(localPanels[i], GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE).addGap(5);
            paralelForHorizontal = paralelForHorizontal.addComponent(localPanels[i], GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE);
        }

        okSearchButton.setText("OK");
        clearSearchButton.setText("Clear");

        clearSearchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                for (int i = 0; i < textFields.length; i++) {
                    textFields[i].setText("");
                    nameComboBoxes[i].setSelectedIndex(0);
                    comparissionComboBoxes[i].setSelectedIndex(0);
                    fireSearchEvent(layerKey, "");
                }
            }
        });
        okSearchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Vector<String> queries = new Vector<String>();
                for (int i = 0; i < textFields.length; i++) {
                    if (comparissionComboBoxes[i].getSelectedItem().toString().equals("<")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" < "+textFields[i].getText());
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals("<=")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" <= "+textFields[i].getText());
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals("==")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" = '"+textFields[i].getText()+"'");
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals("!=")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" <> '"+textFields[i].getText()+"'");
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals("=>")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" >= "+textFields[i].getText());
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals(">")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" > "+textFields[i].getText());
                    } else if (comparissionComboBoxes[i].getSelectedItem().toString().equals("LIKE")) {
                        queries.add(nameComboBoxes[i].getSelectedItem()+" LIKE '"+textFields[i].getText()+"'");
                    }
                }
                fireSearchEvent(layerKey, StringUtil.implode(" AND ", queries));
            }
        });
        
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addGap(10, 10, 10)
                    .addGroup(paralelForHorizontal)
                    .addGap(10, 10, 10))
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(283, Short.MAX_VALUE)
                    .addComponent(okSearchButton)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(clearSearchButton)
                    .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup()
            .addGroup(sequentialForVertical
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(clearSearchButton)
                    .addComponent(okSearchButton))
                .addContainerGap(0, Short.MAX_VALUE))
        );
        layerPane.insertTab(layerKey.getName(), null, jPanel, null, 0);
        // </editor-fold>

        panels.put(layerKey, jPanel);
    }

    public void removeLayer(File layerKey) {
        JPanel panel = panels.get(layerKey);
        panels.remove(layerKey);
        layerPane.remove(panel);
        if (layerPane.getComponents().length == 0) {
            setVisible(false);
        }
    }

    private void initComponents() {
        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        layerPane = new javax.swing.JTabbedPane();
        ActionListener cancelActionListener = new ActionListener() {
           public void actionPerformed(ActionEvent e) {
               setVisible(false);
           }
        };
        KeyStroke escape = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true);
        layerPane.registerKeyboardAction(cancelActionListener, escape, JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layerPane, javax.swing.GroupLayout.DEFAULT_SIZE, 727, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(layerPane, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
        );
        pack();
        setSize(400, 450);
        // </editor-fold>
    }

    public void fireSearchEvent(File layerKey, String cqlQuery) {
        layerCollection.reQueryDataLayer(layerKey, cqlQuery);
	}

}
