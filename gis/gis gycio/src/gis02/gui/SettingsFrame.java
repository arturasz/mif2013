/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * SettingsFrame.java
 *
 * Created on 2009.5.11, 11.22.33
 */

package gis02.gui;

import gis02.*;

/**
 *
 * @author Benamis
 */
public class SettingsFrame extends javax.swing.JFrame {

    private CabledLayerCollection layerCollection;

    public SettingsFrame(CabledLayerCollection layerCollection) {
        this.layerCollection = layerCollection;
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        regionLabel = new javax.swing.JLabel();
        regionField = new javax.swing.JComboBox();
        altitudeFromLabel = new javax.swing.JLabel();
        altitudeFromField = new javax.swing.JTextField();
        altitudeToLabel = new javax.swing.JLabel();
        altitudeToField = new javax.swing.JTextField();
        bufferField = new javax.swing.JTextField();
        bufferLabel = new javax.swing.JLabel();
        closeButton = new javax.swing.JButton();

        setTitle("Cable Installation Settings");
        setAlwaysOnTop(true);
        setResizable(false);

        regionLabel.setText("Region:");

        regionField.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "03 Akmenės r.", "55 Alytaus m.", "52 Alytaus r.", "23 Anykščių r.", "50 Birštono m.", "01 Biržų r.", "57 Druskininkų m.", "28 Ignalinos r.", "38 Jonavos r.", "05 Joniškio r.", "36 Jurbarko r.", "43 Kaišiadorių r.", "44 Kauno m.", "39 Kauno r.", "20 Kelmės r.", "32 Kėdainių r.", "21 Klaipėdos m.", "24 Klaipėdos m.", "18 Klaipėdos r.", "10 Kretingos r.", "14 Kupiškio r.", "56 Lazdijų r.", "51 Marijampolės m.", "45 Marijampolės r.", "02 Mažeikių r.", "35 Molėtų r.", "26 Neringos m.", "06 Pakruojo r.", "13 Palangos m.", "22 Panevėžio m.", "17 Panevėžio r.", "07 Pasvalio r.", "11 Plungės r.", "48 Prienų r.", "19 Radviliškio r.", "31 Raseinių r.", "09 Rokiškio r.", "04 Skuodo r.", "41 Šakių r.", "53 Šalčininkų r.", "15 Šiaulių m.", "08 Šiaulių r.", "27 Šilalės r.", "30 Šilutės r.", "40 Širvintų r.", "37 Švenčionių r.", "34 Tauragės r.", "12 Telšių r.", "46 Trakų r.", "33 Ukmergės r.", "25 Utenos r.", "54 Varėnos r.", "49 Vilkaviškio r.", "47 Vilniaus m.", "42 Vilniaus r.", "29 Visagino m.", "16 Zarasų r." }));

        altitudeFromLabel.setText("Altitude from:");

        altitudeFromField.setText("80");

        altitudeToLabel.setText("Altitude to:");

        altitudeToField.setText("292");

        bufferField.setText("2000");

        bufferLabel.setText("Distance to lake:");

        closeButton.setText("Apply Settings");
        closeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(regionLabel)
                    .addComponent(altitudeFromLabel)
                    .addComponent(altitudeToLabel)
                    .addComponent(bufferLabel))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(altitudeToField, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                    .addComponent(altitudeFromField, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                    .addComponent(regionField, 0, 164, Short.MAX_VALUE)
                    .addComponent(bufferField, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE)
                    .addComponent(closeButton, javax.swing.GroupLayout.DEFAULT_SIZE, 164, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(regionField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(regionLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altitudeFromField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(altitudeFromLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(altitudeToField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(altitudeToLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bufferField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bufferLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(closeButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeButtonActionPerformed
        String[] region = regionField.getSelectedItem().toString().split(" ", 2);
        layerCollection.settingsChanged(new Integer(region[0]), new Double(altitudeFromField.getText()), new Double(altitudeToField.getText()), new Double(bufferField.getText()));
        this.setVisible(false);
    }//GEN-LAST:event_closeButtonActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField altitudeFromField;
    private javax.swing.JLabel altitudeFromLabel;
    private javax.swing.JTextField altitudeToField;
    private javax.swing.JLabel altitudeToLabel;
    private javax.swing.JTextField bufferField;
    private javax.swing.JLabel bufferLabel;
    private javax.swing.JButton closeButton;
    private javax.swing.JComboBox regionField;
    private javax.swing.JLabel regionLabel;
    // End of variables declaration//GEN-END:variables

}
