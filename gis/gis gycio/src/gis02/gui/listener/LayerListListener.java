/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui.listener;

import java.io.File;

/**
 *
 * @author Benamis
 */
public interface LayerListListener {
    public void layerUnselected(File layerKey);
    public void layerSelected(File layerKey);
}
