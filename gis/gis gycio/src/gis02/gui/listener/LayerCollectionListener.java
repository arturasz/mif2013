/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.gui.listener;

import java.io.File;

/**
 *
 * @author Benamis
 */
public interface LayerCollectionListener {

    public void actionDataLayerAdded(File layerKey, boolean visible, double mapMinX, double mapMaxX, double mapMinY, double mapMaxY);
    public void actionDataLayerRemoved(File layerKey);
    public void actionSelectionLayerChanged(File layerKey);
    public void actionVisibilityChanged(File layerKey, boolean visibility);
    public void actionVisibleMapChanged();

}
