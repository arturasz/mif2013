package gis02.gui;

import gis02.*;
import gis02.gui.listener.LayerCollectionListener;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JPanel;
import javax.swing.KeyStroke;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.referencing.crs.EPSGCRSAuthorityFactory;

public class MapPanel extends JPanel
    implements MouseListener, MouseMotionListener, MouseWheelListener, KeyListener {
    
    private LayerCollection layerCollection;

    // MapPanel state
    private MouseMode mouseMode = MouseMode.Move;
    private double scale = 0;
    private double mapCenterX = 0;
    private double mapCenterY = 0;
    private int panelWidth = 300;
    private int panelHeight = 300;
    private Rectangle selectionRectangle = null;
    private int mouseStartX = -1;
    private int mouseStartY = -1;
    private int mouseEndX = -1;
    private int mouseEndY = -1;

    // for minimum scale calculation
    private double mapMaxX = 0;
    private double mapMinX = 0;
    private double mapMaxY = 0;
    private double mapMinY = 0;

    // For map rendering
    private boolean mapChanged = false;
    private boolean paintInProgress = false;
    private long lastPaintTook = 5000;

    // render optimizations
    private BufferedImage bufImage = new BufferedImage(panelWidth, panelHeight, BufferedImage.TYPE_INT_RGB);    // map with selection rectangle on top
    private BufferedImage mapImage = new BufferedImage(panelWidth, panelHeight, BufferedImage.TYPE_INT_RGB);    // just map

    public MapPanel(final LayerCollection layerCollection) {
        this.addMouseListener(this);
        this.addMouseMotionListener(this);
        this.addKeyListener(this);

        this.layerCollection = layerCollection;
        // schedule repaint operation every 500ms
        (new Timer()).schedule(new TimerTask() {
            @Override
            public void run() {
                MapPanel.this.repaint();
            }
        }, 0, 500);

        layerCollection.addListener(new LayerCollectionListener() {
            public void actionDataLayerAdded(File layerKey, boolean visible, double minX, double maxX, double minY, double maxY) {
                mapMinX = minX;
                mapMaxX = maxX;
                mapMinY = minY;
                mapMaxY = maxY;
                if (layerCollection.getNumDataLayers() == 1) {
                    reCenter();
                    setScale(0);
                }
            }

            public void actionDataLayerRemoved(File layerKey) {
            }

            public void actionSelectionLayerChanged(File layerKey) {
            }

            public void actionVisibilityChanged(File layerKey, boolean visibility) {
            }

            public void actionVisibleMapChanged() {
                mapChanged = true;
            }
        });
    }

    @Override
    public void setSize(int width, int height) {
        panelHeight = height;
        panelWidth = width;
        bufImage = new BufferedImage(panelWidth, panelHeight, BufferedImage.TYPE_INT_RGB);
        mapImage = new BufferedImage(panelWidth, panelHeight, BufferedImage.TYPE_INT_RGB);
        mapChanged = true;
        setScale(scale);
    }

    @Override
    public void setSize(Dimension dimension) {
        setSize(dimension.width, dimension.height);
    }

    public void setScale(double scale) {
        double minScale = Math.min(panelWidth/(mapMaxX-mapMinX), panelHeight/(mapMaxY-mapMinY));
        scale = Math.max(scale, minScale);
        if (Math.abs(scale-this.scale) > 0.00001) {
            this.scale = scale;
            mapChanged = true;
        }
    }

    public double getScale() {
        return scale;
    }

    public void moveCenter(int pixelsHorizontly, int pixelsVerticaly) {
        double newCenterX = mapCenterX + pixelsHorizontly/scale;
        double newCenterY = mapCenterY + pixelsVerticaly/scale;
        if ((Math.abs(mapCenterX-newCenterX) > 0.00001) || (Math.abs(mapCenterY-newCenterY) > 0.00001)) {
            mapCenterX = newCenterX;
            mapCenterY = newCenterY;
            mapChanged = true;
        }
    }

    public void reCenter(double minX, double maxX, double minY, double maxY) {
        double newCenterY = (maxY-minY)/2+minY;
        double newCenterX = (maxX-minX)/2+minX;
        if ((Math.abs(mapCenterX-newCenterX) > 0.00001) || (Math.abs(mapCenterY-newCenterY) > 0.00001)) {
            mapCenterX = newCenterX;
            mapCenterY = newCenterY;
            mapChanged = true;
        }
    }

    public void reCenter() {
        reCenter(mapMinX, mapMaxX, mapMinY, mapMaxY);
    }

    public void reCenter(double centerX, double centerY) {
        reCenter(centerX, centerX, centerY, centerY);
    }

    public void zoomIn() {
        setScale(scale*1.1);
    }

    public void zoomOut() {
        setScale(scale/1.1);
    }

    public void setMouseMode(MouseMode mode) {
        mouseMode = mode;
        if (mouseMode != MouseMode.Select) {
            selectionRectangle = null;
        }
    }

    public MouseMode getMouseMode() {
        return mouseMode;
    }

    /* (non-Javadoc)
     * @see javax.swing.JComponent#paint(java.awt.Graphics)
     */
    @Override
    synchronized public void paint(Graphics g) {
        // wont allow few paintings at once
        if (paintInProgress) {
            return;
        }
        paintInProgress = true;

        // no layers present
        if (layerCollection.getNumLayers() == 0) {
            Graphics2D myGraphics = bufImage.createGraphics();
            myGraphics.setColor(new Color(236,233,216));
            myGraphics.fillRect(0, 0, panelWidth, panelHeight);
            g.drawImage(bufImage, 0, 0, this);
            paintInProgress = false;
            return;
        }
        
        // map changed
        if (mapChanged) {
            long miliTime = System.currentTimeMillis();

            double minX = mapCenterX-panelWidth/scale/2;
            double minY = mapCenterY-panelHeight/scale/2;
            double maxX = panelWidth/scale+minX;
            double maxY = panelHeight/scale+minY;
            try {
                ReferencedEnvelope mapArea = new ReferencedEnvelope(minX, maxX, minY, maxY, new EPSGCRSAuthorityFactory().createCoordinateReferenceSystem("EPSG:2600"));
                Rectangle screenArea = new Rectangle(0, 0, panelWidth, panelHeight);
                Graphics2D myGraphics = mapImage.createGraphics();
                myGraphics.setColor(new Color(236,233,216));
                myGraphics.fillRect(0, 0, panelWidth, panelHeight);
                layerCollection.getRenderer().paint(myGraphics, screenArea, mapArea);
                g.drawImage(mapImage, 0, 0, this);
            } catch (Exception e) {
                System.out.println("Exception in MapPanel.paint: "+e.getMessage());
            }
            mapChanged = false;
            lastPaintTook = System.currentTimeMillis()-miliTime;
            System.out.println("MapPaintTime: "+lastPaintTook);
            paintInProgress = false;
            return;
        }

        // maybe selecting a rectangle?
        if ((mouseMode == MouseMode.Select) && (selectionRectangle != null)) {
            Graphics2D myGraphics = bufImage.createGraphics();
            myGraphics.drawImage(mapImage, 0, 0, this);
            myGraphics.setColor(new Color(0,255,0));
            myGraphics.drawRect(selectionRectangle.x, selectionRectangle.y, selectionRectangle.width, selectionRectangle.height);
            g.drawImage(bufImage, 0, 0, this);
            paintInProgress = false;
            return;
        }

        g.drawImage(mapImage, 0, 0, this);
        paintInProgress = false;
    }
    
    // mouse events
    public void mouseEntered(MouseEvent e) {
        if (mouseMode == MouseMode.Select) {
            this.setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        } else if (mouseMode == MouseMode.Move) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
        } else if (mouseMode == MouseMode.Cable) {
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    public void mouseExited(MouseEvent e) {
        setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void mouseClicked(MouseEvent e) {}

    public void mouseMoved(MouseEvent e) {
        if (mouseMode == MouseMode.Select) {
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        } else if (mouseMode == MouseMode.Move) {
            setCursor(new Cursor(Cursor.HAND_CURSOR));
        } else if (mouseMode == MouseMode.Cable) {
            setCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
        }
    }

    public void mouseReleased(MouseEvent e) {
        if ((mouseMode == MouseMode.Select) && (mouseStartX >= 0)) {
            if ((mouseStartX == mouseEndX) && (mouseStartY == mouseEndY)) {
                double minMapX = mapCenterX - panelWidth/scale/2 + mouseStartX/scale - 1/scale;
                double minMapY = mapCenterY + panelHeight/scale/2 - mouseStartY/scale - 1/scale;
                double maxMapX = mapCenterX - panelWidth/scale/2 + mouseStartX/scale + 1/scale;
                double maxMapY = mapCenterY + panelHeight/scale/2 - mouseStartY/scale + 1/scale;
                layerCollection.reAreaDataLayer(null, minMapX, maxMapX, minMapY, maxMapY);
            } else {
                double minMapX = mapCenterX - panelWidth/scale/2 + mouseStartX/scale;
                double minMapY = mapCenterY + panelHeight/scale/2 - mouseEndY/scale;
                double maxMapX = mapCenterX - panelWidth/scale/2 + mouseEndX/scale;
                double maxMapY = mapCenterY + panelHeight/scale/2 - mouseStartY/scale;
                layerCollection.reAreaDataLayer(null, minMapX, maxMapX, minMapY, maxMapY);
            }
        }
        mouseStartX = -1;
        mouseStartY = -1;
        mouseEndX = -1;
        mouseEndY = -1;
        selectionRectangle = null;
        repaint();
    }

    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            if (mouseMode == MouseMode.Move) {
                mouseStartX = e.getX();
                mouseStartY = e.getY();
            } else if (mouseMode == MouseMode.Select) {
                mouseStartX = e.getX();
                mouseStartY = e.getY();
                mouseEndX = e.getX();
                mouseEndY = e.getY();
            } else if (mouseMode == MouseMode.Cable) {
                double mapX = mapCenterX - panelWidth/scale/2 + e.getX()/scale;
                double mapY = mapCenterY + panelHeight/scale/2 - e.getY()/scale;
                ((CabledLayerCollection)layerCollection).addCablePoint(mapX, mapY);
            }
        } else if (e.getButton() == MouseEvent.BUTTON3) {
            ReferencedEnvelope zoomToArea = null;
            if (mouseMode == MouseMode.Cable) {
                zoomToArea = ((CabledLayerCollection)layerCollection).getSearchSpaceEnvelope();
            }
            if (zoomToArea == null) {
                zoomToArea = layerCollection.getSelectionEnvelope();
            }
            if (zoomToArea != null) {
                double minX = zoomToArea.getMinX();
                double minY = zoomToArea.getMinY();
                double maxX = zoomToArea.getMaxX();
                double maxY = zoomToArea.getMaxY();
                double minScale = Math.min(panelWidth/(maxX-minX), panelHeight/(maxY-minY));
                double centerY = (maxY-minY)/2+minY;
                double centerX = (maxX-minX)/2+minX;
                reCenter(centerX, centerY);
                setScale(minScale);
            } else {
                reCenter();
                setScale(0);
            }
        }
    }

    public void mouseDragged(MouseEvent e) {
        if (mouseMode == MouseMode.Move) {
            if (mouseStartX+mouseStartY > 0) {
                int difX = (e.getX() - mouseStartX)*-1;
                int difY = e.getY() - mouseStartY;
                mouseStartX = e.getX();
                mouseStartY = e.getY();
                moveCenter(difX, difY);
            }
        } else if (mouseMode == MouseMode.Select) {
            if (mouseStartX+mouseStartY > 0) {
                mouseEndX = e.getX();
                mouseEndY = e.getY();
                int x = Math.min(mouseStartX, mouseEndX);
                int y = Math.min(mouseStartY, mouseEndY);
                int width = Math.abs(mouseStartX - mouseEndX);
                int height = Math.abs(mouseStartY - mouseEndY);
                selectionRectangle = new Rectangle(x, y, width, height);
                repaint();
            }
        }
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
       int notches = e.getWheelRotation();
       for (int i = 0; i < notches; i++) {
           this.zoomOut();
       }
       for (int i = notches; i < 0; i++) {
           this.zoomIn();
       }
    }

    public void keyPressed(KeyEvent e) {
        KeyStroke up = KeyStroke.getKeyStroke(KeyEvent.VK_UP, KeyEvent.CTRL_DOWN_MASK);
        KeyStroke down = KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, KeyEvent.CTRL_DOWN_MASK);
        KeyStroke current = KeyStroke.getKeyStroke(e.getKeyCode(), e.getModifiers());
        if (current == up) {
            this.zoomIn();
        } else if (current == down) {
            this.zoomOut();
        }
    }

    public void keyTyped(KeyEvent e) {    }

    public void keyReleased(KeyEvent e) {    }

}
