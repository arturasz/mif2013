package gis02;

import gis02.gui.*;
import gis02.gui.listener.*;
import gis02.gui.model.*;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Aurimas Kašauskas
 */
public class Main extends javax.swing.JFrame {
    // Standart components
    private JPanel layersPanel = new JPanel();
    private JScrollPane jScrollPane1 = new JScrollPane();
    private CheckBoxList layersList = new CheckBoxList();
    private JButton addLayerButton = new JButton("Add");
    private JButton dropLayerButton = new JButton("Drop");
    private JFileChooser addLayerDialog = new JFileChooser("./data");
    private JPanel toolsPanel = new JPanel();
    private JToggleButton moveButton = new JToggleButton("Move");
    private JToggleButton selectButton = new JToggleButton("Select");
    private JToggleButton cableButton = new JToggleButton("Cable");
    private JButton attributeDataButton = new JButton("Attribute Data");
    private JButton searchButton = new JButton("Search");
    private JButton clearButton = new JButton("Drop Points");
    private JButton settingsButton = new JButton("Settings");
    private JPanel mapPanelContainer = new JPanel();   // @todo: remove

    // Custom components
    private LayerCollection layerCollection = new CabledLayerCollection();
    private CheckBoxListModel listModel = new CheckBoxListModel(layerCollection, layersList);
    private MapPanel mapPanel = new MapPanel(layerCollection);
    private AttributeDataFrame attributeDataFrame = new AttributeDataFrame(layerCollection);
    private SearchFrame searchFrame = new SearchFrame(layerCollection);
    private SettingsFrame settingsFrame = new SettingsFrame((CabledLayerCollection)layerCollection);
    private DropLayerFrame dropLayerFrame = new DropLayerFrame(layerCollection);

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) throws Exception {
        UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
        new Main().setVisible(true);
    }

    /** Creates new form Main */
    public Main() throws Exception {
        setTitle("GIS program v2.0");
        initComponents();
        attachListeners();
        setMouseMode(MouseMode.Move);
        mapPanelContainer.setSize(1000, 1000);
    }

    private void showSubFrame(Frame frame) {
        Point location = (Point) getLocation().clone();
        location.x = location.x + getWidth()/2 - frame.getWidth()/2;
        location.y = location.y + getHeight()/2 - frame.getHeight()/2;
        location.x = Math.max(location.x, 0);
        location.y = Math.max(location.y, 0);
        frame.setLocation(location);
        frame.setVisible(true);
        frame.toFront();
    }

    private void setMouseMode(MouseMode mouseMode) {
        moveButton.setSelected(false);
        selectButton.setSelected(false);
        cableButton.setSelected(false);
        layerCollection.setMouseMode(mouseMode);
        mapPanel.setMouseMode(mouseMode);

        switch (mouseMode) {
            case Move:      moveButton.setSelected(true);       break;
            case Select:    selectButton.setSelected(true);     break;
            case Cable:     cableButton.setSelected(true);      break;
        }
    }

    private void attachListeners() {
        this.addKeyListener(mapPanel);
        this.addMouseWheelListener(mapPanel);

        layersList.addLayerListListener(new LayerListListener() {
            public void layerUnselected(File layerKey) {
                layerCollection.setVisible(layerKey, false);
            }
            public void layerSelected(File layerKey) {
                layerCollection.setVisible(layerKey, true);
            }
        });

        mapPanelContainer.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
                Dimension dimension = mapPanelContainer.getSize();
                dimension.height -= 30;
                dimension.width -= 10;
                mapPanel.setSize(dimension);
            }
            public void componentMoved(ComponentEvent e) {}
            public void componentShown(ComponentEvent e) {}
            public void componentHidden(ComponentEvent e) {}
        });

        moveButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // call to Main.setMouseMode, not MapPanel.setMouseMode
                setMouseMode(MouseMode.Move);
            }
        });

        selectButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // call to Main.setMouseMode, not MapPanel.setMouseMode
                setMouseMode(MouseMode.Select);
            }
        });

        cableButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // call to Main.setMouseMode, not MapPanel.setMouseMode
                setMouseMode(MouseMode.Cable);
            }
        });

        addLayerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int status = addLayerDialog.showOpenDialog(Main.this);
                    if (status == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = addLayerDialog.getSelectedFile();
                        layerCollection.addDataLayer(selectedFile, true);
                    }
                } catch (Exception ex) {
                }
            }
        });

        dropLayerButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int layerNum = layerCollection.getNumDataLayers();
                if (layerNum == 1) {
                    layerCollection.removeDataLayer(0);
                } else if (layerNum > 1) {
                    showSubFrame(dropLayerFrame);
                }
            }
        });

        attributeDataButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (layerCollection.getNumDataLayers() > 0) {
                    showSubFrame(attributeDataFrame);
                }
            }
        });

        settingsButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showSubFrame(settingsFrame);
            }
        });

        clearButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                layerCollection.clearCablePoints();
            }
        });

        searchButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (layerCollection.getNumDataLayers() > 0) {
                    showSubFrame(searchFrame);
                }
            }
        });
    }

    /** This method is called from within the constructor to
     * initialize the form.
     */
    private void initComponents() throws Exception {
        // Make subframes always on top
        attributeDataFrame.setAlwaysOnTop(true);
        searchFrame.setAlwaysOnTop(true);
        dropLayerFrame.setAlwaysOnTop(true);
        settingsFrame.setAlwaysOnTop(true);

        // My file chooser dialog settings
        FileFilter filter = new ExtensionFileFilter("Shape file", new String[] { "shp" });
        addLayerDialog.setFileFilter(filter);
        addLayerDialog.setAcceptAllFileFilterUsed(false);

        mapPanel.setSize(new java.awt.Dimension(500, 500));
        layersList.setModel(listModel);

        // <editor-fold defaultstate="collapsed" desc="NetBeans Generated Code">
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        layersPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Layers"));
        jScrollPane1.setViewportView(layersList);
        javax.swing.GroupLayout layersPanelLayout = new javax.swing.GroupLayout(layersPanel);
        layersPanel.setLayout(layersPanelLayout);
        layersPanelLayout.setHorizontalGroup(
            layersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layersPanelLayout.createSequentialGroup()
                .addComponent(addLayerButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dropLayerButton)
                .addGap(8, 8, 8))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        layersPanelLayout.setVerticalGroup(
            layersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layersPanelLayout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layersPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(addLayerButton)
                    .addComponent(dropLayerButton)))
        );
        mapPanelContainer.setBorder(javax.swing.BorderFactory.createTitledBorder("Map View"));
        javax.swing.GroupLayout mapPanelLayout = new javax.swing.GroupLayout(mapPanel);
        mapPanel.setLayout(mapPanelLayout);
        mapPanelLayout.setHorizontalGroup(
            mapPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 391, Short.MAX_VALUE)
        );
        mapPanelLayout.setVerticalGroup(
            mapPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 286, Short.MAX_VALUE)
        );
        javax.swing.GroupLayout mapPanelContainerLayout = new javax.swing.GroupLayout(mapPanelContainer);
        mapPanelContainer.setLayout(mapPanelContainerLayout);
        mapPanelContainerLayout.setHorizontalGroup(
            mapPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mapPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        mapPanelContainerLayout.setVerticalGroup(
            mapPanelContainerLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(mapPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        toolsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Tools"));
        javax.swing.GroupLayout toolsPanelLayout = new javax.swing.GroupLayout(toolsPanel);
        toolsPanel.setLayout(toolsPanelLayout);
        toolsPanelLayout.setHorizontalGroup(
            toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(toolsPanelLayout.createSequentialGroup()
                .addComponent(moveButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(selectButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cableButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(searchButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(attributeDataButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(settingsButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clearButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        toolsPanelLayout.setVerticalGroup(
            toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(toolsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(moveButton)
                .addComponent(selectButton)
                .addComponent(cableButton)
                .addComponent(searchButton)
                .addComponent(attributeDataButton)
                .addComponent(settingsButton)
                .addComponent(clearButton))
        );
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(layersPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(mapPanelContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(toolsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(toolsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(mapPanelContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(layersPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pack();
        // </editor-fold>
    }
}