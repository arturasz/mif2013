/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import gis02.tools.PathFinder;
import gis02.tools.DateUtil;
import java.io.File;
import java.util.List;
import java.util.Vector;
import javax.swing.JOptionPane;
import org.geotools.data.FeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DefaultMapLayer;
import org.geotools.map.MapLayer;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.expression.PropertyName;

/**
 *
 * @author Benamis
 */
public class CabledLayerCollection extends LayerCollection {
    
    private Geometry searchSpaceGeometry = null;
    private ReferencedEnvelope searchSpaceEnvelope = null;
    private MapLayer searchSpace = null;
    // surface subCollection in current searchSpace
    private FeatureCollection<SimpleFeatureType, SimpleFeature> surfaceCollection = null;
    // points in searchspace for route-finder
    private MapLayer cablePoints = null;
    private FeatureCollection<SimpleFeatureType, SimpleFeature> cablePointsCollection = FeatureCollections.newCollection();
    // needed stuff
    private GeometryFactory geomFactory;
    private SimpleFeatureType featureType;
    private PathFinder pathFinder;
    // search space parameters
    private boolean searchSpaceChanged = false;
    private int regionId;
    private double altitudeFrom;
    private double altitudeTo;
    private double buffer;

    public CabledLayerCollection() {
        super();
        geomFactory = new GeometryFactory();
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        typeBuilder.setName("mypoint");
        typeBuilder.add("mygeom", Point.class);
        featureType = typeBuilder.buildFeatureType();
    }

    public ReferencedEnvelope getSearchSpaceEnvelope() {
        if (searchSpaceEnvelope != null) {
            return searchSpaceEnvelope;
        } else {
            return null;
        }
    }
    
    @Override
    public void clearCablePoints() {
        if (cablePoints != null) {
            mapContext.removeLayer(cablePoints);
            fireVisibleMapChanged();
        }
        cablePointsCollection = FeatureCollections.newCollection();
        cablePoints = null;
    }

    public void settingsChanged(int regionId, double altitudeFrom, double altitudeTo, double buffer) {
        mapContext.removeLayer(searchSpace);
        mapContext.removeLayer(cablePoints);
        if ((regionId == this.regionId) && (Math.abs(altitudeFrom - this.altitudeFrom) < 0.1) && (Math.abs(altitudeTo - this.altitudeTo) < 0.1) && (Math.abs(buffer - this.buffer) < 0.1)) {
            return;
        }
        searchSpaceChanged = true;
        this.regionId = regionId;
        this.altitudeFrom = altitudeFrom;
        this.altitudeTo = altitudeTo;
        this.buffer = buffer;

        rebuildSearchSpace();
    }

    protected SimpleFeature createCablePointFeature(Geometry geometry) {
        SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(featureType);
        featureBuilder.add(geometry);
        return featureBuilder.buildFeature("Point."+cablePointsCollection.size()+1);
    }

    protected Coordinate[] getCablePointCoordinates() {
        Coordinate[] coordinates = new Coordinate[2];
        FeatureIterator<SimpleFeature> iterator = cablePointsCollection.features();
        int i = 0;
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            coordinates[i] = ((Point)feature.getDefaultGeometry()).getCoordinate();
            i++;
        }
        return coordinates;
    }

    public void addCablePoint(double x, double y) {
        if (cablePointsCollection.size() >= 2) {
            // dont allow third point
            return;
        }
        Coordinate coordinate = new Coordinate(x, y);
        Geometry geometry = geomFactory.createPoint(coordinate);
        if (!geometry.intersects(searchSpaceGeometry)) {
            JOptionPane.showMessageDialog(null, "Selected location does not belong to area you chose");
            return;
        }
        if (cablePoints != null) {
            mapContext.removeLayer(cablePoints);
        }
        cablePointsCollection.add(createCablePointFeature(geometry));
        cablePoints = new DefaultMapLayer(cablePointsCollection, layerStyle.getGreenPointStyle());
        mapContext.addLayer(cablePoints);
        fireVisibleMapChanged();
        // done with visual part

        if (cablePointsCollection.size() == 2) {
            Coordinate[] coordinates = getCablePointCoordinates();
            Geometry[] lines = pathFinder.getPath(coordinates);
            FeatureCollection<SimpleFeatureType, SimpleFeature> cableCollection = FeatureCollectionOperation.lineFeatureCollection(lines);
            cableCollection = FeatureCollectionOperation.colideLinesOnPolygons(cableCollection, surfaceCollection);
            if (cableCollection.size() > 0) {
                MapLayer layer = new DefaultMapLayer(cableCollection, layerStyle.getPathStyle());
                File file = null;
                try {
                    file = new File("result." + DateUtil.now());
                } catch (Exception e) {
                    System.out.println(e.getClass().toString()+"\n"+e.getMessage());
                    e.printStackTrace();
                }
                layers.put(file, layer);
                layerKeys.add(file);
                mapContext.addLayer(layer);
                mapContext.removeLayer(searchSpace);
                mapContext.addLayer(searchSpace);
                fireDataLayerAdded(file, true, mapMinX, mapMaxX, mapMinY, mapMaxY);
            }
            clearCablePoints();
        }
    }

    @Override
    public void setMouseMode(MouseMode mouseMode) {
        boolean mouseModeChanged = (mouseMode != this.mouseMode);
        if (mouseModeChanged == false) {
            return;
        }
        boolean wasCableMode = (this.mouseMode == MouseMode.Cable);
        boolean isCableMode = (mouseMode == MouseMode.Cable);
        super.setMouseMode(mouseMode);
        if (isCableMode) {
            // make sure we have all date we need
            addDataLayer(new File("data/pavirs_lt_p.shp"), false);
            addDataLayer(new File("data/rajonai.shp"), true);
            addDataLayer(new File("data/ezerai.shp"), true);
            rebuildSearchSpace();
            rebuildMapContext();
            fireVisibleMapChanged();
        } else if (wasCableMode) {
            rebuildMapContext();
            fireVisibleMapChanged();
        }
    }

    @Override
    public void rebuildMapContext() {
        super.rebuildMapContext();
        if (mouseMode == MouseMode.Cable) {
            if (searchSpace != null) {
                mapContext.addLayer(searchSpace);
            }
            if (cablePoints != null) {
                mapContext.addLayer(cablePoints);
            }
        }
    }

    protected void rebuildSearchSpace() {
        if (mouseMode != MouseMode.Cable) {
            return;
        }
        // make sure we have all date we need
        addDataLayer(new File("data/pavirs_lt_p.shp"), false);
        addDataLayer(new File("data/rajonai.shp"), true);
        addDataLayer(new File("data/ezerai.shp"), true);
        
        if (!searchSpaceChanged) {
            return;
        }
        searchSpaceChanged = false;

        mapContext.removeLayer(searchSpace);
        clearCablePoints();

        try {
            FilterFactory2 filterFactory = CommonFactoryFinder.getFilterFactory2(null);
            surfaceCollection = FeatureCollections.newCollection();
            FeatureCollection simpleCollection = FeatureCollections.newCollection();

            // get region
            FeatureSource regionFeatureSource = layers.get(new File("data/rajonai.shp")).getFeatureSource();
            Filter regionFilter = CQL.toFilter("OBJECTID = '"+regionId+"'");
            FeatureCollection<SimpleFeatureType, SimpleFeature> fc = regionFeatureSource.getFeatures(regionFilter);
            if (!fc.features().hasNext()) {
                JOptionPane.showMessageDialog(null, "No regions with given id ("+regionId+")");
                fireVisibleMapChanged();
                return;
            }
            SimpleFeature region = fc.features().next();
            Geometry regionGeometry = (Geometry) region.getDefaultGeometry();
            Geometry bufferedRegion = regionGeometry.buffer(buffer);
            // lakes within buffered region
            FeatureSource lakeFeatureSource = layers.get(new File("data/ezerai.shp")).getFeatureSource();
            PropertyName property = filterFactory.property(lakeFeatureSource.getSchema().getGeometryDescriptor().getLocalName());
            Filter filterBBox = (Filter)filterFactory.bbox(property, JTS.toEnvelope(bufferedRegion));
            Filter filterPolygon = (Filter)filterFactory.not(filterFactory.disjoint(property, filterFactory.literal(bufferedRegion)));
            Filter filter = (Filter)filterFactory.and(filterBBox, filterPolygon);
            fc = lakeFeatureSource.getFeatures(filter);
            FeatureIterator<SimpleFeature> iterator = fc.features();

            if (iterator.hasNext()) {
                // there are some lakes, make buffer zone arround them and remove it from regionGeometry
                while (iterator.hasNext()) {
                    SimpleFeature lake = iterator.next();
                    Geometry lakeGeometry = (Geometry) lake.getDefaultGeometry();
                    lakeGeometry = lakeGeometry.buffer(buffer);
                    lake.setDefaultGeometry(lakeGeometry);
                    regionGeometry = regionGeometry.difference(lakeGeometry);
                }
            }

            // select surface elements within regionGeometry
            FeatureSource surfaceFeatureSource = layers.get(new File("data/pavirs_lt_p.shp")).getFeatureSource();
            property = filterFactory.property(surfaceFeatureSource.getSchema().getGeometryDescriptor().getLocalName());
            filterBBox = (Filter)filterFactory.bbox(property, JTS.toEnvelope(regionGeometry));
            filterPolygon = (Filter)filterFactory.not(filterFactory.disjoint(property, filterFactory.literal(regionGeometry)));
            Filter filterAltitude = CQL.toFilter("Aukstis >= '"+altitudeFrom+"' AND Aukstis <= '"+altitudeTo+"'");
            List<Filter> filterList = new Vector<Filter>();
            filterList.add(filterAltitude);
            filterList.add(filterBBox);
            filterList.add(filterPolygon);
            filter = (Filter)filterFactory.and(filterList);
            fc = surfaceFeatureSource.getFeatures(filter);
            iterator = fc.features();

            if (iterator.hasNext()) {
                searchSpaceGeometry = ((Geometry)fc.features().next().getDefaultGeometry());
                while (iterator.hasNext()) {
                    SimpleFeature surfaceElement = iterator.next();
                    Geometry surfaceGeometry = (Geometry) surfaceElement.getDefaultGeometry();
                    surfaceGeometry = surfaceGeometry.intersection(regionGeometry);
                    if (!surfaceGeometry.isEmpty()) {
                        if (surfaceGeometry.getClass() == Polygon.class) {
                            Polygon[] polygons = {(Polygon)surfaceGeometry};
                            surfaceGeometry = new MultiPolygon(polygons, new GeometryFactory());
                        }
                        searchSpaceGeometry = searchSpaceGeometry.union(surfaceGeometry);
                        surfaceElement.setDefaultGeometry(surfaceGeometry);
                        surfaceCollection.add(surfaceElement);
                    }
                }
                pathFinder = new PathFinder(searchSpaceGeometry);
                region.setDefaultGeometry(searchSpaceGeometry);
                simpleCollection.add(region);
                searchSpace = new DefaultMapLayer(simpleCollection, layerStyle.getGreenPolygonStyle());
                searchSpaceEnvelope = JTS.toEnvelope(searchSpaceGeometry);
            } else {
                JOptionPane.showMessageDialog(null, "No area found matching your settings");
                fireVisibleMapChanged();
                return;
            }
        } catch (Exception e) {
            System.out.println(e.getClass().getSimpleName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            fireVisibleMapChanged();
            return;
        }
        rebuildMapContext();
        fireVisibleMapChanged();
    }
}