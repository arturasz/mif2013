/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02;

import gis02.gui.listener.LayerCollectionListener;
import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;
import org.geotools.data.FeatureSource;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.DefaultMapContext;
import org.geotools.map.DefaultMapLayer;
import org.geotools.map.MapContext;
import org.geotools.map.MapLayer;
import org.geotools.referencing.crs.EPSGCRSAuthorityFactory;
import org.geotools.renderer.shape.ShapefileRenderer;
import org.geotools.styling.Style;
import org.opengis.feature.Feature;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.referencing.crs.CoordinateReferenceSystem;

/**
 *
 * @author Benamis
 */
public class LayerCollection {
    protected Vector<File> layerKeys = new Vector<File>();
    protected HashMap<File,MapLayer> layers = new HashMap<File,MapLayer>();
    protected HashMap<File,MapLayer> selectionLayers = new HashMap<File,MapLayer>();
    protected MapContext mapContext;
    protected MouseMode mouseMode;
    protected LayerStyle layerStyle;
    protected double mapMinX;
    protected double mapMaxX;
    protected double mapMinY;
    protected double mapMaxY;
    protected Vector<LayerCollectionListener> listeners = new Vector<LayerCollectionListener>();

    public LayerCollection() {
        layerStyle = new LayerStyle();
        try {
            CoordinateReferenceSystem crs = new EPSGCRSAuthorityFactory().createCoordinateReferenceSystem("EPSG:2600");
            mapContext = new DefaultMapContext(crs);
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
    }

    public Vector<String> getLayerColumns(File layerKey) {
        Vector<String> columns = new Vector<String>();
        FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection = getDataLayerFeatureCollection(layerKey);
        FeatureIterator featureIterator = featureCollection.features();
        Feature feature = featureIterator.next();
        Collection properties = feature.getProperties();
        Iterator<Property> propertyIterator = properties.iterator();

        while (propertyIterator.hasNext()) {
            Property property = (Property) propertyIterator.next();
            if (!property.getName().toString().equals("the_geom")) {
                columns.add(property.getName().toString());
            }
        }
        featureCollection.close(featureIterator);
        return columns;
    }

    public void setMouseMode(MouseMode mouseMode) {
        this.mouseMode = mouseMode;
    }

    public MouseMode getMouseMode() {
        return mouseMode;
    }
    
    public Vector<File> getLayerKeys() {
        return layerKeys;
    }

    public File getLayerKeyFromId(int id) {
        return layerKeys.get(id);
    }

    public ReferencedEnvelope getSelectionEnvelope() {
        Collection<MapLayer> collection = selectionLayers.values();
        Iterator<MapLayer> iterator = collection.iterator();
        if (!iterator.hasNext()) {
            return null;
        }

        ReferencedEnvelope bbox = iterator.next().getBounds();
        while (iterator.hasNext()) {
            bbox.expandToInclude(iterator.next().getBounds());
        }
        return bbox;
    }
    
    public void rebuildMapContext() {
        try {
            CoordinateReferenceSystem crs = new EPSGCRSAuthorityFactory().createCoordinateReferenceSystem("EPSG:2600");
            mapContext = new DefaultMapContext(crs);
            mapContext.setCoordinateReferenceSystem(crs);
            Iterator<File> iterator = layerKeys.iterator();
            while (iterator.hasNext()) {
                File layerKey = iterator.next();
                mapContext.addLayer(layers.get(layerKey));
                MapLayer selectionLayer = selectionLayers.get(layerKey);
                if (selectionLayer != null) {
                    mapContext.addLayer(selectionLayer);
                }
            }
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
    }

    public ShapefileRenderer getRenderer() {
        try {
            ShapefileRenderer renderer = new ShapefileRenderer(mapContext);
            HashMap hints = new HashMap();
            hints.put("forceCRS", new EPSGCRSAuthorityFactory().createCoordinateReferenceSystem("EPSG:2600"));
            renderer.setRendererHints(hints);
            return renderer;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void addDataLayer(File layerKey, boolean visible) {
        try {
            if (!layers.containsKey(layerKey)) {
                URL url = layerKey.toURI().toURL();
                ShapefileDataStore sds = new ShapefileDataStore(url);
                //sds.dispose();
                Style style = layerStyle.getLayerStyle(layerKey);
                FeatureSource<SimpleFeatureType, SimpleFeature> featureSource = sds.getFeatureSource();
                MapLayer mapLayer = new DefaultMapLayer(featureSource, style);
                layerKeys.add(layerKey);
                layers.put(layerKey, mapLayer);
                mapContext.addLayer(mapLayer);
                mapLayer.setVisible(visible);

                if (Configuration.DYNAMIC_MAP_BOUNDS) {
                    ReferencedEnvelope envelope = mapContext.getLayerBounds();
                    mapMinX = envelope.getMinX();
                    mapMaxX = envelope.getMaxX();
                    mapMinY = envelope.getMinY();
                    mapMaxY = envelope.getMaxY();
                } else {
                    mapMinX = Configuration.MAP_MIN_X;
                    mapMaxX = Configuration.MAP_MAX_X;
                    mapMinY = Configuration.MAP_MIN_Y;
                    mapMaxY = Configuration.MAP_MAX_Y;
                }
                fireDataLayerAdded(layerKey, visible, mapMinX, mapMaxX, mapMinY, mapMaxY);
                if (visible) {
                    fireVisibleMapChanged();
                }
            }
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();

            System.exit(0);
        }
    }

    public void removeDataLayer(File layerKey) {
        layerKeys.remove(layerKey);
        MapLayer mapLayer = layers.get(layerKey);
        mapContext.removeLayer(mapLayer);
        layers.remove(layerKey);
        MapLayer selectionLayer = selectionLayers.get(layerKey);
        mapContext.removeLayer(selectionLayer);
        selectionLayers.remove(layerKey);
        fireDataLayerRemoved(layerKey);
        if (mapLayer.isVisible()) {
            fireVisibleMapChanged();
        }
        mapLayer.getFeatureSource().getDataStore().dispose();
    }

    public void removeDataLayer(int id) {
        removeDataLayer(getLayerKeyFromId(id));
    }

    public void setVisible(File layerKey, boolean visibility) {
        MapLayer mapLayer = layers.get(layerKey);
        if (mapLayer != null) {
            if (mapLayer.isVisible() != visibility) {
                layers.get(layerKey).setVisible(visibility);
                MapLayer selectionLayer = selectionLayers.get(layerKey);
                if (selectionLayer != null) {
                    selectionLayer.setVisible(visibility);
                }
                fireVisibilityChanged(layerKey, visibility);
                fireVisibleMapChanged();
            }
        }
    }
    
    public void setVisible(int id, boolean visibility) {
        setVisible(getLayerKeyFromId(id), visibility);
    }

    public boolean getVisible(File layerKey) {
        return layers.get(layerKey).isVisible();
    }

    public boolean getVisible(int id) {
        return getVisible(getLayerKeyFromId(id));
    }

    public int getNumDataLayers() {
        return layers.size();
    }

    public int getNumLayers() {
        return layers.size()+selectionLayers.size();
    }

    public FeatureCollection<SimpleFeatureType, SimpleFeature> getDataLayerFeatureCollection(File layerKey) {
        try {
            return (FeatureCollection<SimpleFeatureType, SimpleFeature>) layers.get(layerKey).getFeatureSource().getFeatures();
        } catch (Exception e) {
            System.out.println(e.getClass().getName());
            System.out.println(e.getMessage());
            e.printStackTrace();
            System.exit(0);
            return null;
        }
    }

    public FeatureCollection<SimpleFeatureType, SimpleFeature> getDataLayerFeatureCollection(int id) {
        return getDataLayerFeatureCollection(getLayerKeyFromId(id));
    }

    public FeatureCollection<SimpleFeatureType, SimpleFeature> getSelectionLayerFeatureCollection(File layerKey) {
        MapLayer mapLayer = selectionLayers.get(layerKey);
        if (mapLayer != null) {
            try {
                return (FeatureCollection<SimpleFeatureType, SimpleFeature>) mapLayer.getFeatureSource().getFeatures();
            } catch (Exception e) {
                System.out.println(e.getClass().getName());
                System.out.println(e.getMessage());
                e.printStackTrace();
                System.exit(0);
            }
        }
        return null;
    }

    public FeatureCollection<SimpleFeatureType, SimpleFeature> getSelectionLayerFeatureCollection(int id) {
        return getDataLayerFeatureCollection(getLayerKeyFromId(id));
    }

    public void setSelectionLayerFeatureCollection(File layerKey, FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection) {
        MapLayer selectionLayer = selectionLayers.get(layerKey);
        if ((featureCollection != null) && (featureCollection.features().hasNext())) {
            MapLayer newSelectionLayer;
            if (selectionLayer != null) {
                newSelectionLayer = new DefaultMapLayer(featureCollection, selectionLayer.getStyle());
            } else {
                newSelectionLayer = new DefaultMapLayer(featureCollection, layerStyle.getRedStyle(layers.get(layerKey).getStyle()));
            }
            selectionLayers.remove(layerKey);
            selectionLayers.put(layerKey, newSelectionLayer);
            newSelectionLayer.setVisible(layers.get(layerKey).isVisible());
            rebuildMapContext();
            fireSelectionChanged(layerKey);
            fireVisibleMapChanged();
        } else {
            selectionLayers.remove(layerKey);
            rebuildMapContext();
            if (selectionLayer != null) {
                fireSelectionChanged(layerKey);
                fireVisibleMapChanged();
            }
        }
    }

    public void setSelectionLayerFeatureCollection(int id, FeatureCollection<SimpleFeatureType, SimpleFeature> featureCollection) {
        setSelectionLayerFeatureCollection(getLayerKeyFromId(id), featureCollection);
    }

    // end of simple layer operations
    
    public void reQueryDataLayer(File layerKey, String cqlQuery) {
        FeatureCollection selectionCollection = FeatureCollectionOperation.filterCql(getDataLayerFeatureCollection(layerKey), cqlQuery);
        setSelectionLayerFeatureCollection(layerKey, selectionCollection);
    }

    public void reQueryDataLayer(int id, String cqlQuery) {
        reQueryDataLayer(getLayerKeyFromId(id), cqlQuery);
    }

    public void reSelectDataLayer(File layerKey, Integer[] objectIds) {
        String localName = layers.get(layerKey).getFeatureSource().getName().getLocalPart();
        System.out.println(localName);
        FeatureCollection selectionCollection = FeatureCollectionOperation.filterId(getDataLayerFeatureCollection(layerKey), localName, objectIds);
        setSelectionLayerFeatureCollection(layerKey, selectionCollection);
    }

    public void reSelectDataLayer(int id, Integer[] objectIds) {
        reSelectDataLayer(getLayerKeyFromId(id), objectIds);
    }

    public void reAreaDataLayer(File layerKey, double minX, double maxX, double minY, double maxY) {
        if (layerKey != null) {
            String geometryDescriptor = layers.get(layerKey).getFeatureSource().getSchema().getGeometryDescriptor().getLocalName();
            FeatureCollection selectionCollection = FeatureCollectionOperation.filterArea(getDataLayerFeatureCollection(layerKey), geometryDescriptor, minX, maxX, minY, maxY);
            setSelectionLayerFeatureCollection(layerKey, selectionCollection);
        } else {
            Iterator<File> iterator = layerKeys.iterator();
            while (iterator.hasNext()) {
                layerKey = iterator.next();
                if (layers.get(layerKey).isVisible()) {
                    String geometryDescriptor = layers.get(layerKey).getFeatureSource().getSchema().getGeometryDescriptor().getLocalName();
                    FeatureCollection selectionCollection = FeatureCollectionOperation.filterArea(getDataLayerFeatureCollection(layerKey), geometryDescriptor, minX, maxX, minY, maxY);
                    setSelectionLayerFeatureCollection(layerKey, selectionCollection);
                }
            }
        }
    }

    public void reAreaDataLayer(int id, double minX, double maxX, double minY, double maxY) {
        reAreaDataLayer(getLayerKeyFromId(id), minX, maxX, minY, maxY);
    }

    public void addListener(LayerCollectionListener listener) {
        if (listener != null) {
            listeners.add(listener);
        }
    }

    public void removeListener(LayerCollectionListener listener) {
        if (listener != null) {
            listeners.remove(listener);
        }
    }

    protected void fireDataLayerAdded(File layerKey, boolean visible, double mapMinX, double mapMaxX, double mapMinY, double mapMaxY) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).actionDataLayerAdded(layerKey, visible, mapMinX, mapMaxX, mapMinY, mapMaxY);
        }
    }

    protected void fireDataLayerRemoved(File layerKey) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).actionDataLayerRemoved(layerKey);
        }
    }

    protected void fireSelectionChanged(File layerKey) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).actionSelectionLayerChanged(layerKey);
        }
    }

    protected void fireVisibilityChanged(File layerKey, boolean visibility) {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).actionVisibilityChanged(layerKey, visibility);
        }
    }

    protected void fireVisibleMapChanged() {
        for (int i = 0; i < listeners.size(); i++) {
            listeners.get(i).actionVisibleMapChanged();
        }
    }

    public void clearCablePoints() {
    }

}
