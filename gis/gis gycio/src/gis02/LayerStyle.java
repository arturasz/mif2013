/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02;

import gis02.tools.StyleCloner;
import java.io.File;
import java.io.FileNotFoundException;
import org.geotools.styling.PolygonSymbolizer;
import org.geotools.styling.SLDParser;
import org.geotools.styling.Style;
import org.geotools.styling.StyleBuilder;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.StyleFactoryFinder;
import org.geotools.styling.StyleFactoryImpl;

/**
 *
 * @author Benamis
 */
public class LayerStyle {
    private Style greenPolygon = null;
    private Style greenPoint = null;
    private Style pathStyle = null;

    public LayerStyle() {
        try {
            greenPolygon = new SLDParser (new StyleFactoryImpl()) {{ setInput(new File(Configuration.dataDirectory+"greenPolygon.sld")); }}.readXML()[0];
            greenPoint = new SLDParser (new StyleFactoryImpl()) {{ setInput(new File(Configuration.dataDirectory+"greenPoint.sld")); }}.readXML()[0];
            pathStyle = new SLDParser (new StyleFactoryImpl()) {{ setInput(new File(Configuration.dataDirectory+"blueCable.sld")); }}.readXML()[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Style getRedStyle(Style originalStyle) {
        StyleFactory sf = StyleFactoryFinder.createStyleFactory();
        StyleCloner styleCloner = new StyleCloner(sf);
        return styleCloner.clone(originalStyle);
    }

    public Style getGreenPolygonStyle() {
        return greenPolygon;
    }

    public Style getGreenPointStyle() {
        return greenPoint;
    }

    public Style getPathStyle() {
        return pathStyle;
    }

    public Style getLayerStyle(File layerKey) throws FileNotFoundException {
        final File sldFile = new File(layerKey.toString().replaceAll(".shp", ".sld"));
        Style style;
        if (sldFile.exists()) {
            style = new SLDParser (new StyleFactoryImpl()) {
                {
                    setInput (sldFile);
                }
            }.readXML () [0];
        } else {
            StyleBuilder sb = new StyleBuilder();
            PolygonSymbolizer ps =  sb.createPolygonSymbolizer();
            style = sb.createStyle(ps);
        }
        return style;
    }
}
