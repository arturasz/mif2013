/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

// make it work with geometry not maplayers
        
package gis02.tools;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Envelope;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.TreeSet;
import java.util.Vector;
import org.geotools.geometry.jts.JTS;

/**
 *
 * @author Benamis
 */
public class PathFinder {

    private Geometry searchSpaceGeometry = null;
    private GeometryFactory geomFactory = new GeometryFactory();

    public PathFinder(Geometry searchSpaceGeometry) {
        this.searchSpaceGeometry = searchSpaceGeometry;
    }

    public Geometry[] getPath(Coordinate[] pointCoordinates) {
        if (pointCoordinates.length != 2) {
            return null;
        }

        long time = System.currentTimeMillis();
        Geometry[] results = getPath(pointCoordinates, 0);
        time = System.currentTimeMillis()-time;
        System.out.println("PathFinder: "+time+" ms.");
        return results;
    }

    private Geometry[] getPath(Coordinate[] pointCoordinates, double bufferSize) {
        // make searchSpace smalled
        double middleX = (pointCoordinates[0].x + pointCoordinates[1].x)/2D;
        double middleY = (pointCoordinates[0].y + pointCoordinates[1].y)/2D;
        double size = Math.max(Math.abs(pointCoordinates[0].x - pointCoordinates[1].x), Math.abs(pointCoordinates[0].y - pointCoordinates[1].y))/2D + bufferSize;
        Envelope envelope = new com.vividsolutions.jts.geom.Envelope(middleX-size, middleX+size, middleY-size, middleY+size);
        Geometry boundingGeometry = JTS.toGeometry(envelope);
        Geometry searchPolygon = searchSpaceGeometry.intersection(boundingGeometry);
        Coordinate[] searchCoordinates = searchPolygon.getCoordinates();
        return getPath(pointCoordinates, searchCoordinates);
    }

    private Geometry[] getPath(Coordinate[] pointCoordinates, Coordinate[] searchCoordinates) {

        Vector<LineString> results = new Vector<LineString>();
        Coordinate[] line = {pointCoordinates[0], pointCoordinates[1]};
        LineString directLine = geomFactory.createLineString(line);
        if (directLine.coveredBy(searchSpaceGeometry)) {
            // direct line found
            results.add(directLine);
        } else {
            // key - destination location; value - source location
            HashMap<Coordinate,Coordinate> reachedFrom = new HashMap<Coordinate,Coordinate>();

            // posible destinations
            LinkedList<Coordinate> allLocations = new LinkedList<Coordinate>();
            for (int i = 0; i < searchCoordinates.length; i++) {
                if (searchSpaceGeometry.touches(geomFactory.createPoint(searchCoordinates[i]))) {
                    allLocations.add(searchCoordinates[i]);
                }
            }
            allLocations.add(pointCoordinates[1]);

            // list of reached locations
            Comparator comparator = new Comparator<DistanceCoordinate>(){
                public int compare(DistanceCoordinate o1, DistanceCoordinate o2) {
                    if (o1.distance+o1.distanceLeft < o2.distance+o2.distanceLeft) {
                        return -1;
                    } else if (o1.distance+o1.distanceLeft > o2.distance+o2.distanceLeft) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            };
            TreeSet<DistanceCoordinate> sortedReachedLocations = new TreeSet<DistanceCoordinate>(comparator);
            sortedReachedLocations.add(new DistanceCoordinate(pointCoordinates[0], pointCoordinates[0], 0, pointCoordinates[1]));
            boolean success = false;

            //System.out.println("Start: "+((int)pointCoordinates[0].x)+":"+((int)pointCoordinates[0].y));
            //System.out.println("Desti: "+((int)pointCoordinates[1].x)+":"+((int)pointCoordinates[1].y));
            //System.out.println("Points in area: "+allLocations.size());
            // try to go to all places from places we reached allready (needs order by distance, so we visit near stuff first)
            while (!sortedReachedLocations.isEmpty()) {
                DistanceCoordinate sourceElement = sortedReachedLocations.pollFirst();
                Coordinate source = sourceElement.coordinate;
                //System.out.println("NowAt: "+((int)source.x)+":"+((int)source.y));
                //System.out.println("reached = "+sortedReachedLocations.size()+"; left = "+allLocations.size());
                if ((Math.abs(source.x - pointCoordinates[1].x) < 0.1D) && (Math.abs(source.y - pointCoordinates[1].y) < 0.1D)) {
                    //System.out.println("destination reached: "+sourceElement.distance);
                    success = true;
                    break;
                }
                // make list of all coordinates in system
                Object[] destinations = allLocations.toArray();

                for (int i = 0; i < destinations.length; i++) {
                    Coordinate destination = (Coordinate) destinations[i];
                    Coordinate[] subLine = {source, destination};
                    LineString subLineString = geomFactory.createLineString(subLine);
                    if (subLineString.coveredBy(searchSpaceGeometry)) {
                        // we can go here
                        allLocations.remove(destination);
                        sortedReachedLocations.add(new DistanceCoordinate(source, destination, sourceElement.distance, pointCoordinates[1]));
                        reachedFrom.put(destination, source);
                        //System.out.println("*GoTo: "+((int)destination.x)+":"+((int)destination.y));
                    }
                }
            }
            if (success) {
                double pathLength = 0;
                Coordinate destination = null;
                Coordinate source = pointCoordinates[1];
                do {
                    destination = source;
                    source = reachedFrom.get(destination);
                    Coordinate[] fullLine = {source, destination};
                    LineString fullLineString = geomFactory.createLineString(fullLine);
                    pathLength += fullLineString.getLength();
                    results.add(fullLineString);
                } while ((Math.abs(source.x - pointCoordinates[0].x) > 0.1D) || (Math.abs(source.y - pointCoordinates[0].y) > 0.1D));
                //System.out.println("route length: "+pathLength);
            }
        }
        Geometry[] resultsArray = new Geometry[results.size()];
        for (int i = 0; i < results.size(); i++) {
            resultsArray[i] = results.get(i);
        }
        return resultsArray;
    }

    private class DistanceCoordinate {
        public Coordinate coordinate;
        public double distance;
        public double distanceLeft;
        public DistanceCoordinate(Coordinate source, Coordinate destination, double dist, Coordinate finalPoint) {
            this.distance = Math.sqrt(Math.pow(source.x-destination.x,2) + Math.pow(source.y-destination.y,2)) + dist;
            this.distanceLeft = Math.sqrt(Math.pow(finalPoint.x-destination.x,2) + Math.pow(finalPoint.y-destination.y,2));
            coordinate = destination;
        }
    }
}
