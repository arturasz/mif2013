/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.tools;

import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 *
 * @author Benamis
 */
public class DateUtil {
    public static final String DATE_FORMAT_NOW = "HH:mm:ss";

    public static String now() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
        return sdf.format(cal.getTime());
    }

}