/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gis02.tools;

import java.util.Vector;

/**
 *
 * @author Benamis
 */
public class StringUtil {

    static public String implode(String delim, Vector a) {
        String result = "";
        for (int i = 0; i < a.size() - 1; i++) {
            result += a.elementAt(i) + delim;
        }
        if (a.size() > 0) result += a.elementAt(a.size() - 1);
        return result;
    }

}
