/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package info.ragauskas.gis;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.*;
import javax.swing.JTable;

import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import javax.swing.table.DefaultTableModel;
import net.opengis.gml.v_1_0_0.MultiLineString;
import net.opengis.gml.v_1_0_0.MultiPolygon;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Query;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.*;
import org.geotools.styling.*;
import org.geotools.styling.Stroke;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.event.MapMouseEvent;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.geometry.BoundingBox;
import org.opengis.geometry.coordinate.LineString;
import org.opengis.geometry.coordinate.Polygon;

/**
 *
 * @author mothership
 */
public class Map extends JMapFrame {
    
    private JPanel sideInfoPanel = new JPanel(new BorderLayout());
    JPanel panel = new JPanel();
    MapContent map = new MapContent();
    JMapFrame that = this;
    private JToolBar toolbar;
    private StyleFactory sf = CommonFactoryFinder.getStyleFactory(null);
    private FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);
    private String geometryAttributeName;
    private enum GeometryType {
        POINT, LINE, POLYGON
    };
    private SimpleFeatureSource featureSource;
    
    /*
    * Some default style variables
    */
    private static final Color DEFAULT_LINE = Color.BLACK;
    private static final Color DEFAULT_FILL = Color.WHITE;
    // private static final Color LINE_COLOUR = Color.BLUE;
    // private static final Color FILL_COLOUR = Color.CYAN;
    private static final Color SELECTED_COLOUR = Color.GREEN;
    private static final float OPACITY = 1.0f;
    private static final float LINE_WIDTH = 1.0f;
    private static final float POINT_SIZE = 3.5f;
    public Point2D startPosWorld = new DirectPosition2D();
    public Point2D endPosWorld = new DirectPosition2D();
    
    private GeometryType geometryType;
    
    //search stuff
    JPanel searchPanel = new JPanel();
    JTextField f = new JTextField(50);
    JButton search = new JButton("Search");
    JPanel infoPanel = new JPanel(new BorderLayout());
    JTable infoTable = new JTable();
    JScrollPane scrollPane = new JScrollPane(infoTable);
    
    //select
    JButton select = new JButton("select");
    private Rectangle selectionRectangle;
    private Layer lastSelected = null;
    private SimpleFeatureCollection selectedFeatures = FeatureCollections
			.newCollection();
    JButton deselect = new JButton("deselect");
    JButton zoomToSelect = new JButton("zoom to select");
    
    //display table info on mape
    JButton displayOnMap = new JButton("Display info");
    
    //pirma dalis
    JButton part1 = new JButton("Part I");
    JFrame part1frame = new JFrame();
    
    public void Start(){
        //map.setTitle("GIS Tool v1.0");
        this.DecorateMap();
        this.setMapContent(map);
        this.initPart1();
        this.setVisible(true);
    }
    
    private void initPart1(){
        JButton count = new JButton("Calculate");
        count.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                part1frame.setVisible(true);
            }
        });
        
        
        toolbar = getToolBar();
        toolbar.add(part1);
        part1frame.setTitle("part 1");
        Container content = part1frame.getContentPane();
        content.setLayout(new FlowLayout());
        part1frame.add(count);
        
        //table
        String columnNames[] = { "Pavadinimas", "Hydro", "Keliai" };
        String dataValues[][] =
		{
			{ "12", "234", "67" },
			{ "-123", "43", "853" },
			{ "93", "89.2", "109" },
			{ "279", "9033", "3092" }
		};
        JTable table = new JTable(dataValues, columnNames );
        table.setVisible(false);
        JScrollPane scrollPane = new JScrollPane( table );
        part1frame.add(table);
        
        part1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                part1frame.setVisible(true);
            }
        });
    }
    
    public void DecorateMap(){
        this.setSize(800, 600);
        this.getContentPane().setLayout(new BorderLayout());
        this.enableToolBar(true);
        this.enableLayerTable(true);
        this.enableStatusBar(true);
        this.AddTopMenu();
        this.initCostumButtons();
        this.addSearch();
    }
    
    private void addSearch(){
        searchPanel.add(f); 
        searchPanel.add(search);
        
        infoPanel.add(searchPanel, BorderLayout.NORTH);
        infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        infoTable.setModel(new DefaultTableModel(0, 0));
        infoTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        
        search.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                    Map.this.executeQuery(Map.this.f.getText());
                    //Map.this.displayOnMap.setEnabled(true);
            }
        });
        
        infoPanel.add(scrollPane, BorderLayout.CENTER); 
        this.getContentPane().add(infoPanel, BorderLayout.SOUTH);
	this.pack();
    }
    
    private void AddTopMenu(){
        JMenuBar m = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem menuItem = new JMenuItem(new AbstractAction("Add layer") {
            public void actionPerformed(ActionEvent e) {
                File file = JFileDataStoreChooser.showOpenFile("shp", null);
                if (file == null) {
                    return;
                }
                try{
                    FileDataStore store = FileDataStoreFinder.getDataStore(file);
                    SimpleFeatureSource featureSource = store.getFeatureSource();
                    Style style = SLD.createSimpleStyle(featureSource.getSchema());
                    Layer layer = new FeatureLayer(featureSource, style);
                    layer.setTitle("test");
                    layer.setVisible(false);
                    that.getMapContent().addLayer(layer);
                }catch(Exception ex){
                    
                }
            }
        });
        fileMenu.add(menuItem);
        m.add(fileMenu);;
        this.setJMenuBar(m);
    } 
    
    private void executeQuery(String queryText) {
        Layer selectedLayer = this.getSelectedLayer();
        if (selectedLayer == null) {
                JOptionPane.showMessageDialog(this,
                                "Please select a layer, before searching.");
                return;
        }
        SimpleFeatureSource source = (SimpleFeatureSource) selectedLayer
                        .getFeatureSource();
        FeatureType schema = source.getSchema();

        String name = schema.getGeometryDescriptor().getLocalName();

        try {
            Filter filter = CQL.toFilter(queryText);

            @SuppressWarnings("unused")
            Query query = new Query(schema.getName().getLocalPart(), filter,
                            new String[] { name });

            SimpleFeatureCollection features = source.getFeatures(filter);

            FeatureCollectionTableModel model = new FeatureCollectionTableModel(
                            features);
            infoTable.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void initCostumButtons(){
        select.setToolTipText("Select item(s)");
        deselect.setToolTipText("Deselect item(s)");
        toolbar = getToolBar();
        toolbar.add(select);
        toolbar.add(deselect);
        toolbar.add(zoomToSelect);
        toolbar.add(displayOnMap);
        
        select.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Map.this.getMapPane().setCursorTool(new SelectTool(Map.this));
            }
        });
        
        deselect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                Map.this.deSelect();

            }
        });
        
        zoomToSelect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Map.this.zoomToSelection();
            }
        });
        
        displayOnMap.addActionListener(new ActionListener() {

            @SuppressWarnings("unchecked")
            public void actionPerformed(ActionEvent e) {
                if (Map.this.lastSelected == null) {
                    Map.this.lastSelected = Map.this.getSelectedLayer();
                }
                if (Map.this.lastSelected == null) {
                    return;
                }
                // if(!Map.this.lastSelected.equals(Map.this.getSelectedLayer())){
                Map.this.deSelect();
                // }

                Map.this.setGeometry(Map.this.getSelectedLayer()
                    .getFeatureSource().getSchema().getGeometryDescriptor());
                int[] selectedRows = Map.this.infoTable.getSelectedRows();
                if (selectedRows.length == 0) {
                    Map.this.infoTable.selectAll();
                    selectedRows = Map.this.infoTable.getSelectedRows();
                }
                Set<FeatureId> IDs = new HashSet<FeatureId>();
                @SuppressWarnings("rawtypes")
                FeatureCollection selectedFeatures = new ListFeatureCollection(
                    (SimpleFeatureType) Map.this.getSelectedLayer()
                                                .getFeatureSource().getSchema());
                for (int i = 0; i < selectedRows.length; i++) {
                String featureID = (String) Map.this.infoTable.getValueAt(
                    selectedRows[i], 0);

                try {
                    @SuppressWarnings("rawtypes")
                    FeatureCollection allFeatures = Map.this
                        .getSelectedLayer().getFeatureSource()
                        .getFeatures();
                    FeatureIterator<Feature> iter = allFeatures.features();
                    while (iter.hasNext()) {
                        Feature feature = iter.next();
                        if (feature.getIdentifier().getID()
                                        .equals(featureID)) {
                                selectedFeatures.add(feature);
                                IDs.add(feature.getIdentifier());
                                break;
                        }
                    }
                } catch (IOException e1) {
                        e1.printStackTrace();
                }
                }
                Map.this.selectedFeatures.clear();
                Map.this.selectedFeatures.addAll(selectedFeatures);
                Map.this.displaySelectedFeatures(IDs);
            }
        });
    }
    
    void selectFeatures(MapMouseEvent ev) {

        System.out.println("Mouse click at: " + ev.getMapPosition());

        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        Point screenPos = ev.getPoint();
        Rectangle screenRect = new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);
        
        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = this.getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                this.getMapContent().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(bbox));

        /*
         * Use the filter to identify the selected features
         */
        try {
            SimpleFeatureCollection selectedFeatures =
                    featureSource.getFeatures(filter);

            SimpleFeatureIterator iter = selectedFeatures.features();
            Set<FeatureId> IDs = new HashSet<FeatureId>();
            try {
                while (iter.hasNext()) {
                    SimpleFeature feature = iter.next();
                    IDs.add(feature.getIdentifier());

                    System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
            }

            if (IDs.isEmpty()) {
                System.out.println("   no feature selected");
            }

            displaySelectedFeatures(IDs);

        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        
    }
    }
    
    public void selectFeatures(Rectangle rectangle) {
            
		this.selectionRectangle = rectangle;
		Layer layer = this.map.layers().get(0);
		if (layer == null) {
			JOptionPane.showMessageDialog(this,
					"Please select a layer, to select from.");
			return;
		} else if (this.lastSelected != null) {
			if (!this.lastSelected.equals(layer)) {
				this.deSelect();
			}
		}
		lastSelected = layer;
		this.setGeometry(layer.getFeatureSource().getSchema()
				.getGeometryDescriptor());
		AffineTransform screenToWorld = this.getMapPane()
				.getScreenToWorldTransform();
		Rectangle2D worldRect = screenToWorld.createTransformedShape(rectangle)
				.getBounds2D();
		ReferencedEnvelope bbox = new ReferencedEnvelope(worldRect, this
				.getMapContent().getCoordinateReferenceSystem());
		/*
		 * Create a Filter to select features that intersect with the bounding
		 * box
		 */
		Filter filter = ff.intersects(ff.property(geometryAttributeName),
				ff.literal(bbox));

		/*
		 * Use the filter to identify the selected features
		 */
		try {
                   SimpleFeatureCollection selectedFeatures = featureSource.getFeatures(filter);
                    
			//SimpleFeatureCollection selectedFeatures = (SimpleFeatureCollection) layer.getFeatureSource().getFeatures(filter);
			this.selectedFeatures.addAll(selectedFeatures);

			if (true) {
				FeatureCollectionTableModel tableModel = new FeatureCollectionTableModel(
						this.selectedFeatures);

				this.infoTable.setModel(tableModel);
				FeatureIterator iter = this.selectedFeatures.features();
				Set<FeatureId> IDs = new HashSet<FeatureId>();
				try {
					while (iter.hasNext()) {
						Feature feature = iter.next();
						IDs.add(feature.getIdentifier());

						// System.out.println("   " + feature.getIdentifier());
					}

				} finally {
					iter.close();
				}

				if (IDs.isEmpty()) {
					// System.out.println("   no feature selected");
				}

				displaySelectedFeatures(IDs);
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return;
		}

	}
    
    private void deSelect(){
        this.selectedFeatures = FeatureCollections.newCollection();

        List<Layer> layers = this.getMapContent().layers();
        for (Layer element : layers) {
                this.setGeometry(element.getFeatureSource().getSchema()
                                .getGeometryDescriptor());
                Style style = createDefaultStyle();
                ((FeatureLayer) element).setStyle(style);
        }
        if (!this.lastSelected.equals(this.getSelectedLayer())) {
                infoTable.setModel(new DefaultTableModel(0, 0));
        }
    }
    
    private void zoomToSelection() {
        FeatureIterator iter = selectedFeatures.features();
        double leftX = -1;
        double leftY = -1;
        double rightX = -1;
        double rightY = -1;
        while (iter.hasNext()) {
            BoundingBox box = iter.next().getBounds();
            rightX = (rightX == -1 || box.getMaxX() > rightX) ? box.getMaxX()
                            : rightX;
            rightY = (rightY == -1 || box.getMaxY() > rightY) ? box.getMaxY()
                            : rightY;
            leftX = (leftX == -1 || box.getMinX() < leftX) ? box.getMinX()
                            : leftX;
            leftY = (leftY == -1 || box.getMinY() < leftY) ? box.getMinY()
                            : leftY;
        }
        if (rightX == -1 || leftX == -1 || rightY == -1 || leftY == -1) {
            return;
        } else if (rightX == leftX) {
            if (rightX == 0) {
                rightX += 10;
            } else {
                leftX -= 10;
                leftX = (leftX < 0) ? 0 : leftX;
            }
        } else if (rightY == leftY) {
            if (rightY == 0) {
                rightY += 10;
            } else {
                leftY -= 10;
                leftY = (leftY < 0) ? 0 : leftY;
            }
        }
        Envelope2D envelope = new Envelope2D();
        envelope.setFrameFromDiagonal(leftX, leftY, rightX, rightY);
        getMapPane().setDisplayArea(envelope);
    }
    
    /**
    * Retrieve information about the feature geometry
    */
    private void setGeometry(GeometryDescriptor geoD) {
        GeometryDescriptor geomDesc = geoD;
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz)
                    || MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeometryType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz)
                    || MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeometryType.LINE;

        } else {
            geometryType = GeometryType.POINT;
        }

    }
        
    private void displaySelectedFeatures(Set<FeatureId> IDs) {
        Style style;

        if (IDs.isEmpty()) {
                style = createDefaultStyle();

        } else {
                style = createSelectedStyle(IDs);
        }

        Layer layer = this.getMapContent().layers().get(0);
        ((FeatureLayer) layer).setStyle(style);
        this.repaint();
    }
    
    /**
    * Create a default Style for feature display
    */
    private Style createDefaultStyle() {
        Rule rule = createRule(DEFAULT_LINE, DEFAULT_FILL);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(rule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    private Style createSelectedStyle(Set<FeatureId> IDs) {
        Rule selectedRule = createRule(SELECTED_COLOUR, SELECTED_COLOUR);
        selectedRule.setFilter(ff.id(IDs));

        Rule otherRule = createRule(DEFAULT_LINE, DEFAULT_FILL);
        otherRule.setElseFilter(true);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(selectedRule);
        fts.rules().add(otherRule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }
    
    /**
    * Helper for createXXXStyle methods. Creates a new Rule containing a
    * Symbolizer tailored to the geometry type of the features that we are
    * displaying.
    */
    private Rule createRule(Color outlineColor, Color fillColor) {
        Symbolizer symbolizer = null;
        Fill fill = null;
        Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(LINE_WIDTH));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
        }

        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }
    
    protected Layer getSelectedLayer() {
        List<Layer> layers = this.getMapContent().layers();
        for (Layer element : layers) {
            if (element.isSelected()) {
                    return element;
            }
        }
        return null;
    }
    
    protected Layer getSelectedLayer2() {
        List<Layer> layers = this.getMapContent().layers();
        boolean b = false;
        for (Layer element : layers) {
            if (element.isSelected()) {
                    b = true;
            }
            if (element.isSelected() && b) {
                    return element;
            }
        }
        return null;
    }
    
    
}
