/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.tutorial;

import java.util.ArrayList;
import java.util.List;

import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.feature.type.AttributeDescriptorImpl;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.filter.text.cql2.CQLException;
import org.geotools.geometry.jts.GeometryCollector;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.geotools.referencing.CRS;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.Name;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.geometry.BoundingBox;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.NoSuchAuthorityCodeException;


import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryCollection;
import com.vividsolutions.jts.geom.GeometryCollectionIterator;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.util.AffineTransformation;
import com.vividsolutions.jts.util.Assert;
public class Attributes {
    
     public static SimpleFeature addAttribute(SimpleFeature original, String attributeName, Class<?> clazz, Object value) {
        return addAttributes(original, new String[] { attributeName }, new Class<?>[] { clazz }, new Object[] { value });
    }

    public static SimpleFeature addAttributes(SimpleFeature original, String[] attributeNames, Class<?>[] classes, Object[] values) {
        Assert.isTrue(attributeNames.length == classes.length);
        Assert.isTrue(attributeNames.length == values.length);

        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        SimpleFeatureType originalType = original.getFeatureType();

        typeBuilder.setCRS(originalType.getCoordinateReferenceSystem());
        typeBuilder.setName(originalType.getName());
        typeBuilder.addAll(originalType.getAttributeDescriptors());

        for (int i = 0; i < attributeNames.length; i++) {
            typeBuilder.add(attributeNames[i], classes[i]);
        }

        SimpleFeatureType type = typeBuilder.buildFeatureType();

        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(type);

        builder.addAll(original.getAttributes());

        for (int i = 0; i < values.length; i++) {
            builder.add(values[i]);
        }

        SimpleFeature result = builder.buildFeature(original.getID());
        result.setDefaultGeometry(original.getDefaultGeometry());

        return result;
    }
    
}
