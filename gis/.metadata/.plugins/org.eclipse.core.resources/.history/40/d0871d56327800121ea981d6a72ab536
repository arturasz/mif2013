/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.geotools.first.first;

import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiLineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Polygon;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.Query;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.Envelope2D;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.*;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;
import org.opengis.geometry.BoundingBox;

/**
 *
 * @author n0mercy
 */
public class Map extends JMapFrame {

    private static final long serialVersionUID = 2487972778765829856L;
    private static ImageIcon selectIco = new ImageIcon(
            "E://Projects//git//MIF2013//gis//1//first//src//main//java//org//geotools//first//first//select.png");
    private static ImageIcon deselectIco = new ImageIcon(
            "E://Projects//git//MIF2013//gis//1//first//src//main//java//org//geotools//first//first//deselect.png");
    private static ImageIcon zoomToSelectIco = new ImageIcon(
            "E://Projects//git//MIF2013//gis//1//first//src//main//java//org//geotools//first//first//zoom-selection.png");
    private MapContent map = new MapContent();
    private StyleFactory sf = CommonFactoryFinder.getStyleFactory(null);
    private FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);
    private SimpleFeatureCollection selectedFeatures = FeatureCollections.newCollection();
    private Layer lastSelected = null;    

    private enum GeometryType {

        POINT, LINE, POLYGON
    };

    /*
     * Some default style variables
     */
    private static final Color DEFAULT_LINE = Color.BLACK;
    private static final Color DEFAULT_FILL = Color.WHITE;
    private static final Color LINE_COLOUR = Color.BLUE;
    private static final Color FILL_COLOUR = Color.CYAN;
    private static final Color SELECTED_COLOUR = Color.RED;
    private static final float OPACITY = 1.0f;
    private static final float LINE_WIDTH = 1.0f;
    private static final float POINT_SIZE = 3.5f;
    private String geometryAttributeName;
    private GeometryType geometryType;
    private JMenuBar menubar = new JMenuBar();
    private JToolBar toolbar;
    private JButton select = new JButton(selectIco);
    private JButton deselect = new JButton(deselectIco);
    private JButton zoomToSelect = new JButton(zoomToSelectIco);
    private JPanel infoPanel = new JPanel(new BorderLayout());
    private JPanel searchPanel = new JPanel();
    private JTable infoTable = new JTable();
    private JTextField searchField = new JTextField(50);
    private JButton searchButton = new JButton("Search");
    private JButton displayOnMapButton = new JButton("Display info");
    ;
    private JScrollPane scrollPane = new JScrollPane(infoTable);

    private void setToolbar() {
        select.setToolTipText("Select item(s)");
        select.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                Map.this.getMapPane().setCursorTool(new SelectTool(Map.this));
            }
        });

        deselect = new JButton(deselectIco);
        deselect.setToolTipText("Deselect all selected items");
        deselect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                Map.this.deSelect();

            }
        });

        zoomToSelect.setToolTipText("Zoom to selection");
        zoomToSelect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                Map.this.zoomToSelection();

            }
        });

        toolbar = getToolBar();
        toolbar.addSeparator();
        toolbar.add(select);
        toolbar.add(zoomToSelect);
        toolbar.add(deselect);
    }

    private void setMenuBar() {
        JMenu layerMenu = new JMenu("Layer");
        JMenuItem layerMenuItem = new JMenuItem("Add layer...");
        layerMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                File file = JFileDataStoreChooser.showOpenFile("shp", new File("E:\\Projects\\univeras\\GIS\\GIS1\\gisData\\LT250shp"), null);
                if (file == null) {
                    return;
                } else {
                    Map.this.addLayer(file);
                }

            }
        });
        layerMenu.add(layerMenuItem);
        menubar.add(layerMenu);
        this.setJMenuBar(menubar);
    }
    
    private void setSearchPanel() {
        searchField.setText("include");
        searchPanel.add(searchField);
        searchPanel.add(searchButton);
        displayOnMapButton.setEnabled(false);
        searchPanel.add(displayOnMapButton);
        infoPanel.add(searchPanel, BorderLayout.NORTH);
        infoTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        infoTable.setModel(new DefaultTableModel(0, 0));
        infoTable.setPreferredScrollableViewportSize(new Dimension(500, 200));
        searchButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent arg0) {
                Map.this.executeQuery(Map.this.searchField.getText());
                Map.this.displayOnMapButton.setEnabled(true);
            }
        });
        displayOnMapButton.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                //if (Map.this.lastSelected == null) {
                Map.this.lastSelected = Map.this.getSelectedLayer();
                //}
                if (Map.this.lastSelected == null) {
                    return;
                }
                    
                Map.this.deSelect();

                Map.this.setGeometry(Map.this.getSelectedLayer().getFeatureSource().getSchema().getGeometryDescriptor());
                int[] selectedRows = Map.this.infoTable.getSelectedRows();
                if (selectedRows.length == 0) {
                    Map.this.infoTable.selectAll();
                    selectedRows = Map.this.infoTable.getSelectedRows();
                }
                Set<FeatureId> IDs = new HashSet<FeatureId>();
                FeatureCollection selectedFeatures = new ListFeatureCollection(
                        (SimpleFeatureType) Map.this.getSelectedLayer().getFeatureSource().getSchema());
                for (int i = 0; i < selectedRows.length; i++) {
                    String featureID = (String) Map.this.infoTable.getValueAt(
                            selectedRows[i], 0);

                    try {
                        FeatureCollection allFeatures = Map.this.getSelectedLayer().getFeatureSource().getFeatures();
                        FeatureIterator<Feature> iter = allFeatures.features();
                        while (iter.hasNext()) {
                            Feature feature = iter.next();
                            if (feature.getIdentifier().getID().equals(featureID)) {
                                selectedFeatures.add(feature);
                                IDs.add(feature.getIdentifier());
                                break;
                            }
                        }
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                Map.this.selectedFeatures.addAll(selectedFeatures);
                Map.this.displaySelectedFeatures(IDs);
            }
        });
        infoPanel.add(scrollPane, BorderLayout.CENTER);
        this.getContentPane().add(infoPanel, BorderLayout.SOUTH);
    }

    public void run() {
        map.setTitle("GISas v1.0");
        this.getContentPane().setLayout(new BorderLayout());
        this.enableToolBar(true);
        this.enableLayerTable(true);
        this.enableStatusBar(true);

        setToolbar();

        setMenuBar();

        setSearchPanel();        

        this.pack();
        this.setMapContent(map);
        this.setSize(800, 600);
        this.setMinimumSize(new Dimension(850, 700));
        this.setMaximumSize(new Dimension(1366, 768));
        this.setLocation(250, 50);
        this.setVisible(true);
    }

    private void executeQuery(String queryText) {
        Layer selectedLayer = this.getSelectedLayer();
        if (selectedLayer == null) {
            JOptionPane.showMessageDialog(this,
                    "Please select a layer, before searching.");
            return;
        }
        SimpleFeatureSource source = (SimpleFeatureSource) selectedLayer.getFeatureSource();
        FeatureType schema = source.getSchema();

        String name = schema.getGeometryDescriptor().getLocalName();

        try {
            Filter filter = CQL.toFilter(queryText);
            
            Query query = new Query(schema.getName().getLocalPart(), filter,
                    new String[]{name});

            SimpleFeatureCollection features = source.getFeatures(filter);

            FeatureCollectionTableModel model = new FeatureCollectionTableModel(
                    features);
            infoTable.setModel(model);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected Layer getSelectedLayer() {
        List<Layer> layers = this.getMapContent().layers();
        for (Layer element : layers) {
            //System.out.println("isSelected: " + element.isSelected() + " name:" + element.getFeatureSource().getName());
            if (element.isSelected()) {
                return element;
            }
        }
        return null;
    }

    private void addLayer(File file) {
        try {
            FileDataStore store = FileDataStoreFinder.getDataStore(file);
            SimpleFeatureSource featureSource = store.getFeatureSource();
            Style style = SLD.createSimpleStyle(featureSource.getSchema());
            Layer layer = new FeatureLayer(featureSource, style);
            this.getMapContent().addLayer(layer);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectFeatures(Rectangle rectangle) {
        Layer layer = this.getSelectedLayer();
        if (layer == null) {
            JOptionPane.showMessageDialog(this,
                    "Please select a layer, to select from.");
            return;
        } else if (this.lastSelected != null) {
            if (!this.lastSelected.equals(layer)) {
                this.deSelect();
            }
        }
        lastSelected = layer;

        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive
         * method here but GeoTools also offers other, more accurate methods.
         */
        this.setGeometry(layer.getFeatureSource().getSchema().getGeometryDescriptor());
        AffineTransform screenToWorld = this.getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(rectangle).getBounds2D();
        // System.out.println(worldRect);
        ReferencedEnvelope bbox = new ReferencedEnvelope(worldRect, this.getMapContent().getCoordinateReferenceSystem());
        /*
         * Create a Filter to select features that intersect with the bounding
         * box
         */
        Filter filter = ff.intersects(ff.property(geometryAttributeName),
                ff.literal(bbox));

        /*
         * Use the filter to identify the selected features
         */
        try {
            SimpleFeatureCollection selectedFeatures = (SimpleFeatureCollection) layer.getFeatureSource().getFeatures(filter);
            this.selectedFeatures.addAll(selectedFeatures);

            FeatureCollectionTableModel tableModel = new FeatureCollectionTableModel(
                    this.selectedFeatures);
            this.infoTable.setModel(tableModel);
            FeatureIterator iter = this.selectedFeatures.features();
            Set<FeatureId> IDs = new HashSet<FeatureId>();
            try {
                while (iter.hasNext()) {
                    Feature feature = iter.next();
                    IDs.add(feature.getIdentifier());

                    // System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
            }

            if (IDs.isEmpty()) {
                // System.out.println("   no feature selected");
            }

            displaySelectedFeatures(IDs);

        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }

    }

    private void displaySelectedFeatures(Set<FeatureId> IDs) {
        Style style;

        if (IDs.isEmpty()) {
            style = createDefaultStyle();

        } else {
            style = createSelectedStyle(IDs);
        }

        ((FeatureLayer) lastSelected).setStyle(style);
        this.repaint();
    }

    /**
     * Create a default Style for feature display
     */
    private Style createDefaultStyle() {
        Rule rule = createRule(DEFAULT_LINE, DEFAULT_FILL);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(rule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    private Style createSelectedStyle(Set<FeatureId> IDs) {
        Rule selectedRule = createRule(SELECTED_COLOUR, SELECTED_COLOUR);
        selectedRule.setFilter(ff.id(IDs));

        Rule otherRule = createRule(DEFAULT_LINE, DEFAULT_FILL);
        otherRule.setElseFilter(true);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(selectedRule);
        fts.rules().add(otherRule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }

    /**
     * Helper for createXXXStyle methods. Creates a new Rule containing a
     * Symbolizer tailored to the geometry type of the features that we are
     * displaying.
     */
    private Rule createRule(Color outlineColor, Color fillColor) {
        Symbolizer symbolizer = null;
        Fill fill = null;
        Stroke stroke = sf.createStroke(ff.literal(outlineColor),
                ff.literal(LINE_WIDTH));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill,
                        geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic,
                        geometryAttributeName);
        }

        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }

    /**
     * Retrieve information about the feature geometry
     */
    private void setGeometry(GeometryDescriptor geoD) {
        GeometryDescriptor geomDesc = geoD;
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz)
                || MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeometryType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz)
                || MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeometryType.LINE;

        } else {
            geometryType = GeometryType.POINT;
        }

    }

    private void deSelect() {
        this.selectedFeatures = FeatureCollections.newCollection();

        List<Layer> layers = this.getMapContent().layers();
        for (Layer element : layers) {
            this.setGeometry(element.getFeatureSource().getSchema().getGeometryDescriptor());
            Style style = createDefaultStyle();
            ((FeatureLayer) element).setStyle(style);
        }
        if (!this.lastSelected.equals(this.getSelectedLayer())) {
            infoTable.setModel(new DefaultTableModel(0, 0));
        }
    }

    private void zoomToSelection() {
        FeatureIterator iter = selectedFeatures.features();
        double leftX = -1;
        double leftY = -1;
        double rightX = -1;
        double rightY = -1;
        while (iter.hasNext()) {
            BoundingBox box = iter.next().getBounds();
            rightX = (rightX == -1 || box.getMaxX() > rightX) ? box.getMaxX()
                    : rightX;
            rightY = (rightY == -1 || box.getMaxY() > rightY) ? box.getMaxY()
                    : rightY;
            leftX = (leftX == -1 || box.getMinX() < leftX) ? box.getMinX()
                    : leftX;
            leftY = (leftY == -1 || box.getMinY() < leftY) ? box.getMinY()
                    : leftY;
        }
        if (rightX == -1 || leftX == -1 || rightY == -1 || leftY == -1) {
            return;
        } else if (rightX == leftX) {
            if (rightX == 0) {
                rightX += 10;
            } else {
                leftX -= 10;
                leftX = (leftX < 0) ? 0 : leftX;
            }
        } else if (rightY == leftY) {
            if (rightY == 0) {
                rightY += 10;
            } else {
                leftY -= 10;
                leftY = (leftY < 0) ? 0 : leftY;
            }
        }
        Envelope2D envelope = new Envelope2D();
        envelope.setFrameFromDiagonal(leftX, leftY, rightX, rightY);
        getMapPane().setDisplayArea(envelope);
    }
}
