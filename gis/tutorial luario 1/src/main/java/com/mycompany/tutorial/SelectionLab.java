/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.tutorial;

import com.vividsolutions.jts.geom.*;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JToolBar;

import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.geometry.jts.ReferencedEnvelope;
import org.geotools.map.Layer;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Fill;
import org.geotools.styling.Graphic;
import org.geotools.styling.Mark;
import org.geotools.styling.Rule;
import org.geotools.styling.Stroke;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.geotools.styling.Symbolizer;
import org.geotools.swing.JMapFrame;
import org.geotools.swing.data.JFileDataStoreChooser;
import org.geotools.swing.event.MapMouseEvent;
import org.geotools.swing.tool.CursorTool;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.type.GeometryDescriptor;
import org.opengis.filter.Filter;
import org.opengis.filter.FilterFactory2;
import org.opengis.filter.identity.FeatureId;

import com.vividsolutions.jts.geom.Polygon;
import java.awt.*;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import org.geotools.data.Query;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.filter.text.cql2.CQL;
import org.geotools.geometry.Envelope2D;
import org.geotools.map.*;
import org.geotools.styling.*;
import org.geotools.swing.table.FeatureCollectionTableModel;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.FeatureType;
import org.opengis.geometry.BoundingBox;

import com.vividsolutions.jts.geom.Geometry;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.geotools.data.FeatureSource;
import org.geotools.referencing.Console;

/**
 * In this example we create a map tool to select a feature clicked
 * with the mouse. The selected feature will be painted yellow.
 *
 * @source $URL: http://svn.osgeo.org/geotools/branches/2.7.x/demo/example/src/main/java/org/geotools/demo/SelectionLab.java $
 */
public class SelectionLab extends JMapFrame {
    
    private float maxHeightDifference;
    /*
     * Factories that we will use to create style and filter objects
     */
    private StyleFactory sf = CommonFactoryFinder.getStyleFactory(null);
    private FilterFactory2 ff = CommonFactoryFinder.getFilterFactory2(null);

    /*
     * Convenient constants for the type of feature geometry in the shapefile
     */
    private enum GeomType { POINT, LINE, POLYGON };

    /*
     * Some default style variables
     */
    private static final Color LINE_COLOUR = Color.BLUE;
    private static final Color LINE_SELECTED_COLOUR = Color.RED;
    private static final Color FILL_COLOUR = Color.CYAN;
    private static final Color FILL1_COLOUR = Color.GREEN;
    private static final Color FILL2_COLOUR = Color.ORANGE;
    private static final Color SELECTED_COLOUR = Color.YELLOW;
    private static final float OPACITY = 1.0f;
    private static final float LINE_WIDTH = 1.0f;
    private static final float POINT_SIZE = 10.0f;
    
    private MapContext map;

    public JMapFrame mapFrame;
    private SimpleFeatureSource featureSource, featureSource2;

    private String geometryAttributeName;
    private GeomType geometryType;
    
    private double minX = -1;
    private double maxX = -1;
    private double minY = -1;
    private double maxY = -1;
    
    private JTextField text;
    private JTable table;
    
    private int color = 0;
    
    SimpleFeatureCollection gSelectedFeatures;
    Rectangle searchArea;
    
    private int lastSelectedLayerIndex = -1;
    
    public DefaultTableModel model;
    /*
     * The application method
     */
    public static void main(String[] args) throws Exception {
        SelectionLab me = new SelectionLab();

        /*File file = JFileDataStoreChooser.showOpenFile("shp", null);
        if (file == null) {
            return;
        }*/

        me.displayShapefile();
    }
    
    /**
     * This method connects to the shapefile; retrieves information about
     * its features; creates a map frame to display the shapefile and adds
     * a custom feature selection tool to the toolbar of the map frame.
     */
    public void displayShapefile() throws Exception {
        //FileDataStore store = FileDataStoreFinder.getDataStore(file);
        //featureSource = store.getFeatureSource();
        //setGeometry();

        /*
         * Create the JMapFrame and set it to display the shapefile's features
         * with a default line and colour style
         */
        //map = new DefaultMapContext();
        //map.setTitle("Test da test");
        //Style style = createDefaultStyle();
        //map.addLayer(featureSource, style);
        //mapFrame = new JMapFrame(map);
        //mapFrame = new JMapFrame();
        mapFrame = new JMapFrame(new MapContent());
        mapFrame.enableToolBar(true);
        mapFrame.enableStatusBar(true);
        mapFrame.enableLayerTable(true);
        //mapFrame.setMapContext(map);
        
        /*
         * Before making the map frame visible we add a new button to its
         * toolbar for our custom feature selection tool
         */
        JToolBar toolBar = mapFrame.getToolBar();
        
        JButton btnSelect = new JButton("Select");
        toolBar.addSeparator();
        toolBar.add(btnSelect);
        
        JButton btnMultiSelect = new JButton("Multi Select");
        toolBar.add(btnMultiSelect);
        
        JButton btnDeselect = new JButton("Deselect");
        toolBar.add(btnDeselect);

        JButton btnZoom = new JButton("Zoom Selected");
        toolBar.addSeparator();
        toolBar.add(btnZoom);
        
        JButton btnReset = new JButton("Reset");
        toolBar.add(btnReset);
        
        JButton test = new JButton("Test");
        toolBar.add(test);
        
        //part 1 frame set up
        
        final JFrame part1 = new JFrame();
        part1.setSize(300, 400);
        part1.setLayout(new BorderLayout());
        
        JLabel hidroLabel = new JLabel("Hidro length");
        JLabel hidroValue = new JLabel("0");
        
        JLabel roadsLabel = new JLabel("Roads length");
        JLabel roadsValue = new JLabel("0");
        
        JButton start = new JButton("Go");
        
        
        
        String[] columnNames = {"District",
                        "District area",
                        "Roads",
                        "Rivers",
                        "Hidro",
                        "medziai krumai",
                        "uzstatyta",
                        "sodai"};
        Object[][] data = {};
        
        model = new DefaultTableModel(data, columnNames) {


        };
        
        JTable table = new JTable(model);
        
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        part1.add(table.getTableHeader(), BorderLayout.PAGE_START);
        part1.add(table, BorderLayout.CENTER);
        
        Object[] row = {
            "Siauliai", 45, 56
        };
        model.addRow(row);
        
        //part1.add(start,BorderLayout.CENTER);
        
        
        
        
        
        
        test.addActionListener(new ActionListener(){
            
            public void actionPerformed(ActionEvent ae) {
                try {
                    countRoadsAndHidro();
                    
                    /*
                    
                    
                    try {
                        roads = (SimpleFeatureCollection) getLayerByName("keliai")
        .getFeatureSource().getFeatures();
                        hydro = (SimpleFeatureCollection) getLayerByName("hydro").getFeatureSource().getFeatures();
                    } catch (IOException ex) {
                        Logger.getLogger(SelectionLab.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    SimpleFeatureCollection apskritis = executeQuery(query,"apskritys");
                    SimpleFeatureCollection apskritisVSkeliai = 
                            new IntersectedFeatureCollection(apskritis,roads);
                    Style style = SLD.createSimpleStyle(apskritisVSkeliai.getSchema());
                    Layer l = new FeatureLayer(apskritisVSkeliai, style);
                    SimpleFeatureIterator feat = apskritisVSkeliai.features();
                    float length    = 0;
                    float floated   = 0;
                    while(feat.hasNext()){
                        floated = Float.parseFloat( feat.next().getAttribute("keliai_LENGTH").toString() );
                        length += floated;
                    }
                    
                    l.setTitle("utena");
                    mapFrame.getMapContent().addLayer(l);
                    * 
                    */
                } catch (IOException ex) {
                    Logger.getLogger(SelectionLab.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
            
        });
        
        test.addActionListener(new ActionListener(){

            public void actionPerformed(ActionEvent ae) {
                part1.setVisible(true);
                
                /*SimpleFeatureCollection roads = null;
                try {
                    roads = (SimpleFeatureCollection) getLayerByName("keliai")
    .getFeatureSource().getFeatures();
                } catch (IOException ex) {
                    Logger.getLogger(SelectionLab.class.getName()).log(Level.SEVERE, null, ex);
                }
                SimpleFeatureCollection a = executeQuery("APSVARDAS = 'Utenos'",3);
                SimpleFeatureCollection b = 
                        new IntersectedFeatureCollection(a,roads);
                Style style = SLD.createSimpleStyle(a.getSchema());
                Layer l = new FeatureLayer(b, style);
                SimpleFeatureIterator feat = b.features();
                float length    = 0;
                float floated   = 0;
                while(feat.hasNext()){
                    floated = Float.parseFloat( feat.next().getAttribute("keliai_LENGTH").toString() );
                    length += floated;
                }
                
                l.setTitle("utena");
                mapFrame.getMapContent().addLayer(l);
                */
            }
            
        });
        
        JButton uzsiimtiKontrabanda = new JButton("Uzsiimti kontrabanda");
        toolBar.addSeparator();
        
        JButton btnNewLayer = new JButton("New Layer...");
        //JButton btnNewLayer = new JButton("");
        //btnNewLayer.setIcon(new ImageIcon(SelectionLab.class.getResource("/org/geotools/swing/icons/open.gif")));
        btnNewLayer.setToolTipText("Open new layer");
        for (int i = 0; i < 10; i++)
            toolBar.addSeparator();
        toolBar.add(btnNewLayer);
        btnNewLayer.setEnabled(false);
        /*
         * When the user clicks the button we want to enable
         * our custom feature selection tool. Since the only
         * mouse action we are intersted in is 'clicked', and
         * we are not creating control icons or cursors here,
         * we can just create our tool as an anonymous sub-class
         * of CursorTool.
         */
        btnSelect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mapFrame.getMapPane().setCursorTool(
                        new CursorTool() {

                            @Override
                            public void onMouseClicked(MapMouseEvent ev) {
                                selectFeatures(ev);
                            }
                        });
            }
        });
        
        btnMultiSelect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mapFrame.getMapPane().setCursorTool(new MultiSelect(SelectionLab.this));
            }
        });
        
        btnNewLayer.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                addLayer2();
                color++;
            }
        });
        
        btnDeselect.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                deselect();
            }
        });
        
        btnZoom.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (minX != -1 && maxX != -1 && minY != -1 && maxY != -1) {
                    Envelope2D env = new Envelope2D();
                    env.setFrameFromDiagonal(minX, minY, maxX, maxY);
                    mapFrame.getMapPane().setDisplayArea(env);
                }
            }
        });
        
        btnReset.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                mapFrame.getMapPane().reset();
            }
        });
        
        uzsiimtiKontrabanda.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                //1. Pasirenkame rajonus, kuriuose ieškosime slidinėjimo trasų.
                
                //2. Atrenkam viršūnes, kurių aukštis didesnis nei 250 metrų
                //3. Atrinktas viršūnes buferizuojame, sukuriamas naujas sluoksnis
                /*SimpleFeatureCollection bufVirs = new BufferedFeatureCollection(executeQuery("AUKSTIS > " + slidinejimoTrasosAukstis, 2), "bufVirs", slidinejimoTrasosDydis);
                //Geometry virsGeom = ConversionUtils.getGeometries(mapFrame.getMapContent().layers().get(2));
                //Geometry bufVirs2 = virsGeom.buffer(10000.0);
                //map.addLayer(ConversionUtils.geometryToLayer(bufVirs2, "Buferizuotos viršūnės"));
                //bufVirs = addArea(bufVirs);
                Style style = SLD.createSimpleStyle(bufVirs.getSchema());
                MapContent map = mapFrame.getMapContent();
                Layer layer = new FeatureLayer(bufVirs, style);
                layer.setTitle("Buferizuotos viršūnės");
                map.addLayer(layer);
                
                
                
                //4. Rajonų sluoksnyje pasirinkti rajonai perdengiami su buferizuotų viršūniu sluoksniu
                //SimpleFeatureCollection feat = new DefaultFeatureCollection(executeQuery("RAJVARDAS = 'Vilniaus r.'", 4));
                SimpleFeatureCollection feat = new DefaultFeatureCollection(gSelectedFeatures);
                //SimpleFeatureCollection feat2 = new DefaultFeatureCollection(executeQuery("include", 1));  
                //SimpleFeatureCollection rajSanVirs = new IntersectedFeatureCollection(bufVirs, feat);
                SimpleFeatureCollection rajSanVirs = Intersection.intersection(feat, bufVirs, "rajSanVirs");
                rajSanVirs = addArea(rajSanVirs);
                Style style2 = SLD.createSimpleStyle(rajSanVirs.getSchema());
                MapContent map2 = mapFrame.getMapContent();
                Layer layer2 = new FeatureLayer(rajSanVirs, style2);
                layer2.setTitle("Raj San Su Virs");
                map2.addLayer(layer2);
                
                //5. Buferizuotų viršūnių sluoksnis dar kartą buferizuojamas (gaunama papėdė)
                //SimpleFeatureCollection papedesTemp = new BufferedFeatureCollection(executeQuery("include", 6), "test2", papedesTeritorijosDydis);
                //SimpleFeatureCollection papedes = Difference.difference(papedesTemp, rajSanVirs, "Papedes");
                
                Geometry virsGeom = ConversionUtils.getGeometries(rajSanVirs);
                Geometry minTownBuffer = virsGeom.buffer(-(papedesTeritorijosDydis));
                Geometry maxTownBuffer = virsGeom.buffer(0.0);
                Geometry papede = maxTownBuffer.difference(minTownBuffer);
                SimpleFeatureCollection papedes = ConversionUtils.geometryToFeatures(papede, "Papedes"); 
                
                Style style3 = SLD.createSimpleStyle(papedes.getSchema());
                MapContent map3 = mapFrame.getMapContent();
                Layer layer3 = new FeatureLayer(papedes, style3);
                layer3.setTitle("Papėdės");
                map3.addLayer(layer3);
                
                SimpleFeatureCollection kalnuPapedes = Intersection.intersection(rajSanVirs, papedes, "kalbuPapedes");
                //SimpleFeatureCollection pavirsius = executeQuery("include", 3);
                //SimpleFeatureCollection auksciai = getFeaturesWithCorrectHeightDifference(kalnuPapedes);
                Style style4 = SLD.createSimpleStyle(kalnuPapedes.getSchema());
                MapContent map4 = mapFrame.getMapContent();
                Layer layer4 = new FeatureLayer(kalnuPapedes, style4);
                layer4.setTitle("Papėd Aukščiai");
                map4.addLayer(layer4);
                
                
                
                //...
                //Buferizuojam kelių sluoksnį
                //SimpleFeatureCollection bufKeliai = new BufferedFeatureCollection(executeQuery("include", 0), "bufKeliai", atstumasIkiKelio);
                //Geometry geomKeliai = ConversionUtils.getGeometries(mapFrame.getMapContent().layers().get(0));
                //Geometry geomBufKeliai = geomKeliai.buffer(atstumasIkiKelio);
                //SimpleFeatureCollection bufKeliai = ConversionUtils.geometryToFeatures(geomKeliai, "bufKeliai");
                //Style style4 = SLD.createSimpleStyle(bufKeliai.getSchema());
                //MapContent map4 = mapFrame.getMapContent();
                //Layer layer4 = new FeatureLayer(bufKeliai, style4);
                //layer4.setTitle("Buferizuoti keliai");
                //map4.addLayer(layer4);
                
                //Padarome sankirtą su atrinktomis teritorijomis
                //SimpleFeatureCollection trasSanKel = Intersection.intersection(rajSanVirs, bufKeliai, "trasSanKel");
                //Style style5 = SLD.createSimpleStyle(trasSanKel.getSchema());
                //MapContent map5 = mapFrame.getMapContent();
                //Layer layer5 = new FeatureLayer(trasSanKel, style5);
                //layer5.setTitle("Tras San Kel");
                //map5.addLayer(layer5);*/
                
            }
        });
        
        queryPanel();

        /**
         * Finally, we display the map frame. When it is closed
         * this application will exit.
         */
        mapFrame.setSize(1100, 700);
        
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/upes.shp"), "upes");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/ezerai.shp"), "ezerai");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/keliai.shp"), "keliai");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/rajonai.shp"), "rajonai");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/apskrity.shp"), "apskritys");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/plotai.shp"), "plotai");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/HIDRO_L.shp"), "hidro_l");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/virsukal.shp"), "virsukalnes");
        addLayer2(new File("C:/Users/n0mercy/Documents/gisData/LT250shp/miskai.shp"), "miskai");
        

        
        for (int i = 0; i < this.mapFrame.getMapContent().layers().size() - 1; i++) {
            this.mapFrame.getMapContent().layers().get(i).setSelected(false);
            this.mapFrame.getMapContent().layers().get(i).setVisible(false);
        }
        mapFrame.setVisible(true);
    }
    
    public void countRoadsAndHidro() throws IOException{
        //intersect feature's layer with roads layer
        SimpleFeatureCollection roads = (SimpleFeatureCollection) getLayerByName("keliai")
            .getFeatureSource().getFeatures();
        
        SimpleFeatureCollection rivers = (SimpleFeatureCollection) getLayerByName("upes")
            .getFeatureSource().getFeatures();
        
        SimpleFeatureCollection plotai = (SimpleFeatureCollection) getLayerByName("plotai")
            .getFeatureSource().getFeatures();
        
        
        
        
        List<Object[]> result = null;
        
        SimpleFeatureIterator iter = gSelectedFeatures.features();
        while(iter.hasNext()){
            SimpleFeature districtFeature = iter.next();
            
            SimpleFeatureCollection c;
            c = new DefaultFeatureCollection("stuff", districtFeature.getType());
            c.add(districtFeature);
            
            Style style;
            //lets intersect this single distric layer with roads;
            SimpleFeatureCollection apskritisVSkeliai = 
                            new IntersectedFeatureCollection(c,roads);
            
            //lets intersect this single district layer with roads;
            SimpleFeatureCollection apskritisVSupes = 
                    new IntersectedFeatureCollection(c, rivers);
            
            SimpleFeatureCollection apskritisVSplotai = 
                    new IntersectedFeatureCollection(c, plotai);
            
            SimpleFeatureIterator i = apskritisVSkeliai.features();
            double keliaiLength = 0, upesLenght = 0,
                   hidrografijosPlotas = 0,
                   medziaiKrumai = 0,
                   uzstatytosTeritorijos = 0,
                   sodai = 0;
            while(i.hasNext()){
                keliaiLength += Double.parseDouble( i.next().getAttribute("keliai_LENGTH").toString() );
            }
            
            i = apskritisVSupes.features();
            
            while(i.hasNext()){
                upesLenght += Double.parseDouble( i.next().getAttribute("upes_LENGTH").toString() );
            }
            
            i = apskritisVSplotai.features();
            
            while(i.hasNext()){
                SimpleFeature sf = i.next();
                String plotai_gkodas = sf.getAttribute("plotai_GKODAS").toString();
                
                //hidrografija
                if(plotai_gkodas.equals("hd1") || plotai_gkodas.equals("hd2") || 
                    plotai_gkodas.equals("hd3") || plotai_gkodas.equals("hd4") ||
                    plotai_gkodas.equals("hd5") || plotai_gkodas.equals("hd9")){
                    hidrografijosPlotas += Double.parseDouble(sf.getAttribute("plotai_SHAPE_area").toString());
                }
                
                if( plotai_gkodas.equals("ms0") ) {
                    medziaiKrumai += Double.parseDouble(sf.getAttribute("plotai_SHAPE_area").toString());
                }
                
                if( plotai_gkodas.equals("pu0") ) {
                    uzstatytosTeritorijos += Double.parseDouble(sf.getAttribute("plotai_SHAPE_area").toString());
                }
                
                if( plotai_gkodas.equals("ms4") ) {
                    sodai += Double.parseDouble(sf.getAttribute("plotai_SHAPE_area").toString());
                }
            }
            
            double districtArea = Double.parseDouble( districtFeature.getAttribute("AREA").toString() );
            
            model.addRow(new Object[]{districtFeature.getAttribute("APSVARDAS").toString(), districtArea, keliaiLength, upesLenght, hidrografijosPlotas, medziaiKrumai, uzstatytosTeritorijos, sodai});    
            
        }
        
        //return result;
    }
    
    public Layer getLayerByName(String name) {
		List<Layer> layers = mapFrame.getMapContent().layers();
		for (Layer element : layers) {
			if (element.getTitle().equals(name)) {
				return element;
			}
		}
		return null;
	}
    
    /**
     * This method is called by our feature selection tool when
     * the user has clicked on the map.
     *
     * @param pos map (world) coordinates of the mouse cursor
     */
    void selectFeatures(MapMouseEvent ev) {

        setGeometry2();
        System.out.println("Mouse click at: " + ev.getMapPosition());

        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        Point screenPos = ev.getPoint();
        Rectangle screenRect = new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);
        
        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = mapFrame.getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                mapFrame.getMapContent().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(bbox));

        /*
         * Use the filter to identify the selected features
         */
        try {
            SimpleFeatureCollection selectedFeatures = //featureSource.getFeatures(filter);
                    (SimpleFeatureCollection) this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getFeatures(filter);

            FeatureCollectionTableModel model = new FeatureCollectionTableModel(selectedFeatures);
            table.setModel(model);
            SimpleFeatureIterator iter = selectedFeatures.features();
            Set<FeatureId> IDs = new HashSet<FeatureId>();
            try {
                while (iter.hasNext()) {
                    SimpleFeature feature = iter.next();
                    IDs.add(feature.getIdentifier());

                    System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
            }

            if (IDs.isEmpty()) {
                System.out.println("   no feature selected");
            }
            gSelectedFeatures = selectedFeatures;
            IDs = getSelectedFeatureIDs(selectedFeatures);
            displaySelectedFeatures(IDs);

        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }
    
    void selectMultiFeatures(Rectangle rec,boolean gui) {

        //System.out.println("Mouse click at: " + ev.getMapPosition());

        /*
         * Construct a 5x5 pixel rectangle centred on the mouse click position
         */
        //Point screenPos = ev.getPoint();
        this.searchArea = rec;
        if(getSelectedLayerIndex() != lastSelectedLayerIndex) {
        	deselect();
                gSelectedFeatures = new ListFeatureCollection((SimpleFeatureType)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
        }
        lastSelectedLayerIndex = getSelectedLayerIndex();
        setGeometry2();
        
        Rectangle screenRect = rec;//new Rectangle(screenPos.x-2, screenPos.y-2, 5, 5);
        
        /*
         * Transform the screen rectangle into bounding box in the coordinate
         * reference system of our map context. Note: we are using a naive method
         * here but GeoTools also offers other, more accurate methods.
         */
        AffineTransform screenToWorld = this.mapFrame.getMapPane().getScreenToWorldTransform();
        Rectangle2D worldRect = screenToWorld.createTransformedShape(screenRect).getBounds2D();
        ReferencedEnvelope bbox = new ReferencedEnvelope(
                worldRect,
                mapFrame.getMapContent().getCoordinateReferenceSystem());

        /*
         * Create a Filter to select features that intersect with
         * the bounding box
         */
        Filter filter = ff.intersects(ff.property(geometryAttributeName), ff.literal(bbox));

        /*
         * Use the filter to identify the selected features
         */
        try {
            System.out.println("Selected: " + getSelectedLayerIndex());
            SimpleFeatureCollection selectedFeatures = //featureSource.getFeatures(filter);
                    (SimpleFeatureCollection) this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getFeatures(filter);
            
            SimpleFeatureIterator iter = selectedFeatures.features();
            Set<FeatureId> IDs = new HashSet<FeatureId>();
            
            try {
                while (iter.hasNext()) {
                    SimpleFeature feature = iter.next(); {
                    if (!gSelectedFeatures.contains(feature))
                        gSelectedFeatures.add(feature);
                        IDs.add(feature.getIdentifier());
                    }
                    
                    System.out.println("   " + feature.getIdentifier());
                }

            } finally {
                iter.close();
            }
            if(gui){
	            FeatureCollectionTableModel model = new FeatureCollectionTableModel(gSelectedFeatures);
	            table.setModel(model);
	
	            if (IDs.isEmpty()) {
	                System.out.println("   no feature selected");
	            }
	            IDs = getSelectedFeatureIDs(gSelectedFeatures);
	            displaySelectedFeatures(IDs);

            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }
    
    
     /**
     * Sets the display to paint selected features yellow and
     * unselected features in the default style.
     *
     * @param IDs identifiers of currently selected features
     */
    public void displaySelectedFeatures(Set<FeatureId> IDs) {
        Style style;

        if (IDs.isEmpty()) {
            style = createDefaultStyle();

        } else {
            style = createSelectedStyle(IDs);
        }

        Layer layer = this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex());
        ((FeatureLayer) layer).setStyle(style);
        this.mapFrame.getMapPane().repaint();
    }
    
    public void addLayer2() {
        File file = JFileDataStoreChooser.showOpenFile("shp", mapFrame);
        if (file != null) {
            try {
                FileDataStore dataStore = FileDataStoreFinder.getDataStore(file);
                SimpleFeatureSource source = dataStore.getFeatureSource();
                Style style = SLD.createSimpleStyle(source.getSchema());
                MapContent map = mapFrame.getMapContent();
                Layer layer = new FeatureLayer(source, style);
                map.addLayer(layer);
                //setGeometry();
                gSelectedFeatures = new ListFeatureCollection((SimpleFeatureType)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void addLayer2(File file, String name) {
        if (file != null) {
            try {
                FileDataStore dataStore = FileDataStoreFinder.getDataStore(file);
                SimpleFeatureSource source = dataStore.getFeatureSource();
                Style style = SLD.createSimpleStyle(source.getSchema());
                MapContent map = mapFrame.getMapContent();
                Layer layer = new FeatureLayer(source, style);
                layer.setTitle(name);
                map.addLayer(layer);
                //setGeometry();
                gSelectedFeatures = new ListFeatureCollection((SimpleFeatureType)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
            }
            catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    public void addLayer() {
        //File file = JFileDataStoreChooser.showOpenFile("shp", null);
        File file = JFileDataStoreChooser.showOpenFile(new String[]{"tif", "tiff", "shp"}, null);
        if (file == null) {
            return;
        }
        
        try {
            if (mapFrame.getMapContent() == null) {
                FileDataStore store = FileDataStoreFinder.getDataStore(file);
                featureSource = store.getFeatureSource();
                setGeometry();

                map = new DefaultMapContext();
                map.setTitle("Žemėlapis");
                Style style = createDefaultStyle();
                this.map.addLayer(featureSource, style);
                MapContent cont = new MapContent(map);
                this.mapFrame.setMapContent(cont);
                this.mapFrame.getMapContent().layers().clear();
                this.map.addLayer(featureSource, style);
                this.mapFrame.repaint();
                System.out.println("Selected layer = " + getSelectedLayerIndex());
                gSelectedFeatures = new ListFeatureCollection((SimpleFeatureType)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
            }
            else {
                FileDataStore store = FileDataStoreFinder.getDataStore(file);
                featureSource = store.getFeatureSource();
                Style style = createDefaultStyle();
                this.map.addLayer(featureSource, style);
                setGeometry2();
                this.mapFrame.repaint();
            }
        }
        catch(Exception e) {}
    }
    
    public void addLayer(File file) {
        //File file = JFileDataStoreChooser.showOpenFile("shp", null);

        if (file == null) {
            return;
        }
        
        try {
            if (mapFrame.getMapContent() == null) {
                FileDataStore store = FileDataStoreFinder.getDataStore(file);
                featureSource = store.getFeatureSource();
                setGeometry();

                map = new DefaultMapContext();
                map.setTitle("Žemėlapis");
                Style style = createDefaultStyle();
                this.map.addLayer(featureSource, style);
                this.mapFrame.setMapContent(map);
                this.mapFrame.getMapContent().layers().clear();
                this.map.addLayer(featureSource, style);
                this.mapFrame.repaint();
                System.out.println("Selected layer = " + getSelectedLayerIndex());
                gSelectedFeatures = new ListFeatureCollection((SimpleFeatureType)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
            }
            else {
                FileDataStore store = FileDataStoreFinder.getDataStore(file);
                featureSource = store.getFeatureSource();
                Style style = createDefaultStyle();
                this.map.addLayer(featureSource, style);
                setGeometry2();
                this.mapFrame.repaint();
            }
        }
        catch(Exception e) {}
    }
    
    /**
     * Create a default Style for feature display
     */
    private Style createDefaultStyle() {
        Rule rule = null;
        if ((color % 3) == 0)
            rule = createRule(LINE_COLOUR, FILL_COLOUR);
        if ((color % 3) == 1)
            rule = createRule(LINE_COLOUR, FILL1_COLOUR);
        if ((color % 3) == 2)
            rule = createRule(LINE_COLOUR, FILL2_COLOUR);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(rule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }
    
    /**
     * Create a Style where features with given IDs are painted
     * yellow, while others are painted with the default colors.
     */
    private Style createSelectedStyle(Set<FeatureId> IDs) {
        Rule selectedRule = createRule(LINE_SELECTED_COLOUR, SELECTED_COLOUR);
        selectedRule.setFilter(ff.id(IDs));

        Rule otherRule = createRule(LINE_COLOUR, FILL_COLOUR);
        otherRule.setElseFilter(true);

        FeatureTypeStyle fts = sf.createFeatureTypeStyle();
        fts.rules().add(selectedRule);
        fts.rules().add(otherRule);

        Style style = sf.createStyle();
        style.featureTypeStyles().add(fts);
        return style;
    }
    
    /**
     * Helper for createXXXStyle methods. Creates a new Rule containing
     * a Symbolizer tailored to the geometry type of the features that
     * we are displaying.
     */
    private Rule createRule(Color outlineColor, Color fillColor) {
        Symbolizer symbolizer = null;
        Fill fill = null;
        Stroke stroke = sf.createStroke(ff.literal(outlineColor), ff.literal(LINE_WIDTH));

        switch (geometryType) {
            case POLYGON:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));
                symbolizer = sf.createPolygonSymbolizer(stroke, fill, geometryAttributeName);
                break;

            case LINE:
                symbolizer = sf.createLineSymbolizer(stroke, geometryAttributeName);
                break;

            case POINT:
                fill = sf.createFill(ff.literal(fillColor), ff.literal(OPACITY));

                Mark mark = sf.getCircleMark();
                mark.setFill(fill);
                mark.setStroke(stroke);

                Graphic graphic = sf.createDefaultGraphic();
                graphic.graphicalSymbols().clear();
                graphic.graphicalSymbols().add(mark);
                graphic.setSize(ff.literal(POINT_SIZE));

                symbolizer = sf.createPointSymbolizer(graphic, geometryAttributeName);
        }
        Rule rule = sf.createRule();
        rule.symbolizers().add(symbolizer);
        return rule;
    }
    
    /**
     * Retrieve information about the feature geometry
     */
    private void setGeometry() {
        GeometryDescriptor geomDesc = //this.mapFrame.getMapContext().getLayer(getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor();
                featureSource.getSchema().getGeometryDescriptor();
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz) ||
                MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeomType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz) ||
                MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeomType.LINE;

        } else {
            geometryType = GeomType.POINT;
        }

    }
    
    private void setGeometry2() {
        GeometryDescriptor geomDesc = this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema().getGeometryDescriptor();
        geometryAttributeName = geomDesc.getLocalName();

        Class<?> clazz = geomDesc.getType().getBinding();

        if (Polygon.class.isAssignableFrom(clazz) ||
                MultiPolygon.class.isAssignableFrom(clazz)) {
            geometryType = GeomType.POLYGON;

        } else if (LineString.class.isAssignableFrom(clazz) ||
                MultiLineString.class.isAssignableFrom(clazz)) {

            geometryType = GeomType.LINE;

        } else {
            geometryType = GeomType.POINT;
        }

    }

    private Set<FeatureId> getSelectedFeatureIDs(FeatureCollection selectedFeatures){
        FeatureIterator iter = selectedFeatures.features();
        Set<FeatureId> IDs = new HashSet<FeatureId>();
        maxY = maxX = minY = minX = -1;
        try {
            while (iter.hasNext()) {
                Feature feature = iter.next();
                BoundingBox bounds = feature.getBounds();
                maxX = (maxX == -1 || bounds.getMaxX() > maxX) ? bounds.getMaxX() : maxX;
                maxY = (maxY == -1 || bounds.getMaxY() > maxY) ? bounds.getMaxY() : maxY;
                minX = (minX == -1 || bounds.getMinX() < minX) ? bounds.getMinX() : minX;
                minY = (minY == -1 || bounds.getMinY() < minY) ? bounds.getMinY() : minY;
                
                if (minX == maxX && minX != -1 && maxX != -1 && minY == maxY && minY != -1 && maxY != -1) {
                    maxX = minX + 2 * POINT_SIZE;
                    maxY = minY + 2 * POINT_SIZE;
                }
                
                IDs.add(feature.getIdentifier());

                System.out.println("   " + feature.getIdentifier());
            }
        }
        finally {
            iter.close();
        }
        return IDs;
    }
    
    public void deselect(){
        this.minX = -1;
        this.maxX = -1;
        this.minY = -1;
        this.maxY = -1;
        setGeometry2();
        table.setModel(new DefaultTableModel(0, 0));
        //gSelectedFeatures.clear();
        gSelectedFeatures = null;
        Set<FeatureId> IDs = new HashSet<FeatureId>();
        displaySelectedFeatures(IDs);
    	this.mapFrame.repaint();
    }
    
    public void queryPanel() {
        JPanel queryPanel = new JPanel(new BorderLayout());
        
        JButton data = new JButton("Data");
        text = new JTextField(80);
        text.setText("include"); // include selects everything!
        JPanel panel = new JPanel();
        panel.add(text);
        panel.add(data);
        queryPanel.add(panel, BorderLayout.NORTH);
        table = new JTable();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        table.setModel(new DefaultTableModel(0, 0));
        table.setPreferredScrollableViewportSize(new Dimension(500, 100));
        JScrollPane scrollPane = new JScrollPane(table);
        queryPanel.add(scrollPane, BorderLayout.CENTER);
        
        data.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                executeQuery(SelectionLab.this.text.getText());
            }
        });
        
        this.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
            gSelectedFeatures = null;
            setGeometry2();
            int[] selectedRows = table.getSelectedRows();
            Set<FeatureId> IDs = new HashSet<FeatureId>();
            
            System.out.println("Selected layer = " + getSelectedLayerIndex());
            FeatureCollection selectedFeatures = new ListFeatureCollection((SimpleFeatureType)mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getSchema());
            //for(int i=0; i < selectedRows.length; i++){
            for (int i = 0; i < table.getRowCount(); i++) {
		String featureId = (String) table.getValueAt(i, 0);
                    try {
                        FeatureCollection features = mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource().getFeatures();
			FeatureIterator iterator = features.features();
			while(iterator.hasNext()){
                            Feature feature = iterator.next();
                            if(feature.getIdentifier().getID().equals(featureId)){
                                selectedFeatures.add(feature);
				break;
                            }
			}
                    } 
                    catch (Exception e1) {
                        e1.printStackTrace();
                    }
            }
            IDs = SelectionLab.this.getSelectedFeatureIDs(selectedFeatures);
            displaySelectedFeatures(IDs);
				}
	});        
        mapFrame.getContentPane().add(queryPanel,BorderLayout.SOUTH);
        mapFrame.pack();
    }
    
    public void executeQuery(String query) {
        System.out.println("Selected layer = " + getSelectedLayerIndex());
        SimpleFeatureSource source = (SimpleFeatureSource)this.mapFrame.getMapContent().layers().get(getSelectedLayerIndex()).getFeatureSource();

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();
        try {
            
	        org.opengis.filter.Filter filter = CQL.toFilter(query);	
	        Query query_ = new Query(schema.getName().getLocalPart(), filter, new String[] { name });	
	        SimpleFeatureCollection features = source.getFeatures(filter);
	        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
	        table.setModel(model);
        }
        catch (Exception e){
        	e.printStackTrace();
        }
    }
    
    public SimpleFeatureCollection executeQuery(String query, String layer) {
        
        SimpleFeatureSource source = (SimpleFeatureSource) getLayerByName(layer).getFeatureSource();

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();
        SimpleFeatureCollection features = null;
        try {
            
	        org.opengis.filter.Filter filter = CQL.toFilter(query);	
	        Query query_ = new Query(schema.getName().getLocalPart(), filter, new String[] { name });	
	        features = source.getFeatures(filter);
	        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
	        table.setModel(model);
        }
        catch (Exception e){
        	e.printStackTrace();
        }
        return features;
    }
    
    public SimpleFeatureCollection executeQuery(String query, int index) {
        SimpleFeatureSource source = (SimpleFeatureSource)this.mapFrame.getMapContent().layers().get(index).getFeatureSource();

        FeatureType schema = source.getSchema();
        String name = schema.getGeometryDescriptor().getLocalName();
        SimpleFeatureCollection features = null;
        try {
            
	        org.opengis.filter.Filter filter = CQL.toFilter(query);	
	        Query query_ = new Query(schema.getName().getLocalPart(), filter, new String[] { name });	
	        features = source.getFeatures(filter);
	        FeatureCollectionTableModel model = new FeatureCollectionTableModel(features);
	        table.setModel(model);
        }
        catch (Exception e){
        	e.printStackTrace();
        }
        return features;
    }
    
    private int getSelectedLayerIndex() {
        int selectedLayerIndex = -1;
        //for (int i = 0; i < this.mapFrame.getMapContext().getLayerCount(); i++) {
        for (int i = this.mapFrame.getMapContent().layers().size() - 1; i >= 0; i--) {
            if (this.mapFrame.getMapContent().layers().get(i).isSelected()) {
                selectedLayerIndex = i;
                break;
            }
        }
        return selectedLayerIndex;
    }
    public void unselectAllAndSelectLayer(int index){
    	unselectAllLayers();
    	this.mapFrame.getMapContent().layers().get(index).setSelected(true);
    }
    public int unselectAllLayers() {
        int selectedLayerIndex = -1;
        //for (int i = 0; i < this.mapFrame.getMapContext().getLayerCount(); i++) {
        for (int i = this.mapFrame.getMapContent().layers().size() - 1; i >= 0; i--) {
            this.mapFrame.getMapContent().layers().get(i).setSelected(false);
        }
        return selectedLayerIndex;
    }
    private SimpleFeatureCollection addArea(SimpleFeatureCollection features) {
        SimpleFeatureIterator iterator = features.features();
        SimpleFeatureCollection finalFeatures = FeatureCollections.newCollection();
        
        /*SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();           
        typeBuilder.setCRS(features.getSchema().getCoordinateReferenceSystem());
        typeBuilder.setName(features.getSchema().getName());
        typeBuilder.add("geom", Polygon.class, 3346);
        typeBuilder.add(Intersection.getShortName(features.getSchema().getName()) + "_id", String.class);
        typeBuilder.addAll(Intersection.getNamedAttributeDescriptors(features.getSchema()));
        typeBuilder.add("Plotas", Double.class);
        SimpleFeatureType type = typeBuilder.buildFeatureType();
        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(type);*/
        double[] area = new double[features.size()];
        int i = 0;
        while (iterator.hasNext()) {
            SimpleFeature feature = iterator.next();
            Geometry geom = (Geometry) feature.getDefaultGeometry();
            area[i] = geom.getArea();
            SimpleFeature result = Attributes.addAttribute(feature, "Plotas", Double.class, area[i]);
            finalFeatures.add(result);
            i++;
        }
        return finalFeatures;
    }
    
    private SimpleFeatureCollection getFeaturesWithCorrectHeightDifference(SimpleFeatureCollection correctForestPercentageCollection) {

        SimpleFeatureCollection averageHeights = FeatureCollections.newCollection();
        try {
        // timer.start("intersectionOld");
        // SimpleFeatureCollection heightsOld = intersectionOld(correctForestPercentageCollection, (SimpleFeatureCollection) getHeights().getFeatureSource().getFeatures(), "heightsOld");
        // simpleFeatureCollectionToLayer(heightsOld, "heightsOld");
        // timer.stop();


        SimpleFeatureCollection heights = Intersection.intersection(correctForestPercentageCollection, (SimpleFeatureCollection) mapFrame.getMapContent().layers().get(3).getFeatureSource().getFeatures(),
                "heights");
        // simpleFeatureCollectionToLayer(heights, "heights");


        

        SimpleFeatureIterator correctForestationIterator = correctForestPercentageCollection.features();
        while (correctForestationIterator.hasNext()) {
            SimpleFeature correctForestationFeature = correctForestationIterator.next();



            SimpleFeatureIterator heightsIterator = heights.subCollection(Intersection.idFilter(correctForestPercentageCollection, correctForestationFeature.getID())).features();



            double minHeight = Double.MAX_VALUE;
            double maxHeight = 0;
            double sumHeight = 0;
            int heightPolygons = 0;

            while (heightsIterator.hasNext()) {
                SimpleFeature heightsFeature = heightsIterator.next();

                double height = (Double) heightsFeature.getAttribute("Aukstis");

                sumHeight += height;
                minHeight = height < minHeight ? height : minHeight;
                maxHeight = height > maxHeight ? height : maxHeight;
                heightPolygons++;
            }

            if (maxHeight - minHeight < getMaxHeightDifference()) {
                double averageHeight = sumHeight / heightPolygons;

                averageHeights.add(Attributes.addAttribute(correctForestationFeature, "averageHeight", Double.class, averageHeight));
            }


        }
        }
        catch(Exception e) {
            System.out.println("Error: " + e);
        }

        // System.out.println(timer.prettyPrint());
        return averageHeights;
    }
    
    public float getMaxHeightDifference() {
        return maxHeightDifference;
    }

    public void setMaxHeightDifference(float maxHeightDifference) {
        this.maxHeightDifference = maxHeightDifference;
    }
    
}