package com.mycompany.tutorial;
import static java.lang.Math.sqrt;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.factory.CommonFactoryFinder;
import org.geotools.feature.AttributeTypeBuilder;
import org.geotools.feature.DefaultFeatureCollection;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureCollections;
import org.geotools.feature.FeatureIterator;
import org.geotools.feature.NameImpl;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.feature.simple.SimpleFeatureTypeBuilder;
import org.geotools.map.FeatureLayer;
import org.geotools.map.Layer;
import org.geotools.map.MapContent;
import org.geotools.styling.FeatureTypeStyle;
import org.geotools.styling.Graphic;
import org.geotools.styling.Mark;
import org.geotools.styling.PointSymbolizer;
import org.geotools.styling.Rule;
import org.geotools.styling.SLD;
import org.geotools.styling.Style;
import org.geotools.styling.StyleFactory;
import org.opengis.feature.Feature;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.filter.FilterFactory;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import static java.lang.Math.abs;

public class GIS extends JFrame {
	SelectionLab mapFrame;
	JTextField atstumasF;
	JTextField TurretAtstumasF;
	JTextField TurretHeightF;
	JTextField TurretSightF;
	JTextField TurretCountF;
	JTextField LakeDistanceF;
	JTextField LakePercentageF;
	JTextField BaseRadiusF;
	JTextField WerehouseDistanceF;
	
	public GIS(SelectionLab mapFrame){
		this.mapFrame = mapFrame;
	}
	public void init(){
		this.getContentPane().setLayout(new BorderLayout());
		
		JPanel dataPanel = new JPanel();
		SpringLayout layout = new SpringLayout();
		dataPanel.setLayout(new GridLayout(0,2));
		JLabel atstumasL = new JLabel("Distance to border(m): ");
		atstumasF = new JTextField("10000");
		dataPanel.add(atstumasL);
		dataPanel.add(atstumasF);
		
		JLabel TurretAtstumasL = new JLabel("Turret distance to forest(m): ");
		TurretAtstumasF = new JTextField("1000");
		dataPanel.add(TurretAtstumasL);
		dataPanel.add(TurretAtstumasF);
		
		JLabel TurretHeightL = new JLabel("Minimal turret height(m)");
		TurretHeightF = new JTextField("100");
		dataPanel.add(TurretHeightL);
		dataPanel.add(TurretHeightF);
		
		JLabel TurretSightL = new JLabel("Minimal turret sight(m)");
		TurretSightF = new JTextField("1000");
		dataPanel.add(TurretSightL);
		dataPanel.add(TurretSightF);
		
		JLabel TurretCountL = new JLabel("Minimal turret amount");
		TurretCountF = new JTextField("3");
		dataPanel.add(TurretCountL);
		dataPanel.add(TurretCountF);
		
		JLabel LakeDistanceL = new JLabel("Distance to lakes(m)");
		LakeDistanceF = new JTextField("10000");
		dataPanel.add(LakeDistanceL);
		dataPanel.add(LakeDistanceF);
		
		JLabel LakePercentageL = new JLabel("Water percentage % ");
		LakePercentageF = new JTextField("1");
		dataPanel.add(LakePercentageL);
		dataPanel.add(LakePercentageF);

		JLabel BaseRadiusL = new JLabel("Werehouse radius(m)");
		BaseRadiusF = new JTextField("1000");
		dataPanel.add(BaseRadiusL);
		dataPanel.add(BaseRadiusF);
		
		/*JLabel WerehouseDistanceL = new JLabel("Distances between werehouses(KM): ");
		WerehouseDistanceF = new JTextField("100");
		dataPanel.add(BaseRadiusL);
		dataPanel.add(BaseRadiusF);*/
		JButton find = new JButton("Find");
		find.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
            	GIS.this.startSearch();
            }
        });
		dataPanel.add(find);
		this.add(dataPanel);
		this.setSize(400, 400);
		this.pack();
	}
	
	/*public FeatureCollection selectFeatures(String query){
		return mapFrame.executeQuery(query,true);
	}
	
	public FeatureCollection bufferFeatures(FeatureCollection features, double distance){
		FeatureIterator featuresI = features.features();
		SimpleFeatureType featureType = (SimpleFeatureType) features.getSchema();
		
        SimpleFeatureTypeBuilder stb = new SimpleFeatureTypeBuilder(); 
        stb.init(featureType); 
        stb.setName("bufferedFeatureType"); 

        //Add the new attribute 
        stb.add("bufferedGeom", Geometry.class); 
        SimpleFeatureType newFeatureType = stb.buildFeatureType(); 
        SimpleFeatureBuilder sfb = new SimpleFeatureBuilder(newFeatureType); 
        SimpleFeatureCollection collection = FeatureCollections.newCollection();
        
		while(featuresI.hasNext()){
			SimpleFeature feature = (SimpleFeature) featuresI.next();
			Geometry geometry = (Geometry) feature.getDefaultGeometry();
			geometry = geometry.buffer(distance);
			
            sfb.addAll(feature.getAttributes()); 
            sfb.set("bufferedGeom",geometry); 
            collection.add(sfb.buildFeature(null)); 
			//feature.setDefaultGeometry(geometry);
		}
		return collection;
	}
	
	public FeatureCollection intersacts(FeatureCollection bufferedFeatures, FeatureCollection nonBuffered){
		FeatureIterator bFeautresI = bufferedFeatures.features();
		ListFeatureCollection result = new ListFeatureCollection((SimpleFeatureType)nonBuffered.getSchema());
		while (bFeautresI.hasNext()){
			SimpleFeature bFeature = (SimpleFeature)bFeautresI.next();
			Geometry bGeometry = (Geometry) bFeature.getAttribute("bufferedGeom");//.getDefaultGeometry();
			
			FeatureIterator nFeaturesI = nonBuffered.features();
			while(nFeaturesI.hasNext()){
				SimpleFeature nFeature = (SimpleFeature) nFeaturesI.next();
				Geometry nGeometry = (Geometry) nFeature.getDefaultGeometry();
				if(bGeometry.intersects(nGeometry)){
					result.add(nFeature);
				}
			}
			//nonBuffered.removeAll((Collection) result);
		}
		return result;
	}*/
	
	public void startSearch(){
		try {
		
		//1.Buferizuojamos Baltarusijos sienos linijos, imant apskritimo spindulį lygų pasirinktam atstumui iki sienos.
		double distanceToBorder = Double.parseDouble(atstumasF.getText());
        //SimpleFeatureCollection bufBorder = new DefaultFeatureCollection(new BufferedFeatureCollection(mapFrame.executeQuery("NM3='Baltarusijos Respublika'",0), "bufBorder", distanceToBorder));
		SimpleFeatureCollection border = this.mapFrame.executeQuery("NM3='Baltarusijos Respublika'",0);
		this.mapFrame.unselectAllAndSelectLayer(2);
		this.mapFrame.selectMultiFeatures(this.mapFrame.searchArea, false);
		SimpleFeatureCollection forests = this.mapFrame.gSelectedFeatures;
		forests = this.filterByDistance(border, forests, distanceToBorder,true);
		Style style = SLD.createSimpleStyle(forests.getSchema());
        MapContent map = mapFrame.mapFrame.getMapContent();
        Layer layer = new FeatureLayer(forests, style);
        layer.setTitle("Miskai salia sienos");
        map.addLayer(layer);
        
        //2.Sankirta tarp 1 žingsnio ir miškų sluoksnio. Taip turime miškus, patenkančius į norimą atstumą.
        //SimpleFeatureCollection miskai = new DefaultFeatureCollection((FeatureCollection<SimpleFeatureType, SimpleFeature>) this.mapFrame.mapFrame.getMapContent().layers().get(1).getFeatureSource().getFeatures());
       /* SimpleFeatureCollection miskai = new DefaultFeatureCollection(this.mapFrame.gSelectedFeatures);
        //SimpleFeatureCollection goodMiskai = Intersection.intersection(bufBorder, miskai, "bufBorder");
        SimpleFeatureCollection goodMiskai = new DefaultFeatureCollection(new IntersectedFeatureCollection(border,miskai));
        //setDefaultGeom(goodMiskai,"secondGeom");
        //rajSanVirs = addArea(rajSanVirs);
        Style style2 = SLD.createSimpleStyle(goodMiskai.getSchema());
        MapContent map2 = mapFrame.mapFrame.getMapContent();
        Layer layer2 = new FeatureLayer(goodMiskai, style2);
        layer2.setTitle("tinkami miskai");
        map2.addLayer(layer2);*/
        
        //3.Buferizuojame miško poligonus sritimi atitinkamu atstumu nuo poligono ribos.
        //4.Atliekame sankirta su  3 ir viršukalnių sluoksniu. Turime miškus ir gretimas viršukalnes.
        this.mapFrame.unselectAllAndSelectLayer(1);
        this.mapFrame.selectMultiFeatures(this.mapFrame.searchArea, false);
        SimpleFeatureCollection kalnai = this.mapFrame.gSelectedFeatures;
        double turretDistance = Double.parseDouble(TurretAtstumasF.getText());
        kalnai = this.filterByDistance(forests,kalnai, turretDistance, false);
        style = SLD.createSimpleStyle(kalnai.getSchema());
        map = mapFrame.mapFrame.getMapContent();
        layer = new FeatureLayer(kalnai, style);
        layer.setTitle("kalnai salia misku");
        map.addLayer(layer);
        
        //5.Atmetame per žemas viršukalnes.
        //double turretHeight = Double.parseDouble(TurretHeightF.getText());
        this.mapFrame.unselectAllAndSelectLayer(6);
        SimpleFeatureCollection filteredByHeight =
        	new DefaultFeatureCollection(mapFrame.executeQuery("AUKSTIS>"+TurretHeightF.getText(),6));
        style = SLD.createSimpleStyle(filteredByHeight.getSchema());
        map = mapFrame.mapFrame.getMapContent();
        layer = new FeatureLayer(filteredByHeight, style);
        layer.setTitle("tinkami kalnai pagal auksti");
        map.addLayer(layer);
        
        //6.Buferizuojame viršukalnes spinduliu, kurio ilgis lygus matomumo parametrui.
        /*double turretSight = Double.parseDouble(TurretSightF.getText());
        SimpleFeatureCollection bufKalnai = new DefaultFeatureCollection(
        		(FeatureCollection<SimpleFeatureType, SimpleFeature>)this.mapFrame.mapFrame.getMapContent().layers().get(8).getFeatureSource().getFeatures());
        bufKalnai = new DefaultFeatureCollection(new BufferedFeatureCollection(bufKalnai,"bufBorder", turretSight));
        style2 = SLD.createSimpleStyle(bufKalnai.getSchema());
        map2 = mapFrame.mapFrame.getMapContent();
        layer2 = new FeatureLayer(bufKalnai, style2);
        layer2.setTitle("buf bokstai");
        map2.addLayer(layer2);*/
        
        //7.Atliekame sankritą su 6 ir kelių sluoksnių, taip turime kelius, kuriuos mato bokštas.
        this.mapFrame.unselectAllAndSelectLayer(3);
        this.mapFrame.selectMultiFeatures(this.mapFrame.searchArea, false);
        
        SimpleFeatureCollection roads = this.mapFrame.gSelectedFeatures;
        	//roads = new DefaultFeatureCollection(new IntersectedFeatureCollection(bufKalnai,roads));
        /*style2 = SLD.createSimpleStyle(roads.getSchema());
        map2 = mapFrame.mapFrame.getMapContent();
        layer2 = new FeatureLayer(roads, style2);
        layer2.setTitle("keliai apie bokstus");
        map2.addLayer(layer2);*/
        
        //8.Atmetame tuos bokštus, kurie nemato nė vieno kelio.
        double turretSight = Double.parseDouble(TurretSightF.getText());
        SimpleFeatureCollection goodTurrets = filterUslessMountains(filteredByHeight,roads,turretSight);
        style = createPointStyle();//SLD.createSimpleStyle(goodTurrets.getSchema());
        map = mapFrame.mapFrame.getMapContent();
        layer = new FeatureLayer(goodTurrets, style);
        layer.setTitle("bokstai matantys kelius");
        map.addLayer(layer);
        
        //9.Atmetame miškus, kurie šalia turi per mažai tinkamų viršukalnių.
        forests = filterUslessForests(goodTurrets, forests, Double.parseDouble(TurretAtstumasF.getText()), Integer.parseInt(TurretCountF.getText()));
        style = SLD.createSimpleStyle(forests.getSchema());
        map = mapFrame.mapFrame.getMapContent();
        layer = new FeatureLayer(forests, style);
        layer.setTitle("miskai su pakankamai bokstu");
        map.addLayer(layer);
        
        //10.Buferizuojame miškus atstumu iki ežerų.
        double lakeDistance = Double.parseDouble(LakeDistanceF.getText());
        this.mapFrame.unselectAllAndSelectLayer(4);
        this.mapFrame.selectMultiFeatures(this.mapFrame.searchArea, false);
        SimpleFeatureCollection lakes = this.mapFrame.gSelectedFeatures;
    	lakes = this.filterByDistance(forests, lakes, lakeDistance, true);
	    style = SLD.createSimpleStyle(lakes.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(lakes, style);
	    layer.setTitle("ezeriokai apie miskus");
	    map.addLayer(layer);
	    
	    //11.Suformuojame teritorijos apskritimą ir pamatuojame ežeringumą – procentas aprėpto ploto atėmus miško plotą.
	    //12.Jei ežeringumas per mažas mišką atmetame.
	    forests = filterForestWithLowWater(forests, lakes, Double.parseDouble(LakePercentageF.getText()));
	    style = SLD.createSimpleStyle(forests.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(forests, style);
	    layer.setTitle("miskai su vandeniu");
	    map.addLayer(layer);
	    
	    //13.Einame per atrinktus miškus ir bandome „įpiešti“ bazės dydžio apskritimą, jei pavyksta formuojame rezultatą.
	    SimpleFeatureCollection bazes = getSandeliokai(forests,Double.parseDouble(BaseRadiusF.getText()));
	    style = SLD.createSimpleStyle(bazes.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(bazes, style);
	    layer.setTitle("bazes");
	    map.addLayer(layer);
	    
	    this.mapFrame.unselectAllAndSelectLayer(12);
	    SimpleFeatureCollection bazesDeep = getBiggest(bazes,"gylis","miskoId");
	    style = SLD.createSimpleStyle(bazesDeep.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(bazesDeep, style);
	    layer.setTitle("!!!Giliausios bazes!!!");
	    map.addLayer(layer);
	    
	    /*double distanceToWerehouse = Double.parseDouble(WerehouseDistanceF.getText())*1000;
	    style = SLD.createSimpleStyle(bazesDeep.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(bazesDeep, style);
	    layer.setTitle("!!!Giliausios bazes!!!");
	    map.addLayer(layer);
	    
	    DefaultFeatureCollection bufSandeliai = new DefaultFeatureCollection(new IntersectedFeatureCollection(new BufferedFeatureCollection(bazesDeep,"any",100000.0),bazesDeep));
	    style = SLD.createSimpleStyle(bufSandeliai.getSchema());
	    map = mapFrame.mapFrame.getMapContent();
	    layer = new FeatureLayer(bufSandeliai, style);
	    layer.setTitle("!!!waje!!!");
	    map.addLayer(layer);*/
	    
	    /*selectLayer("AdminVien_L");
		FeatureCollection featuresBorder = selectFeatures("NM3='Baltarusijos Respublika'");
		featuresBorder = bufferFeatures(featuresBorder, Double.parseDouble(distanceToBorder));
		
		selectLayer("miskai");
		FeatureCollection miskai = selectFeatures("include");
		FeatureCollection atrinktiMiskai = intersacts(featuresBorder,miskai);
		//debug TODO
		mapFrame.displaySelectedFeatures(mapFrame.getSelectedFeatureIDs(atrinktiMiskai));*/
		} catch(Throwable t){
			t.printStackTrace();
		}
	}
	/**
	 * 
	 * @param features1 
	 * @param features2 - sitos feature bus rezultatas, jei tenkina atstuma iki features1
	 * @param distance - atstumas
	 * @param idAttribute - jei ne null, tai imamas atributas laikomas unikaliu, ir dupllikatai nededami i lista
	 * @return
	 */
	public SimpleFeatureCollection filterByDistance(SimpleFeatureCollection features1, SimpleFeatureCollection features2, double distance, boolean unique){
		SimpleFeatureIterator features1I = features1.features();
		SimpleFeatureCollection result = new ListFeatureCollection(features2.getSchema());
		Map<String,Integer> foundIds = new HashMap<String,Integer>();
		
		while(features1I.hasNext()){
			SimpleFeature feature1 = features1I.next();
			SimpleFeatureIterator features2I = features2.features();
			while(features2I.hasNext()){
				SimpleFeature feature2 = features2I.next();
				if(((Geometry)feature2.getDefaultGeometry()).isWithinDistance((Geometry)feature1.getDefaultGeometry(), distance)){
					if(unique){
						if(foundIds.get(feature2.getID()) == null){
							foundIds.put(feature2.getID(), new Integer(1));
							result.add(feature2);
						}
					} else {
						result.add(feature2);
					}
				}
			}
		}
		return result;
	}
	
    public SimpleFeatureCollection getBiggest(SimpleFeatureCollection features, String biggestAtr, String groupAtr){
    	SimpleFeatureIterator featuresI = features.features();
    	Map<String,SimpleFeature> foundFeatures = new HashMap();
    	while(featuresI.hasNext()){
    		SimpleFeature feature = featuresI.next();
    		String key = (String) feature.getAttribute(groupAtr);
    		
    		SimpleFeature storedFeature = foundFeatures.get(key);
    		if(storedFeature != null){
    			double inMap =  Double.parseDouble((String)(storedFeature.getAttribute(biggestAtr)));
    			double current = Double.parseDouble((String)(feature.getAttribute(biggestAtr)));
    			if(current>inMap){
    				foundFeatures.put(key, feature);
    			}
    		} else {
    			foundFeatures.put(key, feature);
    		}
    	}
    	return new ListFeatureCollection(features.getSchema(),new LinkedList(foundFeatures.values()));
    }
	public SimpleFeatureCollection filterUslessMountains(SimpleFeatureCollection mountains, SimpleFeatureCollection roads, double sight){
		SimpleFeatureIterator mount = mountains.features();
		SimpleFeatureCollection result = new ListFeatureCollection(mountains.getSchema());
		while(mount.hasNext()){
			SimpleFeature feature = mount.next();
			SimpleFeatureCollection temp = FeatureCollections.newCollection();
			temp.add(feature);
			temp = new IntersectedFeatureCollection(new BufferedFeatureCollection(temp,"belenkas",sight),roads);
			SimpleFeatureIterator tempF = temp.features();
			if(tempF.hasNext()){
				result.add(feature);
			}
			tempF.close();
			/*Geometry mountG = (Geometry) feature.getDefaultGeometry();
			Geometry sightMountG = mountG.buffer(sight);
			boolean roadFound = false;
			SimpleFeatureIterator roadsI = roads.features();
			while(roadsI.hasNext()){
				SimpleFeature road = roadsI.next();
				Geometry roadG = (Geometry) road.getDefaultGeometry();
				if(sightMountG.intersects(roadG)){
					roadFound = true;
					roadsI.close();
					break;
				}
			}
			if(roadFound){
				result.add(feature);
			}*/
		}
		return result;
	}
	
	public SimpleFeatureCollection filterUslessForests(SimpleFeatureCollection mountains, SimpleFeatureCollection forests,double distanceToForest, int amount){
		SimpleFeatureIterator forestsI = forests.features();
		SimpleFeatureCollection result = new ListFeatureCollection(forests.getSchema());
		while(forestsI.hasNext()){
			SimpleFeature feature = forestsI.next();
			Geometry forestG = (Geometry) feature.getDefaultGeometry();
			//Geometry bufForestG = forestG.buffer(distanceToForest);
			
			int mountsFound = 0;
			SimpleFeatureIterator mountainsI = mountains.features();
			while(mountainsI.hasNext()){
				SimpleFeature mountain = mountainsI.next();
				Geometry mountainG = ((Geometry) mountain.getDefaultGeometry()).buffer(distanceToForest);
				if(forestG.intersects(mountainG)){
					mountsFound++;
				}
				if(mountsFound>=amount){
					mountainsI.close();
					break;
				}
			}
			if(mountsFound>=amount){
				result.add(feature);
			}
		}
		return result;
	}
	
	public SimpleFeatureCollection filterForestWithLowWater(SimpleFeatureCollection forests, SimpleFeatureCollection lakes, double percentage){
		SimpleFeatureCollection result = new ListFeatureCollection(forests.getSchema());
		
		SimpleFeatureIterator forestsI = forests.features();
		while(forestsI.hasNext()){
			SimpleFeature feature = forestsI.next();
			Geometry forestG = (Geometry) feature.getDefaultGeometry();
			Geometry bbox = forestG.getEnvelope();
			double maxX = bbox.getEnvelopeInternal().getMaxX();
			double minX = bbox.getEnvelopeInternal().getMinX();
			double maxY = bbox.getEnvelopeInternal().getMaxY();
			double minY = bbox.getEnvelopeInternal().getMinY();
			
			double centerX = (abs(maxX-minX)/2)+ minX; 
			double centerY = (abs(maxY-minY)/2)+ minY;
			double ilgis = abs(maxX-minX);
			double plotis = abs(maxY-minY);
			double maxRadius = (ilgis > plotis) ? ilgis : plotis;
			//Geometry bufForestG = forestG.buffer(distanceToForest);
			Coordinate centerCoordinate = new Coordinate(centerX,centerY);
			CoordinateArraySequence coordSeq = new CoordinateArraySequence(new Coordinate[]{ centerCoordinate });
			Point centerPoint = new Point(coordSeq, new GeometryFactory());
			Geometry buffedPoint = (Geometry)centerPoint.buffer(maxRadius + 1000);
			SimpleFeatureIterator lakesI = lakes.features();
			double waterArea = 0;
			while(lakesI.hasNext()){
				SimpleFeature lake = lakesI.next();
				if(((Geometry)lake.getDefaultGeometry()).intersects(buffedPoint)){
					Geometry lakePart = ((Geometry) lake.getDefaultGeometry()).intersection(buffedPoint);
					waterArea+= lakePart.getArea();
				}
			}
			System.out.println("Santykiui: " + buffedPoint.getArea() + " " + waterArea);
			if(waterArea/buffedPoint.getArea()>= percentage/100){
				feature.setAttribute("PERIMETER", waterArea/buffedPoint.getArea());
				feature.getType().getAttributeDescriptors();
				result.add(feature);
				System.out.println("santykis: " + waterArea/buffedPoint.getArea());
			}
		}
		
		return result;
	}
	
	public SimpleFeatureCollection getSandeliokai(SimpleFeatureCollection miskai, double radius){
        SimpleFeatureTypeBuilder typeBuilder = new SimpleFeatureTypeBuilder();
        //typeBuilder.setCRS(CRS.decode("EPSG:3346"));
        typeBuilder.setCRS(miskai.getSchema().getCoordinateReferenceSystem());
        //typeBuilder.setName(name);
        typeBuilder.setName(new NameImpl("super_duper_baze"));
        
        AttributeTypeBuilder builderA = new AttributeTypeBuilder();
        builderA.setBinding(Polygon.class);
        AttributeDescriptor attributeDescriptor = builderA.buildDescriptor("geom", builderA.buildType());
        typeBuilder.add(attributeDescriptor);
        //typeBuilder.add("geom", Polygon.class, 3346);
        typeBuilder.add("gylis", String.class);
        typeBuilder.add("miskoId", String.class);
        typeBuilder.setDefaultGeometry("geom");
        SimpleFeatureType type = typeBuilder.buildFeatureType();
        SimpleFeatureCollection result = new ListFeatureCollection(type);
        SimpleFeatureBuilder builder = new SimpleFeatureBuilder(type);
        
        SimpleFeatureIterator miskiokai = miskai.features();
        int id = 0;
        while(miskiokai.hasNext()){
        	final long startTime = System.nanoTime();
        	SimpleFeature miskiokas = miskiokai.next();
        	
        	Geometry forestG = (Geometry) miskiokas.getDefaultGeometry();
			Geometry bbox = forestG.getEnvelope();
			double maxX = bbox.getEnvelopeInternal().getMaxX() - radius;
			double minX = bbox.getEnvelopeInternal().getMinX() + radius;
			double maxY = bbox.getEnvelopeInternal().getMaxY() - radius;
			double minY = bbox.getEnvelopeInternal().getMinY() + radius;
			System.out.println(bbox.getArea());
			System.out.println(bbox.getArea()/(700*700));
			System.out.println(bbox.getArea()/(500*500));
			System.out.println(bbox.getArea()/1000000);
			//ant to didelio misko santykis area is stepo = 12070
			int step =(int)(sqrt(bbox.getArea()/6158));
			System.out.println(step);
			boolean incY=false,incX=false;
			int stepAfterFound = (int)radius*2;
			//cikliokas:
			for(int currentX = (int)minX; currentX < maxX; currentX+=step){
				if(incX){
					currentX+=stepAfterFound;
				}
				incX=false;
				for(int currentY = (int) minY; currentY < maxY; currentY+=step){
					if(incY) {
						currentY+=stepAfterFound;
					}
					Geometry point = constructPoint(currentX, currentY);
					if(!forestG.contains(point)){
						incY=false;
						continue;
					}
					Geometry circle = constructCircle(point, radius);
					if(circle.coveredBy(forestG)){
                        builder.set("geom",circle);
                        builder.add(id);
                        builder.set("gylis", ((MultiPolygon)forestG).getBoundary().distance(circle));
                        builder.set("miskoId", miskiokas.getID());
                        SimpleFeature resultFeature = builder.buildFeature(String.valueOf(id));

                        resultFeature.setDefaultGeometry(circle);
                        result.add(resultFeature);
                        id++;
                        incX=true;
                        incY=true;
                        //break cikliokas;
					} else {
						incY=false;
					}
				}
			}
			final long endTime = System.nanoTime();
			System.out.println("Su step=" +step+" truko=" +(endTime-startTime));
        }
        return result;
	}
	
	public Geometry constructCircle(Geometry point, double radius){
		Geometry buffedPoint = (Geometry)point.buffer(radius);
		return buffedPoint;
	}
	public Geometry constructPoint(double x, double y){
		Coordinate centerCoordinate = new Coordinate(x,y);
		CoordinateArraySequence coordSeq = new CoordinateArraySequence(new Coordinate[]{ centerCoordinate });
		Point centerPoint = new Point(coordSeq, new GeometryFactory());
		return centerPoint;
	}
	
	public SimpleFeatureCollection findApproporiateWerehouses(SimpleFeatureCollection werehouses, int count, double distance){
		SimpleFeatureIterator features1 = werehouses.features();
		int amount = werehouses.size();
		boolean[][] visibilityMatrix = new boolean[amount][amount];
		int i = 0;
		while (features1.hasNext()){
			SimpleFeature feature1 = features1.next();
			
			SimpleFeatureIterator features2 = werehouses.features();
			
			int j = 0;
			while(features2.hasNext()){
				SimpleFeature feature2 = features2.next();
				
				if(feature1.getID().equals(feature2.getID())){
					visibilityMatrix[i][j] = true;
				} else {
					Geometry geo1 = (Geometry)feature1.getDefaultGeometry();
					Geometry geo2 = (Geometry)feature2.getDefaultGeometry();
					if(geo1.isWithinDistance(geo2, distance)){
						visibilityMatrix[i][j] = true;
					} else {
						visibilityMatrix[i][j] = false;
					}
				}
				j++;
			}
			i++;
		}
		return null;
	}
	public void setDefaultGeom(SimpleFeatureCollection collection, String geomAttrName){
		SimpleFeatureIterator features = collection.features();
		while(features.hasNext()){
			features.next().setDefaultGeometry(geomAttrName);
		}
		features.close();
	}
	/*private MapLayer getAndSelectLayerByName(String name){
		MapLayer[] layers = mapFrame.getMapContext().getLayers();
		for(int i = 0; i < layers.length; i++){
			if(layers[i].getFeatureSource().getSchema().getName().getLocalPart().equals(name)){
				mapFrame.setSelectedLayerIndex(i);
				return layers[i];
			}
		}
		return null;
	}
	
	private void selectLayer(String name){
		getAndSelectLayerByName(name).setSelected(true);
	}*/
    /**
     * Create a Style to draw point features as circles with blue outlines
     * and cyan fill
     */
    private Style createPointStyle() {
    	
        Graphic gr = styleFactory.createDefaultGraphic();

        Mark mark = styleFactory.getCircleMark();

        mark.setStroke(styleFactory.createStroke(
                filterFactory.literal(Color.BLUE), filterFactory.literal(1)));

        mark.setFill(styleFactory.createFill(filterFactory.literal(Color.CYAN)));

        gr.graphicalSymbols().clear();
        gr.graphicalSymbols().add(mark);
        gr.setSize(filterFactory.literal(5));

        /*
         * Setting the geometryPropertyName arg to null signals that we want to
         * draw the default geomettry of features
         */
        PointSymbolizer sym = styleFactory.createPointSymbolizer(gr, null);

        Rule rule = styleFactory.createRule();
        rule.symbolizers().add(sym);
        FeatureTypeStyle fts = styleFactory.createFeatureTypeStyle(new Rule[]{rule});
        Style style = styleFactory.createStyle();
        style.featureTypeStyles().add(fts);

        return style;
    }
    static StyleFactory styleFactory = CommonFactoryFinder.getStyleFactory();
    static FilterFactory filterFactory = CommonFactoryFinder.getFilterFactory();
}
